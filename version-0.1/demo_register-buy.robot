*** Settings ***
Documentation       This test help to create a new user.
...                 Trigger buy now, add a card and address from assistant chat while buying.
Library             AppiumLibrary
Resource            android_resource.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application
Library             Screenshot       ${SAVE_SCREENSHOT}
# Suite Teardown  Remove Application  ${APP_PACKAGE}

*** Variables ***
# ${BUYNOW_SUCCESS_TEXT}        I'm placing the order now. I'll let you know when it is done or if there is any issue


*** Test Cases ***
1. Click on register link to register as a new user
    Wait Until Element Is Visible   id=no_account_button     20
    Page Should Contain Element     id=no_account_button    loglevel=INFO
    Click Element   id=no_account_button

2. Enter name surname email password and choose create
    Generate random number

    Generate demo surname   ${NO}

    Generate demo registration email    ${NO}

    Input Text      id=name         qa
    Input Text      id=surname      ${DEMO_SURNAME}
    Input Text      id=email        ${DEMO_REGISTER_EMAIL}
    Input Text      id=password     123
    Click Element   id=submit_button
    Run Keyword If  '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'IDEAS')]    20
    ...     ELSE
    ...     Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Ideas')]    25
    Log To Console  \nSuccessfully created a new account and landed on Ideas Page

# [For 28th and 3rd add base url www.johnlewis.com, change context to web and choose any random product to buy]
3. Click on browse
    Wait Until Element Is Visible   id=action_browse    15
    Click Element   id=action_browse
    Wait Until Element Is Visible  xpath=//android.widget.TextView[contains(@text,'Browse')]
    Log To Console  \nSuccessfully redirected to browse page

4. Choose any random product form the list and send it to assistant
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT3}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT4}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT5}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT8}')]
    ...     ELSE    Log     No product url found

    Wait Until Element Is Visible   id=send_to_assistant_button     15
    Click Element   id=send_to_assistant_button
    Log To Console  \nSuccessfully browsed a product and sent to assistant
    Sleep   1

# 5. Go back and re-enter chat room
#     Click Element   ${APP_BACK_BUTTON}
#     Click Element   xpath=//android.widget.TextView[contains(@text,'Chat')]

5. Verify chat bot messages for a new user without card and address
    Sleep   1
    Chat bot messages received
    Log To Console  \nChat bot messages received successfully in chat room

6. Add quantity
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT6}')]
    Click Element   xpath=//android.widget.TextView[contains(@text,'${TEXT6}')]
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'2')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'2')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'3')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'5')]
    ...     ELSE    Log     Quantity not available in the list
    Log To Console  \nSuccessfully updated quantity
    Sleep   2

7. Add a shipping address as a new user
    Generate flat/house/building/street number for address
    Generate flat/house/building/street name for address
    Generate address phone number
    Generate random recipientname/nickname
    Add shipping address
    Sleep   1

8. Add a card as a new user
    Generate a random CVV
    Add a card
    Sleep   1

9. Add size
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT4}')]
    Click Element   xpath=//android.widget.TextView[contains(@text,'Tops_Size:')]
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Select Tops_Size')]
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'M')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'XL')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'2')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'L')]
    ...     ELSE    Log     Size not available
    Log To Console  \nSuccessfully added size
    Sleep   2

10. Choose buy now from chat room
    Wait Until Element Is Visible   id=middle_button    15
    Log To Console  \nBuy now button visible
    Click Element   id=middle_button
    Sleep   5
    # Element Text Should Be     xpath=//android.widget.LinearLayout[contains(@index,'1')]    ${BUYNOW_SUCCESS_TEXT}
    # Element Text Should Be     xpath=//android.widget.TextView[contains(@text,'placing the order now.')]   ${BUYNOW_SUCCESS_TEXT}
    # Wait Until Element Is Visible       xpath=//android.widget.TextView[contains(@text,'${BUY_NOW_MESSAGE}')]    20
    ${buynow_chatbotresponse}    Get Text    xpath=//android.widget.TextView[contains(@text,'${BUYNOW_SUCCESS_TEXT1}')]
    Log To Console  \n${buynow_chatbotresponse} - received from chat bot
    # Click Element   ${APP_BACK_BUTTON}
    # Sleep   40
    # Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'0')]
    # Page Should Contain Text     ${BUYNOW_SUCCESS_TEXT1}${BUYNOW_SUCCESS_TEXT2}      loglevel=INFO
    # Wait Until Element Is Visible       xpath=//android.widget.TextView[contains(@text,'${BUYNOW_CONFIRMATION}')]    40
    # ${buynow_confirm}   Get Text    xpath=//android.widget.TextView[contains(@text,'${BUYNOW_CONFIRMATION}')]
    # Log To Console  \n'${buynow_confirm}' message received in chat room

11. Logout
    Click Element   ${APP_BACK_BUTTON}
    Click Element   id=action_account
    Click Element   id=profile_icon
    Click Element   id=log_out_button
    Wait Until Element Is Visible   id=no_account_button    15
    Log To Console  \nLogout successful

*** Keywords ***
# Generate demo surname
#     [Arguments]     ${number}
#     ${SURNAME}     Catenate    ${demo_str2}${number}
#     Log To Console      \n Sign-up surname is ${SURNAME}
#     Set Global Variable     ${DEMO_SURNAME}    ${SURNAME}

# Generate demo registration email
#     [Arguments]     ${number}
#     ${EMAIL}    Catenate    ${demo_str1}${demo_str2}${number}${demo_str3}
#     Log To Console  \n Email is ${EMAIL}
#     Set Global Variable     ${DEMO_REGISTER_EMAIL}    ${EMAIL}

# Chat bot messages received
#     Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Based on what I know you, I Have selected these options for you.')]     10
#     # ${TEXT}     Element Text Should Be  xpath=//android.widget.TextView[contains(@text,'Based on what I know you, I Have selected these options for you.')]   Based on what I know you, I Have selected these options for you.
#     # ${TEXT}     Based on what I know you, I Have selected these options for you
#     Page Should Contain Text    ${TEXT1}    loglevel=INFO
#     Log To Console   \n The message '${TEXT1}' is present

#     Page Should Contain Text    ${TEXT2}               loglevel=INFO
#     Log To Console  \n The message '${TEXT2}' is present

#     Page Should Contain Element    ${IMAGE}            loglevel=INFO
#     Log To Console  \n Image with ${IMAGE} id present

#     Page Should Contain Text    ${TEXT4}               loglevel=INFO
#     Log To Console  \n Text found is '${TEXT4}'

#     Page Should Contain Text    ${TEXT5}               loglevel=INFO
#     Log To Console  \n Text found is '${TEXT5}'

#     Page Should Contain Text    ${TEXT6}               loglevel=INFO
#     Log To Console  \n Text found is '${TEXT6}'

#     Page Should Contain Text    ${TEXT7}               loglevel=INFO
#     Log To Console  \n Text found is '${TEXT7}'

#     Page Should Contain Text    ${TEXT8}               loglevel=INFO
#     Log To Console  \n Text found is '${TEXT8}'

#     Page Should Contain Text    ${TEXT9}                loglevel=INFO
#     Log To Console  \n Text found is '${TEXT9}'





