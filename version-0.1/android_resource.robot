*** Settings ***
Documentation		This file contain all the reusable keywords and variables for automation
Library     AppiumLibrary
Library     BuiltIn
Library     String
Library     Screenshot  ${SAVE_SCREENSHOT}

*** Variables ***
#Appium Configuration
# ${APP}                  /mobile_automation/zwoop_automation/android_automation/android_builds/dev_builds/zwoop-0.1.8-debug.apk
${APP}                  /mobile_automation/zwoop_automation/android_automation/android_builds/demo/zwoop-0.6.2-demo.apk
# ${APP}                  /mobile_automation/zwoop_automation/android_automation/android_builds/demo/zwoop-0.6.2-demo.apk

${APPIUM_SERVER}        http://localhost:${PORT}/wd/hub
${PLATFORM_NAME}        Android
${APP_PACKAGE}         com.zwoop.android
${APP_ACTIVITY}        com.zwoop.android.ui.splash.SplashActivity


#Test Data
${INVALID_EMAIL}            qazwoop.com

${VALID_EMAIL_LOGIN}        qa01@zwoop.biz
${VALID_EMAIL_PASSWORD}     123

${INCORRECT_EMAIL}          qa0@zwop.biz
${INCORRECT_PASSWORD}       234432

${GMAIL}                    zwoopautomation@gmail.com
${GMAIL_PASSWORD}           zwoopautomation
${GMAIL_FALSE}              zwoopauto@gmail.com

${LONG_NAME}                Mario Bross Bross Bross Bross Bross
${LONG_SURNAME}             Alex William Wilson Karlo Perrish
# ${INVALID_EMAIL}            abczwoop.com
# ${VALID_EMAIL}              abc@zwoop.biz
# ${INCORRECT_EMAIL}          ABC@zwoop.biz
# ${NON-REGISTERED_EMAIL}     xyz@gmail.com

${NAME}                     John
${SURNAME}                  Parker
${MYACC_UPDATEPASSWORD}     test1234
${str1}                     shamiya.patel
${str2}                     +
${str3}                     @zwoop.biz


${demo_str1}                qa.
${demo_str2}                automation
${demo_str3}                @zwoop.biz

#Buttons
${EMAIL_FIELD}          id=email
${PASSWORD_FIELD}       id=password
${NAME_FEILD}           id=name
${SURNAME_FEILD}        id=surname
${ACCESS}               id=submit_button
${MORE}                 id=right_side_button
${CHECKBOX}             id=option_checkbox
${APP_BACK_BUTTON}      accessibility_id=Navigate up
${DONE_BUTTON}          id=action_done
${CLOSE_OR_CLEAR}       id=action_clear
${CARDNUMBER_FIELD}     id=card_number_field
${BUY_NOW_MESSAGE}      I'm placing the order now.
    ...  I'll let you know when it is done or if there is any issue

#Chat bot messages for a new registered user
${TEXT1}     Based on what I know you, I Have selected these options for you
${TEXT2}     have any payment information, please add one.
${IMAGE}     id=product_image
${TEXT4}     Tops_Size: ???
    # ...     Bottoms_Size: ???
${TEXT4}     Bottoms_Size: ???
${TEXT5}     Price:
${TEXT6}     Quantity: 1
${TEXT7}     Shipping To:
${TEXT8}     Payment Method:
${TEXT9}     I can’t proceed without payment information
${BUYNOW_SUCCESS_TEXT1}      placing the order now.
${BUYNOW_SUCCESS_TEXT2}      I'll let you know when it is done or if there is any issue
${BUYNOW_CONFIRMATION}       I have placed your order.
    ...     Please go to order history for further information.

#Product url list - 3,4,5,8,9 are tops and 1,2,6,7 are bottoms
# ${PRODUCT1}     forever21.com/gl/shop/Catalog/Product/app-main/2000135973
${PRODUCT1}     https://www.houseoffraser.co.uk/men/ted-baker-koolio-graphic-printed-t-shirt/d839781.pd#278550437
# ${PRODUCT2}     forever21.com/gl/shop/Catalog/Product/mens-main/2000269175
${PRODUCT2}     zalora.com.hk/nain-denim-high-waist-shorts-%E8%97%8D%E8%89%B2-4780359.html
# ${PRODUCT3}     forever21.com/gl/shop/Catalog/Product/dress/2000143455
${PRODUCT3}     zalora.com.hk/zalora-tailored-knit-polo-shirt-%E8%BB%8D%E8%97%8D%E8%89%B2-4775909.html
# ${PRODUCT4}     zalora.com.hk/zalora-nt-checkered-cotton-long-sleeve-shirt-%E8%BB%8D%E8%97%8D%E8%89%B2-4725301.html
${PRODUCT4}     zalora.com.hk/zalora-pique-tipping-polo-shirt-%E9%BB%91%E8%89%B2-4775777.html
# ${PRODUCT5}     zalora.com.hk/zalora-nt-weaved-textured-linen-short-sleeve-shirt-%E7%99%BD%E8%89%B2-4759885.html
${PRODUCT5}     forever21.com/gl/shop/Catalog/Product/mens-tees-tanks-graphic/2000095308
# ${PRODUCT6}     zalora.com.hk/zalora-cotton-crew-neck-tank-top-%E9%BB%91%E8%89%B2-4775803.html
${PRODUCT6}     forever21.com/gl/shop/Catalog/Product/bottoms/2000200660
# ${PRODUCT7}     zalora.com.hk/cotton-on-zoe-cut-out-shoulder-top-%E9%BB%91%E8%89%B2-4771299.html
${PRODUCT7}     forever21.com/gl/shop/Catalog/Product/bottoms/2000176105
${PRODUCT8}     forever21.com/gl/shop/Catalog/Product/top_blouses/2000149398
${PRODUCT9}     forever21.com/gl/shop/Catalog/Product/mens-tops/2000115684

#Card number
${VISA_CARD}        4111111111111111
${VISA_EXPIRY}      02/2020
${MASTER_CARD}      5105105105105100
${MASTER_EXPIRY}    07/2019

#Phone number(01689)
${UK_PHNO}          867140

#Size
@{SIZE}             XS  S   M   L   XL

#Error messages
${NAME_LENGTH_ERROR}            String length is more then 32 characters
${GMAIL_ACCOUNT_NOT_FOUND}      Couldent find your Google Account

#Results
# ${DEBUG FILE}           /Users/apple/Desktop/Mobile_app_zwoop/automation_debug_logs/DEBUG_LOGS
# ${REPORT FILE}          /Users/apple/Desktop/Mobile_app_zwoop/automation_reports
# ${OUTPUT FILE}          /Users/apple/Desktop/Mobile_app_zwoop/automation_outputs
# ${LOG FILE}             /Users/apple/Desktop/Mobile_app_zwoop/automation_logs
${SAVE_SCREENSHOT}      /Users/apple/Documents/qa-android_automation/test-fail-screenshots/error_screenshot

*** Keywords ***

Launch Application
    [Arguments]         ${APPIUM_SERVER}   ${PLATFORM_VERSION}   ${DEVICE_NAME}
    [Documentation]     Launch AUT and perform various tests
    Log     ${APPIUM_SERVER}
    Log     ${PLATFORM_NAME}
    Log     ${PLATFORM_VERSION}
    Log     ${DEVICE_NAME}
    Log     ${APP}

    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}
    ...                 deviceName=${DEVICE_NAME}    app=${APP}    appPackage=${APP_PACKAGE}  appActivity=${APP_ACTIVITY}
    ...                 unicodeKeyboard=True    resetKeyboard=True  session-override=True   clearSystemFiles=True    noReset=False

Login Test
    [Arguments]         ${field1}     ${value1}     ${field2}      ${value2}
    Input Text          ${field1}   ${value1}
    Input Text          ${field2}   ${value2}
    Click Element       ${ACCESS}
    # Reset Application

Enter info to register
    Check string length     ${NAME}
    Check string length     ${SURNAME}
    [Arguments]     ${field0}    ${value0}    ${field1}   ${value1}     ${field2}   ${value2}   ${field3}   ${value3}
    Input Text      ${field0}    ${value0}
    Input Text      ${field1}    ${value1}
    Click Element   ${ACCESS}
    Input Text      ${field2}    ${value2}
    Input Text      ${field3}    ${value3}

Check element presence
    [Arguments]                     ${locator}
    Page Should Contain Element     ${locator}
    Log   Elements present on the current page

# Enter name and surname
#     Check string length     ${NAME}
#     Check string length     ${SURNAME}
#     [Arguments]     ${field}    ${value}
#     Input Text      ${field}    ${value}

Generate random number
    ${NUM}   Generate Random String  4   [NUMBERS]
    Log To Console  \n Number generated is ${NUM}
    Set Global Variable     ${NO}   ${NUM}

Generate random quantity
    ${QU}   Generate Random String  1   [NUMBERS]
    Log To Console  \n Number generated is ${QU}
    Set Global Variable     ${QUANTITY}   ${QU}

# Generate demo name
#     [Arguments]     ${number}
#     ${NAME}        Catenate    ${demo_str1}${number}
#     Log To Consloe      \n Sign-up name is ${NAME}
#     Set Global Variable     ${DEMO_NAME}    ${NAME}

Generate demo surname
    [Arguments]     ${number}
    ${SURNAME}     Catenate    ${demo_str2}${number}
    Log To Console      \n Sign-up surname is ${SURNAME}
    Set Global Variable     ${DEMO_SURNAME}    ${SURNAME}

Generate demo registration email
    [Arguments]     ${number}
    ${EMAIL}    Catenate    ${demo_str1}${demo_str2}${number}${demo_str3}
    Log To Console  \n Email is ${EMAIL}
    Set Global Variable     ${DEMO_REGISTER_EMAIL}    ${EMAIL}

Generate random name/surname for sign-up
    ${NAME}     Generate Random String      7   [LETTERS]
    Log To Console  \n Sign-up name/surname is ${NAME}
    Set Global Variable     ${SIGN-UP_NAME}     ${NAME}

Generate random recipientname/nickname
    ${NAME}     Generate Random String      7   [LETTERS]
    Log To Console  \n Recipient name / nickname is ${NAME}
    Set Global Variable     ${RECIPIENT-NICK_NAME}     ${NAME}

Generate email
#Here a new email is generated everytime for fresh registration test
    [Arguments]     ${number}
    ${EMAIL}    Catenate    ${str1}${str2}${number}${str3}
    Log To Console  \n Email is ${EMAIL}
    Set Global Variable     ${NEW_EMAIL_REGISTER}    ${EMAIL}

Generate random valid password
    ${V_PASSWORD}     Generate Random String  6  [NUMBERS]
    Log To Console  \nAttempt to login with randomly generated valid password ${V_PASSWORD}
    Set Global Variable    ${VALID_PASSWORD}     ${V_PASSWORD}

Generate random invalid password
    ${INV_PASSWORD}     Generate Random String  5  [NUMBERS]
    Log To Console  \nAttempt to login with randomly generated invalid password ${INV_PASSWORD}
    Set Global Variable    ${INVALID_PASSWORD}     ${INV_PASSWORD}

Check string length
    [Arguments]     ${string}
     ${LENGTH_NAME}     Get Length     ${string}
     Log   ${LENGTH_NAME}
    Run Keyword If   ${LENGTH_NAME}<=32
    ...    Log   Variable length matched
    ...    ELSE
    ...    Fatal Error  msg=${NAME_LENGTH_ERROR}

Generate address phone number
    ${ph_no}   Generate Random String  8   [NUMBERS]
    Log To Console  \n Phone number is ${ph_no}
    Set Global Variable     ${PH.NO}   ${ph_no}

Generate flat/house/building/street number for address
    ${house_no}     Generate Random String  2  [NUMBERS]
    Log To Console  \nEnter ${house_no} as flat/house/building/street number
    Set Global Variable    ${FHBS_NUM}     ${house_no}

Generate flat/house/building/street name for address
    ${house_name}     Generate Random String  5  [LETTERS]
    Log To Console  \nEnter ${house_name} as flat/house/building/street name
    Set Global Variable    ${FHBS_NAME}     ${house_name}

Generate a random CVV
    ${cvv}   Generate Random String  3   [NUMBERS]
    Log To Console  \n CVV for the card is ${cvv}
    Set Global Variable     ${CVV}   ${cvv}

Add shipping address
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT7}')]     10
    Click Element   xpath=//android.widget.TextView[contains(@text,'${TEXT7}')]
    Wait Until Element Is Visible   id=add_address_button   20
    Click Element   id=add_address_button

    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Input Text      id=address_title    miaddress${NO}
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Input Text      id=address_title    j1address${NO}
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Input Text      id=address_title    xperiaaddress${NO}
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Input Text      id=address_title    primeaddress${NO}
    ...     ELSE    Log     Device name didnt match

    Click Element   id=drop_down_arrow_country
    Click Element   id=search_button

#For UK, uncoment once dev side is ready
    # Run Keyword If      '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)' 'Xperia Z5 Premium Dual'
    # ...     Input Text  id=search_src_text    United Kingdom
    # ...     ELSE        Input Text      id=search_src_text    Hong Kong
    Input Text      id=search_src_text    Hong Kong
    Click Element   id=country_name

    # Run Keyword If      '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)' 'Xperia Z5 Premium Dual'
    # ...     Input Text      id=phone_number     ${UK_PHNO}
    # ...     ELSE        Input Text      id=phone_number     ${PH.NO}
    Input Text      id=phone_number     ${PH.NO}
    Input Text      id=recipient_name   ${RECIPIENT-NICK_NAME}
    Input Text      xpath=//android.widget.EditText[contains(@text,'flatHouseNumber')]      ${FHBS_NUM}
    Input Text      xpath=//android.widget.EditText[contains(@text,'floorNumber')]          ${FHBS_NUM}

    # Run Keyword If      '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)' 'Xperia Z5 Premium Dual'
    # ...     Input Text      xpath=//android.widget.EditText[contains(@text,'flatHouseNumber')]      WF16
    # ...     ELSE    Input Text      xpath=//android.widget.EditText[contains(@text,'flatHouseNumber')]      ${HOUSE_NUM}

    # Run Keyword If      '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)' 'Xperia Z5 Premium Dual'
    # ...     Input Text      xpath=//android.widget.EditText[contains(@text,'floorNumber')]      -
    # ...     ELSE    Input Text      xpath=//android.widget.EditText[contains(@text,'floorNumber')]      ${HOUSE_NUM}

    Run Keyword If  '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Swipe   230     710     230     205
    ...     ELSE    Swipe  550  1170    550     200     1000

    Wait Until Element Is Visible   xpath=//android.widget.EditText[contains(@text,'houseBuildingNumber')]
    Input Text      xpath=//android.widget.EditText[contains(@text,'houseBuildingNumber')]  ${FHBS_NUM}

    Wait Until Element Is Visible   xpath=//android.widget.EditText[contains(@text,'houseBuildingName')]
    Input Text      xpath=//android.widget.EditText[contains(@text,'houseBuildingName')]    ${FHBS_NAME}

    Input Text      xpath=//android.widget.EditText[contains(@text,'streetNumber')]         ${FHBS_NUM}

    Input Text      xpath=//android.widget.EditText[contains(@text,'streetName')]           ${FHBS_NAME}

    Click Element   ${DONE_BUTTON}
    # Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Address saved')]    10
    Log To Console  \nShipping address added successfully

Generate random month/year for card expiry
    ${NUM}   Generate Random String  1   [NUMBERS]
    Log To Console  \n Month generated is ${NUM}
    Set Global Variable     ${MONTH}   ${NUM}

Generate random year for card expiry
    ${NUM}   Generate Random String  1   [NUMBERS]1-9
    Log To Console  \n Year generated is ${NUM}
    Set Global Variable     ${YEAR}   ${NUM}

Add a card
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT8}')]
    Click Element   xpath=//android.widget.TextView[contains(@text,'${TEXT8}')]
    Click Element   id=add_card_button
    Input Text      id=nickname     ${RECIPIENT-NICK_NAME}
    Input Text      id=name_on_card     card${NO}
    Input Text      id=card_number      ${VISA_CARD}
    Generate random month/year for card expiry
    Click Element   id=expiry_month
    Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'${MONTH}')]
    Generate random year for card expiry
    Click Element   id=expiry_year
    Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'${YEAR}')]
    # Input Text      id=expiry_date      ${VISA_EXPIRY}
    Input Text      id=cvv              ${CVV}
    Click Element   ${DONE_BUTTON}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT8}')]
    Log To Console      First card added successfully

Chat bot messages received
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Based on what I know you, I Have selected these options for you.')]     10

    ${message1}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT1}')]
    # Page Should Contain Text    ${TEXT1}    loglevel=INFO
    Log To Console   \n '${message1}' - received in chat room

    ${message2}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT2}')]
    # Page Should Contain Text    ${TEXT2}               loglevel=INFO
    Log To Console  \n '${message2}' - received in chat room

    Capture Page Screenshot     /Users/apple/Documents/qa-android-automation/test_screenshot/product_image1.png
    Page Should Contain Element    ${IMAGE}            loglevel=INFO
    Log To Console  \n '${IMAGE}' - received in chat room

    ${message4}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT4}')]
    # Page Should Contain Text    ${TEXT4}               loglevel=INFO
    Log To Console  \n '${message4}' - received in chat room

    ${message5}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT5}')]
    # Page Should Contain Text    ${TEXT5}               loglevel=INFO
    Log To Console  \n '${message5}' - received in chat room

    ${message6}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT6}')]
    # Page Should Contain Text    ${TEXT6}               loglevel=INFO
    Log To Console  \n '${message6}' - received in chat room

    ${message7}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT7}')]
    # Page Should Contain Text    ${TEXT7}               loglevel=INFO
    Log To Console  \n '${message7}' - received in chat room

    ${message8}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT8}')]
    # Page Should Contain Text    ${TEXT8}               loglevel=INFO
    Log To Console  \n '${message8}' - received in chat room

    ${message9}     Get Text    xpath=//android.widget.TextView[contains(@text,'${TEXT9}')]
    # Page Should Contain Text    ${TEXT9}                loglevel=INFO
    Log To Console  \n '${message9}' - received in chat room


