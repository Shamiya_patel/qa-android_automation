*** Settings ***
Documentation     This test suite helps to test android mobile sign-in.

Library         AppiumLibrary
Resource        android_resource.robot
# Suite Setup     Launch Application
Suite Teardown  Close Application

*** Variables ***
# ${SAVE_SCREENSHOT}      /Users/apple/Desktop/Study_scripts/Android/automation_screenshots
# ${LOG_FILE}             /Users/apple/Desktop/Study_scripts/Android/automation_logs

*** Test Cases ***
TEST-145 - Mobile app: Sign-in
    # Log       ${LOG_FILE}
    Log     "Step 1: Launch Zwoop application"
    Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}

    Log     "Step 2: Sign-in with empty credentials"
    Click Element   ${EMAIL_FIELD}
    Click Element   ${PASSWORD_FIELD}
    Click Element   ${ACCESS}
    Reset Application
    #Add error for empty credential sign-in [PENDING]

    Log     "Step 3: Sign-in with valid email and empty password / empty email and valid password"
    Input Text  ${EMAIL_FIELD}      ${VALID_EMAIL}
    Click Element   ${ACCESS}
    Reset Application
    Generate random valid password
    Input Text  ${PASSWORD_FIELD}       ${VALID_PASSWORD}
    # Hide Keyboard
    Click Element   ${ACCESS}
    Reset Application
    # Add error for empty credential sign-in [PENDING]

    Log     "Step 4: Sign-in using incorrect email format and correct password format & correct email format and incorrect password"
    Generate random valid password
    Generate random invalid password
    Sign-in Test      ${EMAIL_FIELD}     ${INVALID_EMAIL}      ${PASSWORD_FIELD}      ${VALID_PASSWORD}
    Sign-in Test      ${EMAIL_FIELD}     ${VALID_EMAIL}        ${PASSWORD_FIELD}      ${INVALID_PASSWORD}
    #Add error for invalid inputs [PENDING]

    Log     "Step 5: Sign-in with valid email and incorrect password & incorrect email and valid password(mismatch case)"
    Generate random valid password
    Sign-in Test      ${EMAIL_FIELD}      ${VALID_EMAIL}          ${PASSWORD_FIELD}       ${INCORRECT_PASSWORD}
    Sign-in Test      ${EMAIL_FIELD}      ${INCORRECT_EMAIL}      ${PASSWORD_FIELD}       ${VALID_PASSWORD}
    #Add errors for incorrect inputs [PENDING]

    Log     "Step 6: Sign-in with valid email and password"
    Generate random valid password
    Sign-in Test      ${EMAIL_FIELD}      ${VALID_EMAIL}          ${PASSWORD_FIELD}       ${VALID_PASSWORD}
    #Add display landing page for successful sign-in case PENDING

    Log     "Step 7: Sign-in with non-registered email"
    Generate random valid password
    Sign-in Test      ${EMAIL_FIELD}      ${NON-REGISTERED_EMAIL}      ${PASSWORD_FIELD}       ${VALID_PASSWORD}
    #Add error for sign-in with non-registered email [PENDING]

    Log     "Step 8: For non-registered email, check if name and surname fields appear to began with registration"
    Input Text      ${EMAIL_FIELD}        ${VALID_EMAIL}
    Generate random valid password
    Input Text      ${PASSWORD_FIELD}     ${VALID_PASSWORD}
    Click Element   text_input_password_toggle
    Click Element   ${ACCESS}
    Check element presence    ${NAME_FEILD}
    Check element presence    ${SURNAME_FEILD}
    #Add verify landing page for successful registration

    Log     "Step 9: Uninstall app once test ends"
    Remove Application  ${APP_PACKAGE}

# [TEST-145] - Step 6: Login with CORRECT EMAIL address and INCORRECT PASSWORD & INCORRECT EMAIL and CORRECT PASSWORD
#     [Documentation]     Need to locate error message under both fields for more effective test. Use wrong password with correct email and wrong email address with correct password to login.
#     Reset Application
#     Enter valid email
#     Enter incorrect password
#     Select access
#     ERROR EMAIL and PASSWORD do not match
#     Reset Application
#     Enter incorrect EMAIL
#     Enter valid password
#     Select access
#     ERROR EMAIL and PASSWORD do not match

*** Keywords ***
# Select access
#     Click Element   id=submit_button

# Empty credentials error message
#     Element Text Should Be      id=textinput_error  Email a mandatory field.

# INVALID EMAIL address error message
#     Element Text Should Be      id=textinput_error  Email format is not correct.

# INVALID password format error message
#     Element Text Should Be      id=textinput_error  Password must be at least 6 digit long.

# Empty email error message
#     Element Text Should Be      id=textinput_error  Email is missing.

# Empty password error message
#     Element Text Should Be      id=textinput_error  Password is missing.

# ERROR EMAIL and PASSWORD do not match
#     Element Text Should Be      id=textinput_error  Email and password do not match.






