*** Settings ***
Documentation		This file contain all the reusable keywords and variables for automation
Library     AppiumLibrary
Library     BuiltIn
Library     String
Library     Screenshot  ${SAVE_SCREENSHOT}

*** Variables ***
#Appium Configuration
${APP}                  /Users/apple/apk/zwoop-0.01-debug.apk
${APPIUM_SERVER}        http://localhost:${PORT}/wd/hub
${PLATFORM_NAME}        Android
${APP_PACKAGE}         com.zwoop.android
${APP_ACTIVITY}        com.zwoop.android.ui.splash.SplashActivity


#Test Data
#Email
${INVALID_EMAIL}            zwoop.com
${NON-REGISTERED_EMAIL}     xyz@gmail.com
${VALID_EMAIL}              zwoopautomation@gmail.com
${INCORRECT_EMAIL}          ABC@zwoop.biz
${GMAIL}                    zwoopautomation@gmail.com
${GMAIL_FALSE}              zwoopauto@gmail.com
${LONG_NAME}                Mario Bross Bross Bross Bross Bross
${LONG_SURNAME}             Alex William Wilson Karlo Perrish
# ${INVALID_EMAIL}            abczwoop.com
# ${VALID_EMAIL}              abc@zwoop.biz
# ${INCORRECT_EMAIL}          ABC@zwoop.biz
# ${NON-REGISTERED_EMAIL}     xyz@gmail.com

#Entered wrong password for the email(mismatch case)
# ${INVALID_PASSWORD}     0087
${INCORRECT_PASSWORD}   234432
${GMAIL_PASSWORD}       zwoopautomation

${NAME}                 John
${SURNAME}              Parker

#Buttons
${EMAIL_FIELD}          id=email
${PASSWORD_FIELD}       id=password
${NAME_FEILD}           id=name
${SURNAME_FEILD}        id=surname
${ACCESS}               id=submit_button
${MORE}                 id=right_side_button
${CHECKBOX}             id=option_checkbox
${APP_BACK_BUTTON}      accessibility_id=Navigate up
${DONE_BUTTON}          id=action_done
${CLOSE_OR_CLEAR}       id=action_clear
${CARDNUMBER_FIELD}     id=card_number_field
${BUY_NOW_MESSAGE}      I'm placing the order now.
    ...  I'll let you know when it is done or if there is any issue

#Error messages
${NAME_LENGTH_ERROR}            String length is more then 32 characters
${GMAIL_ACCOUNT_NOT_FOUND}      Couldent find your Google Account

#Results
# ${DEBUG FILE}           /Users/apple/Desktop/Mobile_app_zwoop/automation_debug_logs/DEBUG_LOGS
# ${REPORT FILE}          /Users/apple/Desktop/Mobile_app_zwoop/automation_reports
# ${OUTPUT FILE}          /Users/apple/Desktop/Mobile_app_zwoop/automation_outputs
# ${LOG FILE}             /Users/apple/Desktop/Mobile_app_zwoop/automation_logs
${SAVE_SCREENSHOT}      /Users/apple/Documents/qa-android_automation/test-fail-screenshots/error_screenshot


*** Keywords ***

Launch Application
    [Arguments]         ${APPIUM_SERVER}   ${PLATFORM_VERSION}   ${DEVICE_NAME}
    [Documentation]     Launch AUT and perform various actions
    Log     ${APPIUM_SERVER}
    Log     ${PLATFORM_NAME}
    Log     ${PLATFORM_VERSION}
    Log     ${DEVICE_NAME}
    Log     ${APP}

    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}
    ...                 deviceName=${DEVICE_NAME}    app=${APP}    appPackage=${APP_PACKAGE}  appActivity=${APP_ACTIVITY}
    ...                 unicodeKeyboard=True    resetKeyboard=True

Sign-in Test
    [Arguments]         ${field1}     ${value1}     ${field2}      ${value2}
    Input Text          ${field1}   ${value1}
    Input Text          ${field2}   ${value2}
    Click Element       ${ACCESS}
    Reset Application

Enter info to register
    Check string length     ${NAME}
    Check string length     ${SURNAME}
    [Arguments]     ${field0}    ${value0}    ${field1}   ${value1}     ${field2}   ${value2}   ${field3}   ${value3}
    Input Text      ${field0}    ${value0}
    Input Text      ${field1}    ${value1}
    Click Element   ${ACCESS}
    Input Text      ${field2}    ${value2}
    Input Text      ${field3}    ${value3}

Check element presence
    [Arguments]                     ${locator}
    Page Should Contain Element     ${locator}
    Log   Elements present on the current page

Enter name and surname
    Check string length     ${NAME}
    Check string length     ${SURNAME}
    [Arguments]     ${field}    ${value}
    Input Text      ${field}    ${value}

Generate random valid password
    ${V_PASSWORD}     Generate Random String  6  [NUMBERS]
    Log To Console  \nAttempt to login with randomly generated valid password ${V_PASSWORD}
    Set Global Variable    ${VALID_PASSWORD}     ${V_PASSWORD}

Generate random invalid password
    ${INV_PASSWORD}     Generate Random String  5  [NUMBERS]
    Log To Console  \nAttempt to login with randomly generated invalid password ${INV_PASSWORD}
    Set Global Variable    ${INVALID_PASSWORD}     ${INV_PASSWORD}

Check string length
    [Arguments]     ${string}
     ${LENGTH_NAME}     Get Length     ${string}
     Log   ${LENGTH_NAME}
    Run Keyword If   ${LENGTH_NAME}<=32
    ...    Log   Variable length matched
    ...    ELSE
    ...    Fatal Error  msg=${NAME_LENGTH_ERROR}