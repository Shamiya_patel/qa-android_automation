*** Settings ***
Library             AppiumLibrary
Resource            android_resource.robot
Suite Teardown      Close Application
Library             Screenshot       ${SAVE_SCREENSHOT}

*** Variables ***
#Test Data
# #LOG, results and screenshots
# ${SAVE_SCREENSHOT}      /Users/apple/Desktop/Study_scripts/Android/automation_screenshots
# ${LOG_FILE}             /Users/apple/Desktop/Study_scripts/Android/automation_logs

*** Test Cases ***
TEST-144 - Mobile-app: Email registration

    Log     "Step 1: Launch zwoop application"
    Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}

    Log     "Step 2: Skip a mandatory field(name) and register"
    Generate random valid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}   ${VALID_PASSWORD}   ${NAME_FEILD}  ${EMPTY}  ${SURNAME_FEILD}  ${SURNAME}
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    # Add error for empty name

    Log     "Step 3: Skip a mandatory field(surname) and register"
    Generate random valid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}  ${VALID_PASSWORD}  ${NAME_FEILD}  ${NAME}  ${SURNAME_FEILD}  ${EMPTY}
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    #Add error for empty surname

    Log     "Step 4: Skip a mandatory field(email) and register / (password) and register"
    Generate random valid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}  ${VALID_PASSWORD}  ${NAME_FEILD}  ${NAME}  ${SURNAME_FEILD}  ${SURNAME}
    Clear Text  ${EMAIL_FIELD}
    # Hide Keyboard
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    #Add error for empty email
    Generate random valid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}  ${VALID_PASSWORD}  ${NAME_FEILD}  ${NAME}  ${SURNAME_FEILD}  ${SURNAME}
    Clear Text  ${PASSWORD_FIELD}
    # Hide Keyboard
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    #Add error for empty password

     Log     "Step 5: Enter long name with more than 32 character and register"
    Generate random valid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}  ${VALID_PASSWORD}  ${NAME_FEILD}  ${LONG_NAME}  ${SURNAME_FEILD}  ${SURNAME}
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    #Add long name error

    Log     "Step 6: Enter long surname with more than 32 characters and register"
    Generate random valid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}  ${VALID_PASSWORD}  ${NAME_FEILD}  ${NAME}  ${SURNAME_FEILD}  ${LONG_SURNAME}
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    #Add long surname error

    Log     "Step 7: Enter information in incorrect format and select create"
    Generate random invalid password
    Enter info to register  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}  ${INVALID_PASSWORD}  ${NAME_FEILD}  ${NAME}  ${SURNAME_FEILD}  ${SURNAME}
    Click Element   id=checkbox_toc
    Click Element   ${ACCESS}
    Reset Application
    #Add error for incorrect email format
    #Add error for incorrect password format

    Log     "Step 8: Register with new email (non-registered email-> successful case)"
    Input Text                      ${EMAIL_FIELD}      ${NON-REGISTERED_EMAIL}
    Generate random valid password
    Input Text                      ${PASSWORD_FIELD}   ${VALID_PASSWORD}
    Click Element                   text_input_password_toggle
    Click Element                   ${ACCESS}
    #Error under email field 'email doesnt exist, register here' PENDING
    #Redirect to registration page PENDING
    Check element presence          ${NAME_FEILD}
    Check element presence          ${SURNAME_FEILD}
    Enter name and surname          ${NAME_FEILD}       ${NAME}
    Enter name and surname          ${SURNAME_FEILD}    ${SURNAME}
    Click Element       id=checkbox_toc
    Click Element                   ${ACCESS}
    Reset Application

    Log     "Step 9: Remove app once test ends"
    Remove Application  ${APP_PACKAGE}


*** Keywords ***

# Login with valid Email Address
#     [Arguments]         ${fieldl1}     ${value1}     ${field2}      ${value2}
#     Click Element       ${EMAIL_FIELD}
#     Input Text          ${EMAIL_FIELD}   ${VALID_EMAIL}
#     Click Element       ${PASSWORD_FIELD}
#     Input Text          ${PASSWORD_FIELD}   ${VALID_PASSWORD}
