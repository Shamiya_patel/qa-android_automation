*** Variables ***
#Input data
# Keywords list to be used in search
${k1}   Skirt
${k2}   Jeans
${k3}   Red Dress
${k0}   royce grace 34b

@{searchList}   Dress   Blue shirt  Skirts long
#ERROR
# ${sameproductinWatchloisterror}     Product is already in watchlist

#CREDENTIALS
${mail}     qa.zwoopautomation+yHppI@zwoop.biz

*** Keywords ***
Verify shop page
    Page Should Contain Element     ${notificationIcon}
    Page Should Contain Element     ${accountIcon}

    Run Keyword If      '${PLATFORM_VERSION}'>='7.0'
    ...     Page Should Contain Element     ${shopTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Page Should Contain Element     ${primeshopTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Page Should Contain Element     ${shopTab}
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nShop tab present on page

    Run Keyword If      '${PLATFORM_VERSION}'>='7.0'
    ...     Page Should Contain Element     ${watchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Page Should Contain Element     ${primewatchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Page Should Contain Element     ${primewatchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Page Should Contain Element     ${watchlistTab}
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nWatchlist tab present on page

    Page Should Contain Element     ${searchIcon}
    Page Should Contain Element     ${searchBar}
    Page Should Contain Element     ${searchTF}
    # ${element}  Get Text    ${primeshopTab}
    # Log To Console  \nElement is '${element}'

    Wait Until Element Is Visible   ${emptyshopIcon}  10
    Log To Console  \nVerified page content successfully, all elements visible on the page

Go to watchlist tab
    Run Keyword If      '${PLATFORM_VERSION}'>='7.0'
    ...     Click Element     ${watchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click Element     ${primewatchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click Element     ${primewatchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click Element     ${watchlistTab}
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nNavigate to Watchlist tab

    Page Should Contain Element     ${watchlistitemDetailList}
    Log To Console  \nRedirected to watchlist page

Verify once best price is received
    Page Should Contain Element     ${bestpricefoundPH}
    Log To Console  \nBest price received

Verify clear search
    Input Text      ${searchBar}    ${k1}
    Press Keycode   66
    Wait Until Element Is Visible   ${searchresultImage}    30
    Log To Console  \nSearch results found
    Click Element   ${clearsearchTF}
    Log To Console  \nSearch cleared successfully

Search for any keyword and verify
    Input Text      ${searchTF}    ${k2}
    Press Keycode   66
    Page Should Contain Element     ${searchresultImage}
    Page Should Contain Element     ${searchresultText}
    Page Should Contain Element     ${searchresultCurrency}
    Page Should Contain Element     ${searchresultPricerange}
    Wait Until Element Is Visible   ${searchresultImage}    30
    Log To Console  \nSearch results found successfully

Scroll through search results
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200
    # ...     Swipe  220  1170    550     200     1000
    # ...     ELSE    Swipe  550  1170    550     200     1000
    Log To Console  \nScrolling through search result list
    Click Element   ${searchresultImage}
    Log To Console  \nSelected a random product from the search results
    Wait Until Element Is Visible   ${addtowatchlitstButton}    10
    Log To Console  \nRedirected to product detail page and add to watchlist button visible
    Click Element   ${appbackButton}
    Log To Console  \nRedirected back to Shop page

Search product and add to watchlist
    Click Element  ${clearsearchTF}
    Input Text      ${searchTF}    ${k0}
    Press Keycode   66
    Wait Until Element Is Visible   ${searchresultImage}    30
    Log To Console  \nSearch results found

    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200
    Log To Console  \nScrolling through search result list

    Click Element   ${searchresultImage}
    Log To Console  \nClicked any random product
    Wait Until Element Is Visible   ${addtowatchlitstButton}    10
    Log To Console  \nAdd to watchlist button visible
    Sleep   1
    Click Element   ${addtowatchlitstButton}
    Log To Console  \nClicked add to watchlist
    Wait Until Element Is Visible   ${watchlistbuynowButton}    10
    Log To Console  \nAdded product to watchlist

Add same product to watchlist
    Run Keyword If      '${PLATFORM_VERSION}'>='7.0'
    ...     Click Element     ${shopTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click Element     ${primeshopTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click Element     ${primeshopTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click Element     ${shopTab}
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nRedirected back to shop page

    Input Text      ${searchTF}    ${k0}
    Press Keycode   66
    Wait Until Element Is Visible   ${searchresultImage}    30
    Log To Console  \nSearch results found

    Click Element   ${searchresultImage}
    Log To Console  \nClicked any random product
    Sleep   2
    Wait Until Element Is Visible   ${addtowatchlitstButton}    15
    Log To Console  \nVisible
    Click Element   ${addtowatchlitstButton}
    Log To Console  \n Clicked add to watchlist button

    Wait Until Element Is Visible   ${sameproducterror}    10
    ${error}      Get Text    ${sameproducterror}
    Log To Console  \nExpected error is '${error}'

    Click Element   ${appbackButton}
    Log To Console  \nRedirected back to shop page

Delete watchlist item after adding
    Run Keyword If      '${PLATFORM_VERSION}'>='7.0'
    ...     Click Element     ${watchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click Element     ${primewatchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click Element     ${primewatchlistTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click Element     ${watchlistTab}
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nNavigate to Watchlist tab

    # Page Should Contain Element     ${watchlistitemDetailList}
    # Long Press   ${watchlistitemDetailList}
    # Click Element   ${delete}
    # Wait Until Element Is Visible   ${watchlistDeleteerror}  10
    # ${error}    Get Text    ${watchlistDeleteerror}
    # Log To Console  \n'${error}' successfully
    # Page Should Not Contain Element     ${watchlistitemDetailList}
    # Log To Console  \nWatchlist item deleted

    Page Should Contain Element     ${deletewatchlistseconditem}
    Long Press   ${deletewatchlistseconditem}
    Click Element   ${delete}
    Wait Until Element Is Visible   ${watchlistitemdeletedmessage}  10
    ${error}    Get Text    ${watchlistitemdeletedmessage}
    Log To Console  \n'${error}' successfully
    Page Should Not Contain Element     ${deletewatchlistseconditem}
    Log To Console  \nWatchlist item deleted

Share product for non log-in user
    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}    autoGrantPermissions=True
    ...                 deviceName=${DEVICE_NAME}    app=${gmailAPP}    appPackage=${gmailApp_Package}  appActivity=${gmailApp_Activity}
    ...                 unicodeKeyboard=True    noReset=True
    # http://localhost:4726/wd/hub    platformName=Android    platformVersion=7.1.1    autoGrantPermissions=True
    # ...                 deviceName=an_htc_ueleven    app=${gmailAPP}    appPackage=${gmailApp_Package}  appActivity=${gmailApp_Activity}
    # ...                 unicodeKeyboard=True    noReset=True
    Log To Console  \nLaunched gmail app, build used for testing is located at '${gmailAPP}'
    Sleep   2
    Click Element   ${gmailMenuButton}

    Wait Until Element Is Visible   ${chooseDrafts}     10
    Run Keyword If      '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click Element   ${lgDrafts}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_oneplus_oneplusfive'
    ...     Click Element   ${lgDrafts}
    ...     ELSE    Click Element   ${chooseDrafts}
    Log To Console  \nClicked drafts

    # Wait Until Element Is Visible   ${chooseDrafts}     10
    # Click Element   ${chooseDrafts}
    # Log To Console  \nClicked drafts
    Sleep   1
    Run Keyword If      '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Click A Point   x=685   y=530
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click A Point   x=370   y=267
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click A Point   x=765   y=745
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click A Point   x=690   y=495
    ...     ELSE IF     '${DEVICE_NAME}'=='an_oneplus_oneplusfive'
    ...     Click A Point   x=560   y=335
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nClicked first draft from list

    Wait Until Element Is Visible   ${openURL}  10
    # Get Matching Xpath Count    //android.view.View[contains(@index,'0')]
    ${url}  Get Text    ${openURL}
    Log To Console  \nProduct url is '${url}'
    Click Element   ${openURL}
    Log To Console  \nClicked URL

    Wait Until Element Is Visible   ${gmailsharemenuButton}     10
    Click Element   ${gmailsharemenuButton}
    Log To Console  \nClicked three dots to access share menu

    Wait Until Element Is Visible   ${chooseZwoopshare}     10
    Click Element   ${chooseZwoopshare}
    Log To Console  \nClicked Zwoop share

    Wait Until Element Is Visible   ${zwoopLogo}    10
    Log To Console  \nRedirected to login page

Open gamil to share product
    Press Keycode   3
    Sleep   2
    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}    autoGrantPermissions=True
    ...                 deviceName=${DEVICE_NAME}    app=${gmailAPP}    appPackage=${gmailApp_Package}  appActivity=${gmailApp_Activity}
    ...                 unicodeKeyboard=True    noReset=True
    Log To Console  \nLaunched gmail app, build used for testing is located at '${gmailAPP}'
    Sleep   2
    Click Element   ${gmailMenuButton}
    Log To Console  \nClicked gmail menu
    Sleep   2
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe   650     2425    650     2235
    ...     ELSE    Log     \nDevcie other then note

    Wait Until Element Is Visible   ${chooseDrafts}     10
    Run Keyword If      '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click Element   ${lgDrafts}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_oneplus_oneplusfive'
    ...     Click Element   ${lgDrafts}
    ...     ELSE    Click Element   ${chooseDrafts}
    Log To Console  \nClicked drafts
    Sleep   1
    Run Keyword If      '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Click A Point   x=685   y=530
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click A Point   x=370   y=267
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click A Point   x=765   y=745
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click A Point   x=690   y=495
    ...     ELSE IF     '${DEVICE_NAME}'=='an_oneplus_oneplusfive'
    ...     Click A Point   x=560   y=335
    ...     ELSE    Log To Console  \nDevice isnt mentioned in above list
    Log To Console  \nChoose first draft from list

    Wait Until Element Is Visible   ${openURL}  10
    ${url}  Get Text    ${openURL}
    Log To Console  \nProduct url is '${url}'

    Run Keyword If      '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Click Element   ${lgopenURL}
    ...     ELSE    Click Element   ${openURL}
    # Click Element   ${openURL}
    Log To Console  \nClicked URL

    Wait Until Element Is Visible   ${gmailsharemenuButton}     10
    Click Element   ${gmailsharemenuButton}
    Log To Console  \nClicked three dots to access share menu

    Wait Until Element Is Visible   ${chooseZwoopshare}     10
    Click Element   ${chooseZwoopshare}
    Log To Console  \nClicked Zwoop share
    Sleep   5

    Page Should Contain Element     ${addtowatchlitstButton}
    ${button1}     Get Text    ${addtowatchlitstButton}
    Log To Console  \n'${button1}' button present
    Page Should Contain Element     ${buynowButton}
    ${button2}     Get Text    ${buynowButton}
    Log To Console  \n'${button2}' button present
    Log To Console  \nButtons visible, recdirected successfully to product page

    Click Element   ${addtowatchlitstButton}
    Log To Console  \nClicked add to watchlist

    Wait Until Element Is Visible   ${buynowButton}    20
    Log To Console  \nAdded product to watchlist
    Sleep   2

    Wait Until Element Is Visible   ${watchlistitemDetailList}  10
    Log To Console  \nList watchlist item visible
    Long Press   ${watchlistitemDetailList}

    Wait Until Element Is Visible   ${delete}   10
    Click Element   ${delete}
    Wait Until Element Is Visible   ${watchlistitemdeletedmessage}  10
    ${error}    Get Text    ${watchlistitemdeletedmessage}
    Log To Console  \n'${error}' successfully
    Page Should Not Contain Element     ${watchlistitemDetailList}
    Log To Console  \nWatchlist item deleted




