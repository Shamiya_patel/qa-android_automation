*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            profilepage_locators.robot

Resource            TEST-facebook-registerlogin-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app, choose register with facebook and choose device back button
    Cancel facebook registration

2. Register with facebook
    First time registration with facebook
    Verify registration with facebook

3. Login with facebook
    Facebook login
    Verify login with facebook