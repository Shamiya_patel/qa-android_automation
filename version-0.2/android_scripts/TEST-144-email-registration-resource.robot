*** Variables ***
#Input data
${firstname}        Jannet William QA android
${lastName}         Moore leone Walker

${invalidName}       myname-isveryyyyyy123456longggggg7
${invalidSurname}    mysurnameis-veryyy11223longgggggg3

${wrongemail1}      amanda@.com
${wrongemail2}      aryadeph.com

${string1}          qa.zwoopautomation
${string2}          +
${zwoopmail}        @zwoop.biz

${shortpassword}         ab12

#Error Messages
${emptyregisternameError}          Name is a mandatory field
${emptyregistersurnameError}       Surname is a mandatory field
${emptyregisteremailError}         Email is a mandatory field
# ${emptyregisterpasswordError}      Password is a mandatory field

${invalidnameformatError}          Name can't contain any digits or special characters
${invalidsurnameformatError}       Surname can't contain any digits or special characters
${invalidemailformatError}         Email format is not correct
${invalidpasswordformatError}      Password must be at least 6 digits long

${useExistingemailtoregister}      User exists. Please log in.


*** Keywords ***
Choose create account link
    Click Element   ${registernewaccountButton}

Check register page content
    Page Should Contain Element     ${inappbacktoLoginpage}
    Page Should Contain Element     ${zwoopLogo}
    Page Should Contain Element     ${enternameField}
    Page Should Contain Element     ${entersurnameField}
    Page Should Contain Element     ${enteremailField}
    Page Should Contain Element     ${enterpasswordField}
    Page Should Contain Element     ${register/loginButton}
    Page Should Contain Element     ${loginwithGoogle}
    Page Should Contain Element     ${loginwithFacebook}
    Log To Console   \nVerified register page content

Empty registration
    [Arguments]         ${locator}   ${expectmessage1}    ${text1}   ${expectmessage2}  ${text2}  ${expectmessage3}   ${text3}     ${expectmessage4}
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  760     240     600
    ...     ELSE     Log    Device isnt J1
    Wait Until Element Is Visible    ${locator}     10
    Click Element       ${locator}

    Wait Until Element Is Visible    ${expectmessage1}
    ${warningmessage}    Get Text    ${expectmessage1}
    Should Be Equal As Strings  ${text1}    ${warningmessage}
    Log To Console  \nError message is '${warningmessage}'

    Wait Until Element Is Visible    ${expectmessage2}
    ${warningmessage}    Get Text    ${expectmessage2}
    Should Be Equal As Strings  ${text2}    ${warningmessage}
    Log To Console  \nError message is '${warningmessage}'

    Wait Until Element Is Visible    ${expectmessage3}
    ${warningmessage}    Get Text    ${expectmessage3}
    Should Be Equal As Strings  ${text3}    ${warningmessage}
    Log To Console  \nError message is '${warningmessage}'

    Wait Until Element Is Visible    ${expectmessage4}
    ${warningmessage}    Get Text    ${expectmessage4}
    # Should Be Equal As Strings  ${text}    ${warningmessage}
    Log To Console  \nError message is '${warningmessage}'
    Reset Application

Input credentials
    [Arguments]         ${name}     ${surname}     ${email}      ${password}   ${locator}   ${expectmessage}
    Input Text          ${enternameField}       ${name}
    Input Text          ${entersurnameField}    ${surname}
    Input Text          ${enteremailField}      ${email}
    Input Text          ${enterpasswordField}   ${password}
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  760     240     600
    ...     ELSE     Log    Device isnt J1
    Click Element       ${locator}
    Wait Until Element Is Visible    xpath=//android.widget.TextView[contains(@text,'${expectmessage}')]    10
    ${warningmessage}    Get Text    xpath=//android.widget.TextView[contains(@text,'${expectmessage}')]
    Log To Console  \nExpected error message is '${warningmessage}'
    Reset Application

Input invalid format name and surname
    [Arguments]         ${name}     ${surname}     ${email}      ${password}    ${locator}   ${expectmessage}   ${text}
    Input Text          ${enternameField}       ${name}
    Input Text          ${entersurnameField}    ${surname}
    Input Text          ${enteremailField}      ${email}
    Input Text          ${enterpasswordField}   ${password}
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  760     240     600
    ...     ELSE     Log    Device isnt J1
    Click Element       ${locator}
    Wait Until Element Is Visible    ${expectmessage}      10
    ${error}    Get Text    ${expectmessage}
    Should Be Equal As Strings  ${text}    ${error}
    Log To Console  \nError message is '${error}'
    Reset Application

Input correct credentials
    [Arguments]         ${name}     ${surname}     ${email}      ${password}   ${locator}
    Input Text          ${enternameField}       ${name}
    Input Text          ${entersurnameField}    ${surname}
    Input Text          ${enteremailField}      ${email}
    Input Text          ${enterpasswordField}   ${password}
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  760     240     600
    ...     ELSE     Log    Device isnt J1
    Click Element       ${locator}
    Wait Until Element Is Visible       ${successloginCheck}      20
