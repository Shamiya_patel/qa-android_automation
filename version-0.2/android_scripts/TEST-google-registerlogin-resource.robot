*** Variables ***
#Input data
# ${gmail}            katieee.more@gmail.com
# ${gmailpassword}    zwooptest






*** Keywords ***
Cancel google registration
    Click Element   ${loginwithGoogle}
    Log To Console  \nClicked on register with google

    Wait Until Element Is Visible       ${googleaccountslistTitle}      10
    ${title}    Get Text    ${googleaccountslistTitle}
    Log To Console  \nGoogle account list page title is '${title}'

    # Press Keycode   4
    # Wait Until Element Is Visible       ${cancelgoogleregistrationError}
    # ${googlesnackbarErrormessage}     Get Text    ${cancelgoogleregistrationError}
    Log To Console  \nRegistration with google cancelled
    Reset Application

Register with existing google account
    Click Element   ${loginwithGoogle}
    Log To Console  \nClicked on register with google
    Wait Until Element Is Visible       ${googleaccountslistTitle}      10
    Click Element   ${firstaccountfromList}
    ${username}  Get Text    ${firstaccountfromList}
    Log To Console  \nRegistered on zwoop with '${username}' google account

    Click Element   ${confirmgoogleaccountButton}
    Wait Until Element Is Visible       ${successloginCheck}      20
    Log To Console  \nSuccessfully registered on Zwoop with google account

Verify google registration
    Click Element   ${accountsettingsIcon}
    Wait Until Element Is Visible       ${accountpageTitle}     10
    Log To Console  \nRedirected to accounts page

    Click Element   ${profileIcon}
    Wait Until Element Is Visible       ${profilepageTitle}
    Log To Console  \nRedirected to profile page

    Page Should Contain Element     ${userprofilenameText}
    ${name}    Get Text    ${userprofilenameText}
    Log To Console  \nCustomer name is '${name}', registration with facebook successful.

    Click Element   ${logoutButton}
    Wait Until Element Is Visible   ${zwoopLogo}    15
    Log To Console  \nLogout successful, redirected back to zwoop login page

#Remember to remove the google account prior testing this[Skip as per team decission]
# Add a new google account to login
#     Click Element   ${loginwithGoogle}
#     Log To Console  \nClicked google button

#     Wait Until Element Is Visible   ${addnewgoogleaccountButton}    10
#     Log To Console  \nElement found

#     Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Click Element      ${joneaddnewgoogleaccountButton}
#     ...     ELSE     Click Element      ${addnewgoogleaccountButton}
#     Log To Console  \nClicked add new account button

#     Wait Until Element Is Visible   ${googlesigninpageTitle}    10
#     Log To Console  \nRedirected to google sign in page

#     Input Text      ${entergmailemailField}      ${gmail}
#     Click Element   ${emailnextButton}
#     Log To Console  \nEntered email and choose next button

#     Wait Until Element Is Visible   ${entergmailpassword}   10
#     Input Text      ${entergmailpassword}       ${gmailpassword}
#     Click Element   ${passwordnextButton}
#     Log To Console  \nEntered password and choose next button

#     ${T&Clink}   Get Text    ${termsandconditionLink}
#     Log To Console  \n${T&Clink}

#     Run Keyword If      '${DEVICE_NAME}'=='an_htc_ueleven'
#     ...     Click Element      ${HTCagreegoogleButton}
#     ...     ELSE     Click Element      ${NoteagreegoogleButton}
#     # ...
#     # '${DEVICE_NAME}'=='an_ss_notefour'
#     # ...     ELSE    Log     Device name didnt match

#     # Click Element   ${agreegoogleButton}
#     Log To Console  \nClicked agree button

#     Wait Until Element Is Visible   ${confirmgoogleaccountButton}   10
#     Click Element   ${confirmgoogleaccountButton}
#     Log To Console  \nClicked on allow button

#     Wait Until Element Is Visible       ${successloginCheck}      20
#     Log To Console  \nSuccessfully registered on Zwoop with google account







