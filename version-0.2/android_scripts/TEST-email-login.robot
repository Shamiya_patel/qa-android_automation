*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot

Resource            TEST-144-email-registration-resource.robot
Resource            TEST-email-login-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application


*** Variables ***


*** Test Cases ***
1. Launch app and verify page content
    Verify login page content

2. Login with empty email and password
    Empty/invalid credentials test  ${EMPTY}    ${EMPTY}        ${register/loginButton}      ${emptyemailError}     ${emptypasswordError}

3. Login with empty email
    Invalid login test  ${EMPTY}    ${loginPassword}     ${register/loginButton}      ${emptyemailError}

4. Login with empty password
    Invalid login test  ${correctSigninemail}    ${EMPTY}   ${register/loginButton}     ${emptypasswordError}

5. Login with invalid email format
    Invalid login test  ${incorrectEmail}   ${loginPassword}    ${register/loginButton}     ${emptyemailError}

6. Login with valid email and incorrect password
    Empty/invalid credentials test  ${correctSigninemail}    ${mismatchPassword}    ${register/loginButton}     ${emptyemailError}     ${emptypasswordError}

7. Login with correct credentials
    Valid login test    ${correctSigninemail}    ${registerPassword}            ${register/loginButton}

8. Logout from app
    Logout