#PROFILE PAGE LOCATORS

*** Variables ***
${profilepageTitle}         xpath=//android.widget.TextView[contains(@text,'Profile')]
# ${userprofilenameText}      id=first_name_edittext

#---------------------------------------------------EDIT PROFILE--------------------------------------------------------------#
${enterFirstname}       id=activity_profile_firstname_edittext
${firstnameError}       id=activity_profile_firstname_error

${enterSurname}         id=activity_profile_surname_edittext
${surnameError}         id=activity_profile_surname_error

${gender}               id=activity_profile_gender_value
${genderpop-upTitle}    id=title_quantity_selection
${male}                 id=radio_btn_male
${female}               id=radio_btn_female
${genderError}          id=activity_profile_gender_error

${dateofbirth}          id=activity_profile_dob_value
${dobError}             id=activity_profile_dob_error

${currentMonth}         id=date_picker_header_date
${currentYear}          id=date_picker_header_year
${selectyear}           id=text1

# ${year1985}             xpath=//android.widget.TextView[contains(@text,'1985')]
${calander}             id=month_view
# ${choosedate}           accessibility_id=15 February 1985

# ${year1988}             xpath=//android.widget.TextView[contains(@text,'1988')]
# ${chooseandsavedate}    accessibility_id=28 February 1988
${calanderCancel}       id=button2
${calanderOk}           id=button1

${registeredEmail}      id=activity_profile_email_edittext
${emailError}           id=activity_profile_email_error

${successSaveprofile}   xpath=//android.widget.TextView[contains(@text,'Your profile has been updated')]

#---------------------------------------------------CHANGE PASSWORD-------------------------------------------------------------#
${changepasswordButton}     id=change_password_button_in_profile
${changepasswordpage}       xpath=//android.widget.TextView[contains(@text,'Change password')]

${enterOldpassword}         id=activity_change_password_old_password
${oldpasswordError}         id=activity_change_password_old_password_error

${enterNewpassword}         id=activity_change_password_new_password
${newpasswordError}         id=activity_change_password_new_password_error

${enterConfirmpassword}     id=activity_change_password_confirm
${confirmpasswordError}     id=activity_change_password_confirm_error

${passwordchangedMessage}    xpath=//android.widget.TextView[contains(@text,'Password successfully changed')]

${logoutButton}             id=log_out_button

#Profile image locator is same as on accounts page


