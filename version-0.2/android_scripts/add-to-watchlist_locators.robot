#------------------------WATCH-LIST--------------------------#
*** Variables ***
${addtowatchlitstButton}          biz.zwoop.android:id/add_to_watchlist_button_product_details

${emptywatchlistIcon}             biz.zwoop.android:id/empty_watchlist_icon

${watchlistImage}                 id=watched_item_image

${watchlistbuynowButton}          id=buy_now_button_watchlist

${watchlistitemDetailList}        biz.zwoop.android:id/watched_item_details
${watchlistitemdeletedmessage}    xpath=//android.widget.TextView[contains(@text,'Watchlist item Deleted')]
${deletewatchlistseconditem}      xpath=//android.widget.RelativeLayout[contains(@index,'1')]


#-----------------------BEST PRICE----------------------------#
${bestpricefoundPH}           id=best_price
${currencyPH}                 id=currency
${itempricePH}                id=watched_item_price
${shippingcostPH}             id=watched_item_shipping_cost
${watcheditemdatetimePH}      id=watched_item_date_time
${shippingtoPH}               id=shipping_nick_name
${merchantnamePH}             id=merchant_name

