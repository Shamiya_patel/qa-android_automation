*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            notification_locators.robot

Resource            TEST-google-registerlogin-resource.robot
Resource            TEST-email-login-resource.robot
Resource            TEST-notification-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Login to account and go to back-door
    Valid login test    ${correctSigninemail}    ${password}    ${register/loginButton}
    Sleep   10
    # Register with existing google account
    # Add a new google account to login

# 2. Go to backdoor and trigger address not supported notification
    # Go to back-door
    # Trigger notification first time    ${notsupportedaddressRadioButton}   ${notsupportedaddressRadioButton}   ${triggernotificationButton}    ${triggernotificationButton}

3. Go to backdoor and trigger address not supported notification, open notification drawer and verify redirection
    Verify notification     ${addresstitleText}     ${addressNotSupportedTitle}
    Notification redirection test   ${addresstitleText}     ${addressfailimage}

4. Go back to backdoor, trigger total price update notification and verify
    # Go back to backdoor from notifications
    # Trigger notification second time    ${totalpriceupdateRadioButton}  ${totalpriceupdateRadioButton}  ${triggernotificationButton}    ${triggernotificationButton}
    Verify notification     ${totalpriceupdateText}     ${totalpriceupdateTitle}
    # Page Should Contain Element     ${bluedotNotifications}
    Notification redirection test   ${totalpriceupdateText}     ${totalpriceupdateimage}
# ${totalPriceUpdate}
# 5. Go back to backdoor, trigger purchase complete notification and verify
#     Go back to backdoor from notifications
#     Trigger notification second time    ${purchasecompleteRadioButton}  ${purchasecompleteRadioButton}  ${triggernotificationButton}    ${triggernotificationButton}
#     Verify notification     ${purchasecompletedText}     ${purchasecompleted}
#     # Notification redirection test

# 6. Go back to backdoor, trigger purchase failed notification and verify
#     Go back to backdoor from notifications
#     Trigger notification second time    ${purchasefailedRadioButton}  ${purchasefailedRadioButton}  ${triggernotificationButton}    ${triggernotificationButton}
#     Verify notification     ${purchsefailedText}     ${purchasefailed}

# 7. Go back to backdoor, trigger wrong card info notification and verify
#     Go back to backdoor from notifications
#     Trigger notification second time    ${wrongcardinfoRadioButton}  ${wrongcardinfoRadioButton}  ${triggernotificationButton}    ${triggernotificationButton}
#     Verify notification     ${wrongcardinfoText}     ${wrongcardinfoTitle}
#     # Notification redirection test

# 8. Go back to backdoor, trigger merchant not responding notification and verify
#     Go back to backdoor from notifications
#     Trigger notification second time    ${merchantnotrespondingRadioButton}  ${merchantnotrespondingRadioButton}  ${triggernotificationButton}    ${triggernotificationButton}
#     Verify notification     ${merchantnotrespondingText}     ${merchantnotresponding}

# 9. Go back to backdoor, trigger merchant out of stock notification and verify
#     Go back to backdoor from notifications
#     Trigger notification second time    ${merchantoutofstockRadioButton}  ${merchantoutofstockRadioButton}  ${triggernotificationButton}    ${triggernotificationButton}
#     Verify notification     ${merchantoutofstockText}     ${merchantoutofstock}

# 10. Go back to backdoor, trigger unknown notification and verify
#     Go back to backdoor from notifications
#     Trigger notification second time    ${unknownnotificationradioButton}  ${unknownnotificationradioButton}  ${triggernotificationButton}    ${triggernotificationButton}
#     Verify notification     ${unknownText}     ${unknown}

# 11. Go back to app landing page and logout
#     Press Keycode   4
#     Log To Console  \nRedirected from notification page to backdoor
#     Sleep   1
#     Click Element   ${appbackButton}
#     Log To Console  \nRedirected from backdoor to landing page
#     Sleep   1
#     Press Keycode   4
#     Log To Console  \nRedirected from backdoor to landing page
#     Press Keycode   4
#     Log To Console  \nRedirected from backdoor to landing page
#     # Logout


