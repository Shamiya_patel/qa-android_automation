*** Settings ***
Library             AppiumLibrary

Resource            android-common-resource.robot
# Resource            TEST-email-login-resource.robot
# Resource            TEST-mobileapp-shop-resource.robot

# Resource            create-signin_locator.robot
# Resource            landing-page_locators.robot
# Resource            shop-search_locators.robot
# Resource            add-to-watchlist_locators.robot

# Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
# Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Launch gmail app
    Open Application   http://localhost:4723/wd/hub    platformName=Android    platformVersion=7.1.1    autoGrantPermissions=True
    ...                 deviceName=an_htc_ueleven    app=${gmailAPP}    appPackage=${gmailApp_Package}  appActivity=${gmailApp_Activity}
    ...                 unicodeKeyboard=True    noReset=True
    Log To Console  \nLaunched app, build used for testing is located at '${gmailAPP}'