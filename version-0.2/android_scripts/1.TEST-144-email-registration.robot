*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot

Resource            TEST-144-email-registration-resource.robot
Resource            TEST-email-login-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Choose register and verify registration page
    Choose create account link
    Sleep   2
    Check register page content
    ${emailName} =    Generate Random String    5   [LETTERS]
    ${correctEmail}     Catenate    ${string1}${string2}${emailName}${zwoopmail}
    Log To Console    \nEmail to be used for registration is ${correctEmail}
    Set Global Variable    ${correctEmail}

    ${name}=   Generate Random String  2   [LETTERS]
    ${firstName}    Catenate    ${firstname}${name}
    Log To Console    \nFirst name to be used for registration is ${firstName}
    Set Global Variable    ${firstName}

2. Empty credentials
    Empty registration       ${register/loginButton}     ${emptynameError}  ${emptyregisternameError}   ${emptysurnameError}    ${emptyregistersurnameError}    ${emptyemailError}  ${emptyregisteremailError}  ${emptypasswordError}
       # ${emptyregisterpasswordError}

3. Name empty
    Choose create account link
    Input credentials       ${EMPTY}    ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}      ${emptyregisternameError}
    Sleep    1

4. Invalid name format
    Choose create account link
    Input invalid format name and surname   ${invalidName}      ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}     ${emptynameError}  ${invalidnameformatError}

5. Surname empty
    Choose create account link
    Input credentials       ${firstName}     ${EMPTY}        ${correctEmail}    ${registerPassword}   ${register/loginButton}      ${emptyregistersurnameError}

6. Invalid surname format
    Choose create account link
    Input invalid format name and surname    ${firstName}    ${invalidSurname}  ${correctEmail}     ${registerPassword}   ${register/loginButton}   ${emptysurnameError}     ${invalidsurnameformatError}

7. Empty email
    Choose create account link
    Input credentials       ${firstName}     ${lastName}     ${EMPTY}           ${registerPassword}   ${register/loginButton}      ${emptyregisteremailError}

8. Wrong email format1
    Choose create account link
    Input credentials       ${firstName}     ${lastName}     ${wrongemail1}     ${registerPassword}   ${register/loginButton}      ${invalidemailformatError}

9. Wrong email format2
    Choose create account link
    Input credentials       ${firstName}     ${lastName}     ${wrongemail2}     ${registerPassword}   ${register/loginButton}      ${invalidemailformatError}

10. Empty password
    Choose create account link
    Input credentials       ${firstName}     ${lastName}     ${correctEmail}    ${EMPTY}   ${register/loginButton}      ${invalidpasswordformatError}

11. Register with short password
    Choose create account link
    Input credentials       ${firstName}     ${lastName}     ${correctEmail}        ${shortpassword}     ${register/loginButton}      ${invalidpasswordformatError}

12. Register with an existing account
    Choose create account link
    Input credentials       ${firstName}     ${lastName}     ${logingmailEmail}     ${registerPassword}   ${register/loginButton}    ${useExistingemailtoregister}

13. Correct credentials sign-up
    Choose create account link
    Input correct credentials       ${firstName}     ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}
    Log To Console  \nSuccessfully created a new account and landed on landing Page

14. Logout of the app
    Logout



