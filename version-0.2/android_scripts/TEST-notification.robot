*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            notification_locators.robot

Resource            TEST-google-registerlogin-resource.robot
Resource            TEST-email-login-resource.robot
Resource            TEST-notification-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app and login to account
    Valid login test    ${correctSigninemail}    ${registerPassword}    ${register/loginButton}
    Sleep   10
    # Register with existing google account
    # Add a new google account to login

2. Trigger address not supported notification, open notification drawer and verify redirection
    Verify notification     ${addresstitleText}     ${addressNotSupportedTitle}
    Notification redirection test   ${addresstitleText}
    Delete notification     ${addresstitleText}

4. Trigger total price update notification and verify
    Verify notification     ${totalpriceupdateText}     ${totalpriceupdateTitle}
    Notification redirection test   ${totalpriceupdateText}
    Delete notification     ${totalpriceupdateText}
# ${totalPriceUpdate}
5. Trigger purchase complete notification and verify
    Verify notification     ${purchasecompletedText}     ${purchasecompletedTitle}
    Purchase completed notification redirection test
    Delete notification     ${purchasecompletedText}

6. Trigger purchase failed notification and verify
   Verify notification     ${purchsefailedText}     ${purchasefailedTitle}
   Notification redirection test   ${purchsefailedText}
   Delete notification     ${purchsefailedText}

7. Trigger wrong card info notification and verify
    Verify notification     ${wrongcardinfoText}     ${wrongcardinfoTitle}
    Notification redirection test   ${wrongcardinfoText}
    Delete notification     ${wrongcardinfoText}

8. Trigger merchant not responding notification and verify
    Verify notification     ${merchantnotrespondingText}     ${merchantnotrespondingTitle}
    Notification redirection test   ${merchantnotrespondingText}
    Delete notification     ${merchantnotrespondingText}

9. Trigger merchant out of stock notification and verify
    Verify notification     ${merchantoutofstockText}     ${merchantoutofstockTitle}
    Notification redirection test   ${merchantoutofstockText}
    Delete notification     ${merchantoutofstockText}

# 10. Trigger all types of notifications and validate
#     Trigger all notifications simultaneously and verify

# 10. Trigger unknown notification and verify
#     Verify notification     ${unknownText}     ${unknown}

# 11. Go back to app landing page and logout
#     Press Keycode   4
#     Log To Console  \nRedirected from notification page to backdoor
#     Sleep   1
#     Click Element   ${appbackButton}
#     Log To Console  \nRedirected from backdoor to landing page
#     Sleep   1
#     Press Keycode   4
#     Log To Console  \nRedirected from backdoor to landing page
#     Press Keycode   4
#     Log To Console  \nRedirected from backdoor to landing page
#     # Logout


