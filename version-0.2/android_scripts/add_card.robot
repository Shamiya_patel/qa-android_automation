*** Settings ***
Documentation       This unit test helps to verify add a new card.

Library             AppiumLibrary

Resource            common_resource.robot
Resource            common-variable.robot
# Resource            create-signin_locator.robot
# Resource            logout_locator.robot
Resource            landing_page_locators.robot
Resource            account_locator.robot
Resource            add_card_locator.robot
Resource            add_card_variables.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Go to settings and choose cards option
    Wait Until Element Is Visible   ${SETTINGS}     15
    Click Element   ${SETTINGS}
    Click Element   ${CARDS_ICON}

2. Choose '+' to add card from accounts page
    Click Element   ${+BUTTON}

3. Enter nickname in card form
    Generate random card nickname
    Input Text   ${CARD_NICKNAME}   ${NICKNAME}
    Log To Console      \nCard nickname added

4. Enter name on card
    Generate random name for card
    Input Text      ${NAME_ON_CARD}    card-${NAME_CARD}
    Log To Console      \nName on card added

5. Enter card number
    Input Text   ${CARD_NO}     ${VISA}
    Log To Console      \nCard number added

6. Select expiry month
    Generate random month for card expiry
    Click Element   ${EXPIRY_MONTH}
    Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'${MONTH}')]
    Log To Console      \nCard expiry month selected

7. Select expiry year
    Generate random year for card expiry
    Click Element   ${EXPITY_YEAR}
    Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'${YEAR}')]
    Log To Console      \nCard expiry year selected

8. Enter cvv
    Generate a random CVV
    Input Text      ${CVV}      ${CVV_NO}
    Log To Console      \nCard cvv added

9. Choose save
    Click Element   ${DONE_BUTTON}
    Wait Until Element Is Visible    ${CARD_TITLE}     15
    Log To Console      \nCard form saved successfully


*** Keywords ***
Generate random card nickname
    ${NAME}     Generate Random String      5   [LETTERS]
    Log To Console  \nNickname for card is ${NAME}
    Set Global Variable     ${NICKNAME}     ${NAME}

Generate random name for card
    ${NAME_C}     Generate Random String      9   [LETTERS]
    Log To Console  \nName on card is ${NAME_C}
    Set Global Variable     ${NAME_CARD}     ${NAME_C}

Generate random month for card expiry
    ${NUM}   Generate Random String  1   [NUMBERS]
    Log To Console  \n Month generated is ${NUM}
    Set Global Variable     ${MONTH}   ${NUM}

Generate random year for card expiry
    ${NM}   Generate Random String  1   [NUMBERS]1-9
    Log To Console  \n Year generated is ${NM}
    Set Global Variable     ${YEAR}   ${NM}

Generate a random CVV
    ${cvv}   Generate Random String  3   [NUMBERS]
    Log To Console  \n CVV for the card is ${cvv}
    Set Global Variable     ${CVV_NO}   ${cvv}








