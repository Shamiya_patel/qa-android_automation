*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            profilepage_locators.robot

Resource            TEST-google-registerlogin-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app, choose register with google and choose device back button
    Cancel google registration

2. Register with google using existing account from the list
    Register with existing google account
    Verify google registration

#[As add new account is on goole beeter skip it as per team discussion so commented]
# 3. Login with new google account
#     Add a new google account to Login
#     Logout