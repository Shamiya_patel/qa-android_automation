*** Settings ***
Documentation       This file contain all the reusable keywords and variables for automation
Library             AppiumLibrary
Library             BuiltIn
Library             String

Resource            create-signin_locator.robot
Resource            account_locators.robot
Resource            profilepage_locators.robot

*** Variables ***
${APP}                  /mobile_automation/zwoop_automation/android_automation/android_builds/newdev/zwoop-0.2.5-dev.apk
${APPIUM_SERVER}        http://localhost:${PORT}/wd/hub
${PLATFORM_NAME}        Android
${APP_PACKAGE}          biz.zwoop.android
${APP_ACTIVITY}         biz.zwoop.android.ui.splash.SplashActivity

${gmailApp_Package}     com.google.android.gm
${gmailApp_Activity}    ConversationListActivityGmail
${gmailAPP}             /Users/apple/Downloads/Gmail_v8.1.28.186013355.release_apkpure.com.apk

#Data
#GMAIL
${logingmailEmail}         zwoopautomation@gmail.com
# ${correctPassword}         123456

#EMAIL
${emailId}              qa.zwoopautomation+zNkyj@zwoop.biz
${registerPassword}     123457


#Common buttons
${appbackButton}      accessibility_id=Navigate up
# ${CANCLE_ADDRESS_SAVE}  accessibility_id=Cancel
${save/doneButton}    id=action_done
${delete}             accessibility_id=Delete

*** Keywords ***
Launch Application
    [Arguments]   ${APPIUM_SERVER}   ${PLATFORM_VERSION}   ${DEVICE_NAME}
    [Documentation]     Launch AUT to perform various tests
    Log     ${APPIUM_SERVER}
    Log     ${PLATFORM_NAME}
    Log     ${PLATFORM_VERSION}
    Log     ${DEVICE_NAME}
    Log     ${APP}

    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}    autoGrantPermissions=True
    ...                 deviceName=${DEVICE_NAME}    app=${APP}    appPackage=${APP_PACKAGE}  appActivity=${APP_ACTIVITY}
    ...                 unicodeKeyboard=True    resetKeyboard=True  session-override=True   clearSystemFiles=True    noReset=True
    Log To Console  \nLaunched app, build used for testing is located at '${APP}'

    # Open Application    http://localhost:4725/wd/hub    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}    autoGrantPermissions=True
    # ...                 deviceName=${DEVICE_NAME}    app=${APP}    appPackage=${APP_PACKAGE}  appActivity=${APP_ACTIVITY}
    # ...                 unicodeKeyboard=True    resetKeyboard=True  session-override=True   clearSystemFiles=True    noReset=False
    # Log To Console  \nLaunched app, build used for testing is located at '${APP}'

Email Log-in
    Input Text      ${enteremailField}       ${emailId}
    Input Text      ${enterpasswordField}    ${password}
    Click Element   ${register/loginButton}
    Wait Until Element Is Visible       ${successloginCheck}      20
    Log To Console  \nSign-in successful

Logout
    Wait Until Element Is Visible   ${accountsettingsIcon}    20
    Click Element   ${accountsettingsIcon}
    Log To Console  \nRedirected to account page

    # Page Should Contain Element     ${accountscustomerName}
    Wait Until Element Is Visible   ${accountscustomerName}  10
    ${username}     Get Text    ${accountscustomerName}
    Log To Console  \nProfile name is '${username}'

    Wait Until Element Is Visible   ${logoutButton}     10
    Click Element   ${logoutButton}
    Wait Until Element Is Visible   ${zwoopLogo}    15
    Log To Console  \nLogout successful, redirected back to login page


