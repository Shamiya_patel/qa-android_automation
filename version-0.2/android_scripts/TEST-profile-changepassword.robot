*** Settings ***
Documentation     This is a unit test for change password, performs below tests
...     1. Login to an existing account
...     2. Go to accounts and perform various tests to change password

Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            account_locators.robot
Resource            profilepage_locators.robot

Resource            TEST-profile-changepassword-resource.robot
Resource            TEST-144-email-registration-resource.robot
Resource            TEST-email-login-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Verify landing page and register a new user
    Generate first name and email
    Choose create account link
    Check register page content
    Input correct credentials       ${firstName}     ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}
    Log To Console  \nSuccessfully created a new account and landed on landing Page
#-------------------------------------------- OR ----------------------------------------------------------#
# 1. Login to an existing account and change password
#     Verify login page content
#     Valid login test    ${correctSigninemail}    ${registerPassword}    ${register/loginButton}
#     Log To Console  \nLogged in successful

2. Go to profile
    Go to profile page

3. Navigate to change password page
    Choose change password

4. Empty change password test
    Empty credentials test

5. Old password empty test
    Change pasword test     ${EMPTY}    ${newpassword}    ${newpassword}    ${oldpasswordError}     ${newpasswordError}     ${confirmpasswordError}

6. New password empty test
    Change pasword test     ${loginPassword}    ${EMPTY}    ${newpassword}    ${oldpasswordError}     ${newpasswordError}     ${confirmpasswordError}

7. Confirm new password empty test
    Change pasword test     ${loginPassword}    ${newpassword}    ${EMPTY}    ${oldpasswordError}     ${newpasswordError}     ${confirmpasswordError}

# 8. Incorrect old password test -----TBD
#     Change pasword test     ${randomoldpassword}    ${newpassword}    ${newpassword}    ${oldpasswordError}     ${newpasswordError}     ${confirmpasswordError}

9. Short new password test
    Change pasword test     ${loginPassword}    ${newshortpass}    ${newshortpass}    ${oldpasswordError}     ${newpasswordError}     ${confirmpasswordError}

10. Mismatch new passsowrd and confirm password test
    Change pasword test     ${loginPassword}    ${mismatchnewpass}    ${newpassword}    ${oldpasswordError}     ${newpasswordError}     ${confirmpasswordError}

11. Successully update password
    Correct change password

12. Logout
    Go back to account page from profile page and logout




