*** Variables ***
#Input data
${correctSigninemail}       qa.zwoopautomation+DbAZE@zwoop.biz
# qa.zwoopautomation+zNkyj@zwoop.biz, qa.zwoopautomation+DbAZE@zwoop.biz,   qa.zwoopautomation+ggfik@zwoop.biz
${incorrectEmail}           qa.automationvUXhDzwoop.biz
${loginPassword}            123457
# ${shortpassword}        1122
${mismatchPassword}         123477

#Login error messages
# ${emptyregisternameError}          Name is a mandatory field
# ${emptyregistersurnameError}       Surname is a mandatory field
# ${invalidnameformatError}          Name can't contain any digits or special characters
# ${invalidsurnameformatError}       Surname can't contain any digits or special characters
# ${emptyregisteremailError}         Email is a mandatory field
# ${emptyregisterpasswordError}      Password is a mandatory field
# ${invalidemailformatError}         Email format is not correct
# ${invalidpasswordformatError}      Password must be at least 6 digits long
# ${mismatchincorrectpasswordError}  Login failed. Email or password is wrong
# ${useExistingemailtoregister}      This email has been used


*** Keywords ***
Verify login page content
    Page Should Contain Element     ${zwoopLogo}
    Page Should Contain Element     ${loginwithGoogle}
    Page Should Contain Element     ${loginwithFacebook}
    Page Should Contain Element     ${enteremailField}
    Page Should Contain Element     ${enterpasswordField}
    Page Should Contain Element     ${forgotPasswordbutton}
    Page Should Contain Element     ${register/loginButton}
    Page Should Contain Element     ${registernewaccountButton}
    Log To Console   \nVerified login page content

Empty/invalid credentials test
    [Arguments]         ${email}      ${password}   ${locator}   ${expectmessage1}  ${expectmessage2}
    Input Text          ${enteremailField}      ${email}
    Input Text          ${enterpasswordField}   ${password}
    Click Element       ${locator}
    Wait Until Element Is Visible   ${expectmessage1}    10
    ${Warningmessage}    Get Text    ${expectmessage1}
    Log To Console  \nExpected error message is '${Warningmessage}'

    Wait Until Element Is Visible   ${expectmessage2}    10
    ${Warningmessage}    Get Text    ${expectmessage2}
    Log To Console  \nExpected error message is '${Warningmessage}'
    Reset Application

Invalid login test
    [Arguments]         ${email}      ${password}   ${locator}   ${expectmessage}
    Input Text          ${enteremailField}      ${email}
    Input Text          ${enterpasswordField}   ${password}
    Click Element       ${locator}
    Wait Until Element Is Visible   ${expectmessage}  10
    # xpath=//android.widget.TextView[contains(@text,'${expectmessage}')]    10
    ${Warningmessage}    Get Text    ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'
    Reset Application

Valid login test
    [Arguments]         ${email}      ${password}   ${locator}
    Input Text          ${enteremailField}      ${email}
    Input Text          ${enterpasswordField}   ${password}
    Click Element       ${locator}
    Log To Console  \nLogin email is '${email}'
    Wait Until Element Is Visible   ${successloginCheck}      20
    Log To Console  \nLogin successful, landed on Shop page.

