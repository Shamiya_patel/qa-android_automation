#ADD ADDRESS LOCATORS
*** Variables ***
#Page titles
${addresspagetitleLocator}             xpath=//android.widget.TextView[contains(@text,'Addresses')]
${addnewaddresspagetitleLocator}       xpath=//android.widget.TextView[contains(@text,'Add Address')]

#locators
${chatroomaddaddressLocator}          id=add_address_button
${addnewaddressLocator}               id=fab_new_address
${formaddresstitleLocator}            id=address_title
#1. country list dropdown, after selecting country name
${countrydropdownbuttonLocator}            id=drop_down_arrow_country
${selectedcountrynameLocator}              id=country_selection_textview
#OR
# ${selectcountryLocator}   xpath=//android.widget.TextView[contains(@text,'Select Country')]
#Search is same for city/region on address for HK
${searchcountrybuttonLocator}        id=search_button
#accessibility_id=Search
#Input text for the search is same for city/region search
${entersearchCountryLocator}         id=search_src_text
${chooseCountrynameLocator}          id=country_name
#2. country code dropdown after selecting country
${selectedpostcodeLocator}             id=country_code_phone_number
# ${SELECTHKCOUNTY}        xpath=//android.widget.TextView[contains(@text,'Hong Kong')]

#COMMON
${phonenumberLocator}                 id=phone_number
${countrycodedropdownLocator}         id=drop_down_icon
${firstnameLocator}                   id=first_name
${lastnameLocator}                    id=last_name
${flat/housenoLocator}                xpath=//android.widget.EditText[contains(@text,'flatHouseNumber')]
${floornoLocator}                     xpath=//android.widget.EditText[contains(@text,'floorNumber')]
${house/buildingnoLocator}            xpath=//android.widget.EditText[contains(@text,'houseBuildingNumber')]
${house/buildingnameLocator}          xpath=//android.widget.EditText[contains(@text,'houseBuildingName')]
${streetnoLocator}                    xpath=//android.widget.EditText[contains(@text,'streetNumber')]
${streetnameLocator}                  xpath=//android.widget.EditText[contains(@text,'streetName')]
#UK
${uktownLocator}                      xpath=//android.widget.EditText[contains(@text,'townCity')]
${ukwardLocator}                      xpath=//android.widget.EditText[contains(@text,'ward')]
${ukdistrictLocator}                  xpath=//android.widget.EditText[contains(@text,'district')]
${ukpostcodeLocator}                  xpath=//android.widget.EditText[contains(@text,'postCode e.g. N1 9LH')]

#HK
${hkareadropdownLocator}              xpath=//android.widget.TextView[contains(@text,'Central and Western')]
${hkcitydropdownLocator}              xpath=//android.widget.TextView[contains(@text,'Hong Kong Island')]
${citydropdownLocator}                id=drop_down_arrow

#Select input search from the list is same for both city/region
${selectcityLocator}                  id=option_name
#Close is same for both city/region
${closepopupLocator}                  id=search_close_btn
#Take close without savinf address from common_locators.robot
${saveaddressLocator}                 id=action_done
${addresssavedtoastmessageLocator}    xpath=//android.widget.TextView[contains(@text,'Address saved')]

