*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot

Resource            TEST-email-login-resource.robot
Resource            TEST-144-email-registration-resource.robot

Resource            TEST-account-profile-resource.robot
Resource            TEST-profile-changepassword-resource.robot


Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application


*** Variables ***


*** Test Cases ***
1. Launch app and register/login
    ${emailName} =    Generate Random String    5   [LETTERS]
    ${correctEmail}     Catenate    ${string1}${string2}${emailName}${zwoopmail}
    Log To Console    \nEmail to be used for registration is ${correctEmail}
    Set Global Variable    ${correctEmail}

    ${name}=   Generate Random String  2   [LETTERS]
    ${firstName}    Catenate    ${firstname}${name}
    Log To Console    \nFirst name to be used for registration is ${firstName}
    Set Global Variable    ${firstName}

    ###LOGIN###
    # Verify login page content
    # Valid login test    ${correctSigninemail}    ${password}    ${register/loginButton}
    # Log To Console  \nLogged in successful

    Choose create account link
    Check register page content
    Input correct credentials       ${firstName}     ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}
    Log To Console  \nSuccessfully created a new account and landed on landing Page

2. Navigate to profile page
    Go to profile page

3. Check profile page
    Verify profile page

4. Clear all fields and save profile page
    Clear inputs
    Save empty profile

5. Test invalid inputs for name and surname
    Invalid inputs  ${enterFirstname}   ${nameInvalid}      ${firstnameError}
    Sleep   1
    Invalid inputs  ${enterSurname}     ${surnameInvalid}   ${surnameError}

6. Enter first name and rest field empty
    Input only one field     ${updatefirstName}    ${EMPTY}    ${EMPTY}

7. Enter surname name and rest field empty
    Input only one field     ${EMPTY}    ${lastName}    ${EMPTY}

8. Enter email and rest field empty
    Input only one field     ${EMPTY}    ${EMPTY}    ${correctSigninemail}

9. Select gender, rest fields empty and save
    Back forth accounts>profile
    Clear inputs
    Choose gender   ${male}

10. Select DOB and rest field empty
    Back forth accounts>profile
    Clear inputs
    Choose DOB

11. Enter all inputs and save complete profile
    Save profile

12. Go back to accounts and logout
    Go back to account page from profile page and logout


