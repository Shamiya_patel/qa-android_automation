*** Settings ***
Library             AppiumLibrary

Resource            android-common-resource.robot
Resource            TEST-email-login-resource.robot
Resource            TEST-mobileapp-shop-resource.robot

Resource            create-signin_locator.robot
Resource            account_locators.robot
Resource            landing-page_locators.robot
Resource            shop-search_locators.robot
Resource            add-to-watchlist_locators.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
# 1. Share product, prior login
#     Share product for non log-in user

2. Launch app and login to an existing account
    Verify login page content
    Valid login test    ${mail}    ${registerPassword}    ${register/loginButton}
    Log To Console  \nLogged in successful and landed on shop page

3. Check shop page
    Verify shop page

4. Check clear search
    Verify clear search

5. Input keyword and search
    Search for any keyword and verify

6. Go to product page from search list and go back to shop page
    Scroll through search results

7. Search and add to watchlist
    Search product and add to watchlist

8. Search and add same product to watchlist
    Add same product to watchlist

# 9. Delete watchlist item
#     Delete watchlist item after adding

10. Share a product from gmail
    Open gamil to share product

11. Logout and share product
    Logout

12. Non-login user share
    Share product for non log-in user

13. Re-login and check for best price
    Verify login page content
    Valid login test    ${mail}    ${registerPassword}            ${register/loginButton}
    Log To Console  \nLogged in successful and landed on shop page

14. Check best price
    Go to watchlist tab
    Verify once best price is received

9. Delete watchlist item
    Delete watchlist item after adding

