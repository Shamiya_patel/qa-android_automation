*** Variables ***
#Input data
${updatefirstName}    Mario Bross is my name
${updatesureName}     Surname is very very very very long
${nameInvalid}        Lugi Bross - is my name
${surnameInvalid}     My surname is - very very very very long

*** Keywords ***
Verify profile page
    Page Should Contain Element     ${enterFirstname}
    Page Should Contain Element     ${enterSurname}
    Page Should Contain Element     ${registeredEmail}
    Log To Console  \nElements visible

    Wait Until Element Is Visible   ${enterFirstname}   10
    ${firstname}    Get Text    ${enterFirstname}
    Log To Console  \nUser name is '${firstname}'

    Wait Until Element Is Visible   ${enterSurname}   10
    ${surname}    Get Text    ${enterSurname}
    Log To Console  \nUser name is '${surname}'

    Wait Until Element Is Visible   ${registeredEmail}   10
    ${regemail}    Get Text    ${registeredEmail}
    Log To Console  \nUser name is '${regemail}'

Clear inputs
    Clear Text  ${enterFirstname}
    Clear Text  ${enterSurname}
    Clear Text  ${registeredEmail}
    Log To Console  \nCleared all fields

Back forth accounts>profile
    Click Element   ${appbackButton}
    Wait Until Element Is Visible   ${accountpageTitle}     10
    ${page}     Get Text    ${accountpageTitle}
    Log To Console  \nRedirected back to '${page}' page

    Click Element   ${profileIcon}
     Wait Until Element Is Visible   ${profilepageTitle}     10
    ${page}     Get Text    ${profilepageTitle}
    Log To Console  \nRedirected to '${page}' page again

Save empty profile
    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button

    Wait Until Element Is Visible   ${firstnameError}   10
    ${error1}    Get Text    ${firstnameError}
    Log To Console  \nError message is '${error1}'
    # Sleep   2
    Page Should Contain Element     ${surnameError}
    ${error2}    Get Text    ${surnameError}
    Log To Console  \nError message is '${error2}'

    Page Should Contain Element     ${genderError}
    ${error3}    Get Text    ${genderError}
    Log To Console  \nError message is '${error3}'

    Page Should Contain Element     ${dobError}
    ${error4}    Get Text    ${dobError}
    Log To Console  \nError message is '${error4}'

    Page Should Contain Element     ${emailError}
    ${error5}    Get Text    ${emailError}
    Log To Console  \nError message is '${error5}'


Input only one field
    [Arguments]     ${fname}    ${srname}   ${mail}
    Input Text  ${enterFirstname}   ${fname}
    Log To Console  \nInput '${fname}' as first name

    Input Text  ${enterSurname}     ${srname}
    Log To Console  \nInput '${srname}' as surname name

    Click Element   ${dateofbirth}
    Wait Until Element Is Visible   ${calanderCancel}   10
    Click Element   ${calanderCancel}

    Input Text  ${registeredEmail}  ${mail}
    Log To Console  \nInput '${mail}' to update email

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button

    Page Should Contain Element     ${firstnameError}
    ${error}    Get Text    ${firstnameError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${surnameError}
    ${error}    Get Text    ${surnameError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${genderError}
    ${error}    Get Text    ${genderError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${dobError}
    ${error}    Get Text    ${dobError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${emailError}
    ${error}    Get Text    ${emailError}
    Log To Console  \nError message is '${error}'

Choose gender
    [Arguments]     ${gen}
    Click Element   ${gender}
    Wait Until Element Is Visible   ${genderpop-upTitle}    10
    ${pop-up}   Get Text    ${genderpop-upTitle}
    Log To Console  \nPop-up '${pop-up}' appears

    Wait Until Element Is Visible     ${gen}    10
    ${value}    Get Text    ${gen}
    Log To Console  \nChoose '${value}' as gender
    Click Element   ${gen}

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button, gender selected

    Page Should Contain Element     ${firstnameError}
    ${error}    Get Text    ${firstnameError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${surnameError}
    ${error}    Get Text    ${surnameError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${dobError}
    ${error}    Get Text    ${dobError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${emailError}
    ${error}    Get Text    ${emailError}
    Log To Console  \nError message is '${error}'

#HACK
# Scroll year for calander
#     Run Keyword If  '${DEVICE_NAME}'=='an_htc_ueleven'
#     ...     Swipe   720     825    720     2015
#     ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_prime'
#     ...     Swipe   340     430     340     950
#     ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   240     380     240     650
#     ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_notefour'
#     ...     Swipe   720     825     720     2015
#     ...     ELSE    Log     Device is not in the above list

Scroll for Xperia
    Swipe   530     530    530     1400
    Sleep   1
    Swipe   530     530    530     1400
    Sleep   1
    Swipe   530     530    530     1400
    Log To Console  \nScrolled through years for calander successfully

Scroll for Prime
    Swipe   340     430    340     950
    Sleep   1
    Swipe   340     430    340     950
    Sleep   1
    Swipe   340     430    340     950
    Log To Console  \nScrolled through years for calander successfully

Scroll for J1
    Swipe   240     380    240     650
    Sleep   1
    Swipe   240     380    240     650
    Sleep   1
    Swipe   240     380    240     650
    Log To Console  \nScrolled through years for calander successfully

Scroll for HTC and Note4
    Swipe   720     825    720     2015
    Sleep   1
    Swipe   720     825    720     2015
    Sleep   1
    Swipe   720     825    720     2015
    Log To Console  \nScrolled through years for calander successfully

Scroll for Pixel2
    Swipe   535     590    535     1400
    Sleep   1
    Swipe   535     590    535     1400
    Sleep   1
    Swipe   535     590    535     1400
    Log To Console  \nScrolled through years for calander successfully

Scroll for LG-G5
    Swipe   680     760    680     1945
    Sleep   1
    Swipe   680     760    680     1945
    Sleep   1
    Swipe   680     760    680     1945
    Log To Console  \nScrolled through years for calander successfully

Scroll for Oneplus
    Swipe   540     695    540     1490
    Sleep   1
    Swipe   540     695    540     1490
    Sleep   1
    Swipe   540     695    540     1490
    Log To Console  \nScrolled through years for calander successfully

Choose DOB
    Click Element   ${dateofbirth}
    Wait Until Element Is Visible   ${currentYear}  10
    Log To Console  \nSelect year element visible

    Click Element   ${currentYear}
    Wait Until Element Is Visible   ${calanderCancel}   10
    # Swipe   440     1013    440     480
    # Swipe   350     440    350     1000 -J1

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Scroll for J1
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Scroll for Prime
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Scroll for Xperia
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Scroll for HTC and Note4
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Scroll for HTC and Note4
    ...     ELSE IF     '${DEVICE_NAME}'=='an_google_pixeltwo'
    ...     Scroll for Pixel2
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Scroll for LG-G5
    ...     ELSE IF     '${DEVICE_NAME}'=='an_oneplus_oneplusfive'
    ...     Scroll for Oneplus
    ...     ELSE    Log To Console  \nDevice not mentioned in above list
    Sleep   1
    Wait Until Element Is Visible   ${selectyear}     10
    ${year}     Get Text    ${selectyear}
    Click Element   ${selectyear}
    Log To Console  \nSelected '${year}' as year

    #HARD CODED DATE
    # Click Element   ${choosedate}
    # ${date}     Get Text    ${choosedate}
    # Log To Console  \nDate to be selected is '${date}'
    Click Element   ${calanderOk}

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button, DOB selected

    Page Should Contain Element     ${firstnameError}
    ${error}    Get Text    ${firstnameError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${surnameError}
    ${error}    Get Text    ${surnameError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${genderError}
    ${error}    Get Text    ${genderError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element         ${emailError}
    ${error}    Get Text    ${emailError}
    Log To Console  \nError message is '${error}'

Invalid inputs
    [Arguments]     ${field}    ${value}    ${errormessage}
    Input Text  ${field}   ${value}
    Log To Console  \nInput '${value}' as first name

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button, DOB selected

    Page Should Contain Element         ${errormessage}
    ${error}    Get Text    ${errormessage}
    Log To Console  \nError message is '${error}'

Save profile
    Input Text  ${enterFirstname}   ${updatefirstName}
    Log To Console  \nEntered '${updatefirstName}' as first name

    Input Text  ${enterSurname}     ${lastName}
    Log To Console  \nEntered '${lastName}' as surname name

    Click Element   ${gender}
    Wait Until Element Is Visible   ${genderpop-upTitle}    10
    ${pop-up}   Get Text    ${genderpop-upTitle}
    Log To Console  \nPop-up '${pop-up}' appears

    Wait Until Element Is Visible     ${female}    10
    ${value}    Get Text    ${female}
    Log To Console  \nChoose '${value}' as gender
    Click Element   ${female}

    Click Element   ${dateofbirth}
    Wait Until Element Is Visible   ${currentYear}  10
    Log To Console  \nSelect year element visible

    Click Element   ${currentYear}
    Wait Until Element Is Visible   ${calanderCancel}   10
    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Scroll for J1
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Scroll for Prime
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Scroll for Xperia
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Scroll for HTC and Note4
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Scroll for HTC and Note4
    ...     ELSE IF     '${DEVICE_NAME}'=='an_google_pixeltwo'
    ...     Scroll for Pixel2
    ...     ELSE IF     '${DEVICE_NAME}'=='an_lg_gfive'
    ...     Scroll for LG-G5
    ...     ELSE IF     '${DEVICE_NAME}'=='an_oneplus_oneplusfive'
    ...     Scroll for Oneplus
    ...     ELSE    Log To Console  \nDevice not mentioned in above list
    Sleep   1
    Wait Until Element Is Visible   ${selectyear}     10
    ${year}     Get Text    ${selectyear}
    Click Element   ${selectyear}
    Log To Console  \nSelected '${year}' as year

    # Click Element   ${chooseandsavedate}
    # ${date}     Get Text    ${chooseandsavedate}
    # Log To Console  \nDate to be selected is '${date}'
    Click Element   ${calanderOk}

    Input Text  ${registeredEmail}  ${correctEmail}
    Log To Console  \nEnter '${correctEmail}' and update email

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button

    Wait Until Element Is Visible   ${successSaveprofile}     10
    ${message}   Get Text    ${successSaveprofile}
    Log To Console  \n'${message}'


