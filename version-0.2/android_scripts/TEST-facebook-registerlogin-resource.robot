*** Variables ***
#Input data

${fbemail}      janet.zwp@gmail.com
${fbpassword}   zwooptest


*** Keywords ***
Cancel facebook registration
    Click Element   ${loginwithFacebook}
    Log To Console  \nClicked on register with facebook
    #BELOW STEPS OF THIS KEYWORD INCLUDES THIRD PARTY APPLICATION FACEBOOK - HIGH CHANCES TO BREAK--->FREGILE TEST
    Wait Until Element Is Visible       ${enterFBemail}      10
    Press Keycode   4
    Wait Until Element Is Visible       ${cancelfbregistration}
    ${snackbarErrormessage}     Get Text    ${cancelfbregistration}
    Log To Console  \n${snackbarErrormessage}
    Reset Application

First time registration with facebook
    Click Element   ${loginwithFacebook}
    Log To Console  \nClicked on register with facebook

    #BELOW STEPS OF THIS KEYWORD INCLUDES THIRD PARTY APPLICATION FACEBOOK - HIGH CHANCES TO BREAK--->FREGILE TEST
    Wait Until Element Is Visible       ${enterFBemail}      10
    Page Should Contain Element     ${enterFBemail}
    Page Should Contain Element     ${enterFBpassword}
    Page Should Contain Element     ${fbloginbutton2}
    Log To Console  \nVerified registration page content

    Input Text      ${enterFBemail}     ${fbemail}
    Input Text      ${enterFBpassword}  ${fbpassword}
    Click Element   ${fbloginbutton2}

    Wait Until Element Is Visible       ${confirmpageText}      10
    Click Element   ${continueandconfirmAccess}
    Log To Console  \nClicked continue and confirm to register with facebook

    Wait Until Element Is Visible       ${successloginCheck}      20
    Log To Console  \nSuccessfully registered on Zwoop with facebook

Verify registration with facebook
    Click Element   ${accountsettingsIcon}
    Wait Until Element Is Visible       ${accountpageTitle}     10
    Log To Console  \nRedirected to accounts page

    Click Element   ${profileIcon}
    Wait Until Element Is Visible       ${profilepageTitle}
    Log To Console  \nRedirected to profile page

    Page Should Contain Element     ${userprofilenameText}
    ${FBname}    Get Text    ${userprofilenameText}
    Log To Console  \nCustomer name is '${FBname}', registration with facebook successful.

    Click Element   ${logoutButton}
    Wait Until Element Is Visible   ${zwoopLogo}    15
    Log To Console  \nLogout successful, redirected back to zwoop login page

# Kill app and launch it again [silent login test]

Facebook login
    Click Element   ${loginwithFacebook}
    Log To Console  \nClicked on login with facebook

    #BELOW STEPS OF THIS KEYWORD INCLUDES THIRD PARTY APPLICATION FACEBOOK - HIGH CHANCES TO BREAK--->FREGILE TEST
    Wait Until Element Is Visible       ${enterFBemail}      10
    Get Source

    Page Should Contain Element     ${enterFBemail}
    Page Should Contain Element     ${enterFBpassword}
    Page Should Contain Element     ${fbloginbutton2}
    Log To Console  \nVerified login page content

    Input Text      ${enterFBemail}     ${fbemail}
    Input Text      ${enterFBpassword}  ${fbpassword}
    Click Element   ${fbloginbutton2}

    Wait Until Element Is Visible       ${confirmpageText}      10
    Click Element   ${continueandconfirmAccess}
    Log To Console  \nClicked continue and confirm to login with facebook

    Wait Until Element Is Visible       ${successloginCheck}      20
    Log To Console  \nSuccessfully login on Zwoop with facebook

Verify login with facebook
    Click Element   ${accountsettingsIcon}
    Wait Until Element Is Visible       ${accountpageTitle}     10
    Log To Console  \nRedirected to accounts page

    Click Element   ${profileIcon}
    Wait Until Element Is Visible       ${profilepageTitle}
    Log To Console  \nRedirected to profile page

    Page Should Contain Element     ${userprofilenameText}
    ${name}    Get Text    ${userprofilenameText}
    Log To Console  \nCustomer name is '${name}', login with facebook successful.

