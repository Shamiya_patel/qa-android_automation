#ADD ADDRESS LOCATORS
*** Variables ***
${addresspageTitle}              xpath=//android.widget.TextView[contains(@text,'Addresses')]
${emptyaddresspageimage}         id=empty_address_list_icon
${addnewaddressIcon}             id=fab_new_address
${addnewaddresspageTitle}        xpath=//android.widget.TextView[contains(@text,'Add a new address')]

${countryTitle}                  id=activity_add_address_country_label
${flagcountrycode}               id=flag_phone_number
${countrycodeNumber}             id=country_code_phone_number

#Country selection
${selectedCountryname}           id=country_selection_textview

#MANDATORY TEXT FIELDS
# TF stands for text fields to input text
${addresstitleTF}           id=address_title
${addresstitleError}        id=activity_add_address_title_error

${streetnumberTF}           xpath=//android.widget.EditText[contains(@text,'Street Number')]
${streetnoError}            xpath=//android.widget.TextView[contains(@index,'9')]

${streetnameTF}             xpath=//android.widget.EditText[contains(@text,'Street Name')]
${streetnameError}          xpath=//android.widget.TextView[contains(@index,'11')]

${town/cityTF}              xpath=//android.widget.EditText[contains(@text,'Town / City')]
${town/cityError}           xpath=//android.widget.TextView[contains(@index,'13')]

${countyTF}                 xpath=//android.widget.EditText[contains(@text,'County')]
${countyError}              xpath=//android.widget.TextView[contains(@index,'15')]

# ${districtTF}               xpath=//android.widget.EditText[contains(@text,'District')]
# ${districtError}            xpath=//android.widget.TextView[contains(@index,'17')]

${postcodeTF}               xpath=//android.widget.EditText[contains(@text,'Post Code, e.g. N1 9LH')]
${postcodeError}            xpath=//android.widget.TextView[contains(@index,'19')]

${phonenumberTF}            id=phone_number
${phonenumberError}         id=activity_add_address_phone_error

${recipientfirstnameTF}     id=first_name
${recipientfirstnameError}  id=activity_add_address_firstname_error

${recipientlastnameTF}      id=last_name
${recipientlastnameError}   id=activity_add_address_lastname_error


#EMPTY mandatory fields error messages
# ${emptyTFerror}       xpath=//android.widget.TextView[contains(@,' ')]
# ${streetnoemptyError}       xpath=//


#NON-MANDATORY FIELDS
${flat/houseTF}             xpath=//android.widget.EditText[contains(@text,'Flat / House Number')]
${flat/houseError}          xpath=//android.widget.TextView[contains(@index,'1')]

${floorTF}                  xpath=//android.widget.EditText[contains(@text,'Floor Number')]
${floorError}               xpath=//android.widget.TextView[contains(@index,'3')]

${house/buildingnoTF}       xpath=//android.widget.EditText[contains(@text,'House / Building Number')]
${housebuildingNoError}     xpath=//android.widget.TextView[contains(@index,'5')]

${house/buildingnameTF}     xpath=//android.widget.EditText[contains(@text,'House / Building Name')]
${housebuildingNameError}   xpath=//android.widget.TextView[contains(@index,'7')]

${postcountryPH}            xpath=//android.widget.TextView[contains(@index,'16')]
${postcountryValue}         id=address_options_textview
${postcountryDropdown}      id=drop_down_arrow
${searchpostcountry}        id=search_bar
${searchTF}                 id=search_src_text
${choosepostcountry}        xpath=//android.widget.RelativeLayout[contains(@index,'0')]
# ${wardTF}                   xpath=//android.widget.EditText[contains(@text,'Ward')]
# ${wardError}                xpath=//android.widget.TextView[contains(@index,'15')]




