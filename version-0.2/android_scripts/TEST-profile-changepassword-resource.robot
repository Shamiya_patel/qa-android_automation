*** Variables ***
#Input data
${randomoldpassword}    220011
${newshortpass}         1234
${mismatchnewpass}      123-45
${newpassword}          123457

# ERROR MESSAGES
${emptyOldpassword}     Please fill in old password
${emptyNewpassword}     Please fill in new password
${emptConfirmpassword}  Confirm new password


*** Keywords ***
Generate first name and email
    ${emailName} =    Generate Random String    5   [LETTERS]
    ${correctEmail}     Catenate    ${string1}${string2}${emailName}${zwoopmail}
    Log To Console    \nEmail to be used for registration is ${correctEmail}
    Set Global Variable    ${correctEmail}

    ${name}=   Generate Random String  2   [LETTERS]
    ${firstName}    Catenate    ${firstname}${name}
    Log To Console    \nFirst name to be used for registration is ${firstName}
    Set Global Variable    ${firstName}

Go to profile page
    Click Element   ${accountsettingsIcon}
    Wait Until Element Is Visible   ${accountpageTitle}     10
    ${page}     Get Text    ${accountpageTitle}
    Log To Console  \nRedirected to '${page}' page

    Click Element   ${profileIcon}
    Wait Until Element Is Visible   ${profilepageTitle}     10
    ${page}     Get Text    ${profilepageTitle}
    Log To Console  \nRedirected to '${page}' page

Choose change password
    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     710     230     205
    ...     ELSE    Swipe  550  1170    550     300
    # Swipe   720     2137    720     1960 ---HTC
    Page Should Contain Element     ${changepasswordButton}
    Click Element   ${changepasswordButton}
    Log To Console  \nClicked change password button

    Wait Until Element Is Visible   ${changepasswordpage}   10
    ${page}     Get Text    ${changepasswordpage}
    Log To Console  \nRedirected to '${page}' page

Empty credentials test
    Wait Until Element Is Visible   ${enterOldpassword}    10
    Page Should Contain Element     ${enterOldpassword}
    Page Should Contain Element     ${enterNewpassword}
    Page Should Contain Element     ${enterConfirmpassword}
    Log To Console  \nAll element visible

    Click Element   ${save/doneButton}
    Wait Until Element Is Visible   ${oldpasswordError}   10
    ${message1}     Get Text    ${oldpasswordError}
    Log To Console  \Error message is '${message1}'
    Sleep   1
    Wait Until Element Is Visible   ${newpasswordError}   10
    ${message2}     Get Text    ${newpasswordError}
    Log To Console  \Error message is '${message2}'
    Sleep   1
    Wait Until Element Is Visible   ${confirmpasswordError}   10
    ${message3}     Get Text    ${confirmpasswordError}
    Log To Console  \Error message is '${message3}'

Change pasword test
    [Arguments]     ${old}  ${new}  ${confirmnew}   ${error1}   ${error2}   ${error3}
    Wait Until Element Is Visible   ${enterOldpassword}    10
    Page Should Contain Element     ${enterOldpassword}
    Page Should Contain Element     ${enterNewpassword}
    Page Should Contain Element     ${enterConfirmpassword}
    Log To Console  \nAll element visible

    Input Text   ${enterOldpassword}   ${old}
    Log To Console  \nEntered old password

    Input Text   ${enterNewpassword}    ${new}
    Log To Console  \nEntered new password

    Input Text   ${enterConfirmpassword}    ${confirmnew}
    Log To Console  \nRe-entered new pasword to confirm

    Click Element   ${save/doneButton}
    Wait Until Element Is Visible   ${error1}   10
    ${message1}     Get Text    ${error1}
    Log To Console  \Error message is '${message1}'
    Sleep   1
    Wait Until Element Is Visible   ${error2}   10
    ${message2}     Get Text    ${error2}
    Log To Console  \Error message is '${message2}'
    Sleep   1
    Wait Until Element Is Visible   ${error3}   10
    ${message3}     Get Text    ${error3}
    Log To Console  \Error message is '${message3}'

Correct change password
    Wait Until Element Is Visible   ${enterOldpassword}    10
    Page Should Contain Element     ${enterOldpassword}
    Page Should Contain Element     ${enterNewpassword}
    Page Should Contain Element     ${enterConfirmpassword}
    Log To Console  \nAll element visible

    Input Text   ${enterOldpassword}   ${registerPassword}
    Log To Console  \nEntered old password

    Input Text   ${enterNewpassword}    ${newpassword}
    Log To Console  \nEntered new password

    Input Text   ${enterConfirmpassword}    ${newpassword}
    Log To Console  \nRe-entered new pasword to confirm

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save
    Wait Until Element Is Visible   ${passwordchangedMessage}   10
    ${message}     Get Text    ${passwordchangedMessage}
    Log To Console  \n'${message}' message displayed
    Sleep   1
    Wait Until Element Is Visible   ${profilepageTitle}   10
    Log To Console  \nRedirected back to profile page

Go back to account page from profile page and logout
    Click Element   ${appbackButton}
    Wait Until Element Is Visible   ${accountpageTitle}     10
    ${page}     Get Text    ${accountpageTitle}
    Log To Console  \nRedirected back to '${page}' page

    Click Element   ${logoutButton}
    Wait Until Element Is Visible   ${zwoopLogo}      20
    Log To Console  \nLogout successful


