*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            add_card_locator.robot
Resource            account_locators.robot

Resource            TEST-myaccount-addcard-resource.robot
Resource            TEST-144-email-registration-resource.robot
Resource            TEST-profile-changepassword-resource.robot
Resource            TEST-email-login-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Launch app and login to an existing account
#LOGIN
    Verify login page content
    Valid login test    ${correctSigninemail}    ${loginPassword}   ${register/loginButton}
    Log To Console  \nLogged in successful

#REGISTER
    # ${emailName} =    Generate Random String    5   [LETTERS]
    # ${correctEmail}     Catenate    ${string1}${string2}${emailName}${zwoopmail}
    # Log To Console    \nEmail to be used for registration is ${correctEmail}
    # Set Global Variable    ${correctEmail}

    # ${name}=   Generate Random String  2   [LETTERS]
    # ${firstName}    Catenate    ${firstname}${name}
    # Log To Console    \nFirst name to be used for registration is ${firstName}
    # Set Global Variable    ${firstName}
    # Choose create account link
    # Input correct credentials       ${firstName}     ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}
    # Log To Console  \nSuccessfully created a new account and landed on landing Page

2. Go to accounts > Cards
    Go to cards page and verify
    Fill in card form and save

3. Remove card
    Delete card

4. Go to accounts and logout
    Go back to account page from profile page and logout