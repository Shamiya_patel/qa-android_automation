#Log-in page locators

*** Variables ***
#Commonbiz.zwoop.android:id/logo_image
${zwoopLogo}                    biz.zwoop.android:id/logo_image
${stateloginregisterHeader}     id=state_header
${enteremailField}              id=email_edittext
${enterpasswordField}           id=password_edittext
${register/loginButton}         id=submit_button
# ${successloginCheck}            accessibility_id=Search
${successloginCheck}            id=search_src_text

#Log-in
${forgotPasswordbutton}         id=forgot_password_button
${registernewaccountButton}     id=no_account_button

#Register / log-in methods
${loginwithGoogle}              id=google_sign_in_button
${loginwithFacebook}            id=fb_sign_in_button

#Facebook login locators
${enterFBemail}                 id=m_login_email
${enterFBpassword}              id= m_login_password
${cancelfbregistration}         xpath=//android.widget.TextView[contains(@text,'Facebook registration was cancelled.')]
# ${loginFBbutton1}               id= u_0_5
${fbloginbutton2}               xpath=//android.widget.Button[contains(@text,'Log In')]
# ${fbloginButton3}               id=u_0_6
${confirmpageText}              id=m-future-page-header-title
${continueandconfirmAccess}     id=u_0_3
${cancelconfirmButton}          xpath=//android.widget.Button[contains(@text,'Cancel')]

#Google login locators
${googleaccountslistTitle}          xpath=//android.widget.TextView[contains(@text,'Choose account for Zwoop')]
${cancelgoogleregistrationError}    id=snackbar_text
${firstaccountfromList}             com.google.android.gms:id/account_display_name
${confirmgoogleaccountButton}       com.google.android.gms:id/accept_button

#Add new google account[Commeneted - as per team decission]
#J1
${joneaddnewgoogleaccountButton}        xpath=//android.widget.Button[contains(@text,'Use another account')]
# #HTC,note4
${addnewgoogleaccountButton}        android:id/button2
${googlesigninpageTitle}            headingText
${entergmailemailField}             identifierId
${emailnextButton}                  identifierNext
${entergmailpassword}               xpath=//android.widget.EditText[contains(@index,'0')]
${passwordnextButton}               passwordNext
${termsandconditionLink}            tosLink
${NoteagreegoogleButton}            next
${HTCagreegoogleButton}             signinconsentNext

#Register
${enternameField}           id=first_name_edittext
${entersurnameField}        id=last_name_textview
${inappbacktoLoginpage}     id=back_button_actionbar

#Log-in error messages [Do get text and check, if doesnt seem promising use xpath]
#Error messages [Not used]
${emptyemailError}          id=email_error_textview
${emptypasswordError}       id=password_error_textview

${emptynameError}           id=first_name_error_textview
${emptysurnameError}        id=last_name_error_textview
