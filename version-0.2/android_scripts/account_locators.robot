#ACCOUNT PAGE LOCATORS

*** Variables ***
# ${accountsettingsIcon}     id=action_account
# OR
${accountsettingsIcon}      id=Account

${accountpageTitle}        xpath=//android.widget.TextView[contains(@text,'Account')]

${ordersIcon}              id=order_icon

${addressesIcon}           id=addresses_icon

${cardsIcon}               id=cards_icon

${profileIcon}             id=profile_icon

${accountscustomerName}    biz.zwoop.android:id/customer_name

${faqIcon}                 id=faq_icon

# ${ABOUT_ICON}               id=about_icon

${logoutButton}            biz.zwoop.android:id/log_out_button


#------------------Depricated-----------------------#
# ${PROFILE_IMAGE}            id=profile_image
# ${PREFERENCES_ICON}         id=settings_icon
# ${FEEDBACK_ICON}            id=feedback_icon

