#Notification locators

*** Variables ***
${BackdoorIcon}         id=action_debug_backdoor
${backdoorpageTitle}    xpath=//android.widget.TextView[contains(@text,'Debug Backdoor')]
${infoTab}              xpath=//android.widget.TextView[contains(@text,'INFO')]

${actionTab}            xpath=//android.widget.TextView[contains(@text,'ACTIONS')]
${primeactionTab}       xpath=//android.widget.TextView[contains(@text,'Actions')]

${bluedotNotifications}         id=notification_badge
${bestpriceTabnotification}     id=best_price_tab_title

${deletenotificationButton}         id=action_delete
${canceldeletenotificationButton}   id=action_clear

#-----------------------------------NOTIFICATION TYPE - Backdoor-------------------------------------------#
# ${notsupportedaddressRadioButton}       id=radio_button_type_address_not_supported
# ${totalpriceupdateRadioButton}          id=radio_button_type_total_price_update
# ${purchasecompleteRadioButton}          id=radio_button_type_purchase_completed
# ${purchasefailedRadioButton}            id=radio_button_type_purchase_failed
# ${wrongcardinfoRadioButton}             id=radio_button_type_wrong_card_information
# ${merchantnotrespondingRadioButton}     id=radio_button_type_merchant_not_responding
# ${merchantoutofstockRadioButton}        id=radio_button_type_merchant_out_of_stock
# ${unknownnotificationradioButton}       id=radio_button_type_unknown

#Notification image type
${defaultimageRadioButton}              id=radio_button_default_image
${dynamicimageRadioButton}              id=radio_button_dynamic_image

# ${unknownnotificationradioButton}   xpath=//android.widget.RadioButton[contains(@text,'Unknown')]
# ${notificationpanel}                id=statusBarBackground

#Trigger notification button
${triggernotificationButton}             id=trigger_notification_button

#-------------------------------------Android permissions----------------------------------------#
${allowButton}      id=com.android.packageinstaller:id/permission_allow_button
${denyButton}       id=com.android.packageinstaller:id/permission_deny_button

#-------------------------------------NOTIFICATION DRAWER - android system-----------------------------------------#
${notificationIcon}                 id=right_icon
${notificationTime}                 id=time

#----------------------------------NOTIFICATION TITLE - Drawer----------------------------------------------#
${addresstitleText}             xpath=//android.widget.TextView[contains(@text,'Address Not Supported')]
# ${addressbodyText}          xpath=//android.widget.TextView[contains(@index,'1')]
${totalpriceupdateText}         xpath=//android.widget.TextView[contains(@text,'Total Price Update')]
${purchasecompletedText}        xpath=//android.widget.TextView[contains(@text,'Purchase Completed')]
${purchsefailedText}            xpath=//android.widget.TextView[contains(@text,'Purchase Failed')]
${wrongcardinfoText}            xpath=//android.widget.TextView[contains(@text,'Wrong Card Information')]
${merchantnotrespondingText}    xpath=//android.widget.TextView[contains(@text,'Merchant Not Responding')]
${merchantoutofstockText}       xpath=//android.widget.TextView[contains(@text,'Merchant Out Of Stock')]
${unknownText}                  xpath=//android.widget.TextView[contains(@text,'Zwoop Notification')]

#---------------------------------NOTIFICATION BODY - Drawer-------------------------------------------------#
# ${notificationappnameText}          id=app_name_text
# ${notificationText}                 xpath=//android.widget.TextView[contains(@text,'Mock body text for notification')]
# id=big_text

${notificationtitleSony}            xpath=//android.widget.TextView[contains(@text,'Zwoop')]
# android:id/title

#NOTIFICATION PAGE
${notificationpageTitle}            xpath=//android.widget.TextView[contains(@text,'Notifications')]

#---------------------------------------------------APP NOTIFICATION ---------------------------------------------------------#
${appnotification}              id=notification_title
${appnotificationTimestamp}     id=notification_time
${appnotificationImage}         id=notification_image

#GENERIC NEXT PAGE REDIRECTION AS PER MOCK DATA
${notificationRedirectionCheck}     id=activity_order_status_image
${notificationdetailPage}           xpath=//android.widget.TextView[contains(@text,'Amazon-Mock')]
#PURCHASE COMPLETED REDIRECTION
${purchasecompletedCheck}       id=activity_order_details_order_id_label

# ${notsupportedAddress}      xpath=//android.widget.TextView[contains(@text,'')]
# ${totalPriceUpdate}         xpath=//android.widget.TextView[contains(@text,'Total Price Update')]
# ${purchaseCompleted}        xpath=//android.widget.TextView[contains(@text,'Purchase Completed')]
# ${purchaseFailed}           xpath=//android.widget.TextView[contains(@text,'Purchase Failed')]
# ${wrongCardInfo}            xpath=//android.widget.TextView[contains(@text,'Wrong card information')]
# ${Merchantnotresponding}    xpath=//android.widget.TextView[contains(@text,'')]
# ${Merchantoutofstock}       xpath=//android.widget.TextView[contains(@text,'')]
# ${UnknownNotif}             xpath=//android.widget.TextView[contains(@text,'')]




