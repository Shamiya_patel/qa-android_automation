#Forgot password page locators

*** Variables ***
${backtologinpageButton}    id=back_button_actionbar

${intromessageText}         id=intro_message
${enteremailField}          id=email_edittext
${resetpasswordbutton}      id=submit_button

#ERRORS
${forgotpasswordError}      id=email_error_textview
${successMessage}           xpath=//android.widget.TextView[contains(@text,'Password request sent')]
# Password request could not be sent. Please try again