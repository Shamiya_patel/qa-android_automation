*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            forgotpassword_locators.robot

Resource            TEST-email-login-resource.robot
Resource            TEST-forgotpassword-resource.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Verify login page
    Verify login page content
    Choose forgot password link and verify

2. Test forgot password without entering email
    Empty email test    ${forgotpasswordError}

3. Test forgot password with invalid email
    Enter email and send    ${invalidemail}  ${forgotpasswordError}

4. Test forgot password with a non-registered email
    Enter email and send    ${nonregisteredemail}  ${forgotpasswordError}

5. Choose forgot password with unconfirmed email
    Enter email and send    ${unconfirmedEmail}     ${forgotpasswordError}

6. Enter valid email and send
    Enter valid email

7. Reset password again using same email
    Enter email and send    ${forgotpasswordvalidEmail}     ${forgotpasswordError}

