#CARD PAGE LOCATORS

*** Variables ***
${cardpageTitle}           xpath=//android.widget.TextView[contains(@text,'Cards')]
${emptycardpageImage}      id=empty_card_list_icon

${addnewcardIcon}          id=fab_new_card
${addnewcardpageTitle}     xpath=//android.widget.TextView[contains(@text,'Add card')]
${savedcardnameTitle}      id=card_name

${successavecardMessage}        xpath=//android.widget.TextView[contains(@text,'Card saved')]
# ${deletecardsuccessMessage}     xpath=//android.widget.TextView[contains(@text,'')]

#CARD FORM
${cardnicknameTF}               id=nickname
${nameoncardTF}                 id=name_on_card
${cardnoTF}                     id=card_number
${visacreditcardRadiobutton}    id= radio_visa_credit
${visadebitcardRadiobutton}     id=radio_visa_debit
${expirymonthSelector}          id=expiry_month
${expiryyearSelector}           id=expiry_year
${cvvTF}                        id=cvv
${noaddress}                    id=address_wrapper_no_address
# ---------------------------
# ${CARDNAME}                   id=card_name
# ${DELETE_CARD}                accessibility_id=Delete
# ${CANCELDELETECARD}           accessibility_id=Cancel

# ${TEXTNICKNAME}         xpath=//android.widget.TextView[contains(@text,'Nickname')]
# ${TEXTNAMEONCARD}       xpath=//android.widget.TextView[contains(@text,'Name on card')]
# ${TEXTCARDNUMBER}       xpath=//android.widget.TextView[contains(@text,'Card number')]
# ${TEXTEXPIRYDATE}       xpath=//android.widget.TextView[contains(@text,'Expiry Date')]
# ${TEXTCVV}              xpath=//android.widget.TextView[contains(@text,'CVV')]
# ${TEXTADDRESS}          xpath=//android.widget.TextView[contains(@text,'Address')]

# ${EXPIRYMONTHPLACEHOLDER}    xpath=//android.widget.EditText[contains(@text,'MM')]
# ${EXPIRYYEARPLACEHOLDER}     xpath=//android.widget.EditText[contains(@text,'YYYY')]