*** Variables ***
#------------------------------INPUT DATA------------------------------#
#HK
${countrynamehk}     Hong Kong
${streetnamehk}      Jervois Street
#UK
# ${countrynameuk}         United Kingdom
${ukcode}               077
${ukstreetname}         Carew Terrace
${uktowncityname}       Denton
${ukcounty}               Greater Manchester
# ${district}             Manchester
${postcode}             M34 6FE
${recipientfirstName}   Mario
${recipientlastName}    Bross


#------------------------------PLACEHOLDERS------------------------------#
${addressplaceholder}               Address Title

# ${countrytitle}                     Country
${ukcountryplaceholder}             United Kingdom

${flathousenoplaceholder}           Flat / House Number
${floornoplaceholder}               Floor Number

${housebuildingnoplaceholder}       House / Building Number
${housebuildingnameplaceholder}     House / Building Name

${ukpostcodeplaceholder}            +44
${ukphonenumberplaceholder}         Contact number e.g. 07700900515

# ${firstnameplaceholder}             First name
# ${lastnameplaceholder}              Last name

${streetnoplaceholder}              Street Number
${streetnameplaceholder}            Street Name

${towncityplaceholder}              Town / City

${countyplaceholder}                County
# ${wardplaceholder}                  Ward
${postcountryplaceholder}           Post Country

# ${districtplaceholder}              District

${postcodeplaceholder}              Post Code, e.g. N1 9LH

${firstnameplaceholder}             Recipient first name
${lastnameplaceholder}              Recipient last name

#HK
# ${hkcountryplaceholder}             Hong Kong
# ${hkpostcodeplaceholder}            +852
# # ${hkphonenumberplaceholder}
# ${hkareadropdownplaceholder}        Central and Western
# ${hkcityplaceholder}                Hong Kong Island

*** Keywords ***
# Go to add new address page
#     Click Element   ${addnewaddressLocator}
#     Wait Until Element Is Visible   ${addnewaddresspagetitleLocator}   10
#     Log To Console  \nEnter add address page

Go to addresses and verify
    Click Element   ${accountsettingsIcon}
    Wait Until Element Is Visible   ${accountpageTitle}  10
    ${page}     Get Text    ${accountpageTitle}
    Log To Console  \nRedirected to '${page}' page

    Click Element   ${addressesIcon}
    Wait Until Element Is Visible   ${addresspageTitle}  10
    ${page}     Get Text    ${addresspageTitle}
    Log To Console  \nRedirected to '${page}' page

    Page Should Contain Element     ${addnewaddressIcon}
    Page Should Contain Element     ${emptyaddresspageimage}
    Log To Console  \nAccount doesnt have any address.
    Sleep   1

Choose add icon and verify page content
    Click Element   ${addnewaddressIcon}
    Log To Console  \nClicked on add new address icon
    Wait Until Element Is Visible   ${addnewaddresspageTitle}   10
    ${page}     Get Text    ${addnewaddresspageTitle}
    Log To Console  \nRedirected to '${page}' page

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe   350     830     350     690
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe   350     830     350     690     1000
    ...     ELSE    Log To Console  \nDevice isnt in above list

    Page Should Contain Element     ${addresstitleTF}
    ${placeholder1}  Get Text    ${addresstitleTF}
    Should Be Equal As Strings  ${placeholder1}     ${addressplaceholder}
    Log To Console  \n'${placeholder1}', placeholder text matched

    Page Should Contain Element     ${countryTitle}
    Page Should Contain Element     ${selectedCountryname}
    ${placeholder2}  Get Text    ${selectedCountryname}
    Should Be Equal As Strings  ${placeholder2}     ${ukcountryplaceholder}
    Log To Console  \nCountry selected is '${placeholder2}', placeholder text matched

    Page Should Contain Element     ${flat/houseTF}
    ${placeholder3}  Get Text    ${flat/houseTF}
    Should Be Equal As Strings  ${placeholder3}     ${flathousenoplaceholder}
    Log To Console  \n'${placeholder3}', placeholder text matched

    Page Should Contain Element     ${floorTF}
    ${placeholder4}  Get Text    ${floorTF}
    Should Be Equal As Strings  ${placeholder4}     ${floornoplaceholder}
    Log To Console  \n'${placeholder4}', placeholder text matched

    Page Should Contain Element     ${house/buildingnoTF}
    ${placeholder5}     Get Text    ${house/buildingnoTF}
    Should Be Equal As Strings  ${placeholder5}    ${housebuildingnoplaceholder}
    Log To Console  \n'${placeholder5}', place holder matched

    Page Should Contain Element     ${house/buildingnameTF}
    ${placeholder6}     Get Text   ${house/buildingnameTF}
    Should Be Equal As Strings  ${placeholder6}  ${housebuildingnameplaceholder}
    Log To Console  \n'${placeholder6}', place holder matched

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe   400     1240    400     300
    ...     ELSE IF  '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe   400     1700    400     800
    ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe   720     2500    720     800
    ...     ELSE IF  '${DEVICE_NAME}'=='an_google_pixeltwo'
    ...     Swipe   520     1760    520     550
    ...     ELSE    Swipe  720  2500    720     800
    Log To Console  \nScrolling through address form
    Sleep   1

    # Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    # ...     Swipe   230     780     230     200
    # ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_prime'
    # ...     Swipe   400     1240    400     280
    # ...     ELSE IF  '${DEVICE_NAME}'=='an_sony_xp'
    # ...     Swipe   400     1700    400     605
    # ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_notefour'
    # ...     Swipe   720     2500    720     800
    # ...     ELSE    Swipe  720  2500    720     800
    # Log To Console  \nScrolling through address form
    # Sleep   1
    # Click Element   ${postcodeTF}
    # Log To Console  \nClicked element postcode for further scroll

    Page Should Contain Element    ${streetnumberTF}
    ${placeholder7}     Get Text   ${streetnumberTF}
    Should Be Equal As Strings  ${placeholder7}    ${streetnoplaceholder}
    Log To Console  \n'${placeholder7}', place holder matched

    Page Should Contain Element    ${streetnameTF}
    ${placeholder8}     Get Text   ${streetnameTF}
    Should Be Equal As Strings  ${placeholder8}  ${streetnameplaceholder}
    Log To Console  \n'${placeholder8}', place holder matched

    Page Should Contain Element    ${town/cityTF}
    ${placeholder9}     Get Text   ${town/cityTF}
    Should Be Equal As Strings  ${placeholder9}  ${towncityplaceholder}
    Log To Console  \n'${placeholder9}', place holder matched

    Page Should Contain Element     ${countyTF}
    ${placeholder10}     Get Text   ${countyTF}
    Should Be Equal As Strings  ${placeholder10}  ${countyplaceholder}
    Log To Console  \n'${placeholder10}', place holder matched

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200
    Sleep   2

    Page Should Contain Element     ${postcountryPH}
    ${placeholder11}     Get Text   ${postcountryPH}
    Should Be Equal As Strings  ${placeholder11}  ${postcountryplaceholder}
    Log To Console  \n'${placeholder11}', place holder matched

    Page Should Contain Element     ${postcountryValue}
    ${placeholder17}     Get Text   ${postcountryValue}
    # Should Be Equal As Strings  ${placeholder11}  ${districtplaceholder}
    Log To Console  \nSelected post country is '${placeholder17}'

    Page Should Contain Element     ${postcodeTF}
    Page Should Contain Element     ${postcountryDropdown}
    ${placeholder12}     Get Text   ${postcodeTF}
    Should Be Equal As Strings  ${placeholder12}  ${postcodeplaceholder}
    Log To Console  \n'${placeholder12}', place holder matched

    Page Should Contain Element     ${flagcountrycode}
    Page Should Contain Element     ${countrycodeNumber}
    ${placeholder13}     Get Text    ${countrycodeNumber}
    Should Be Equal As Strings  ${placeholder13}  ${ukpostcodeplaceholder}
    Log To Console  \n'${placeholder13}', place holder matched

    Page Should Contain Element     ${phonenumberTF}
    ${placeholder14}     Get Text   ${phonenumberTF}
    Should Be Equal As Strings  ${placeholder14}  ${ukphonenumberplaceholder}
    Log To Console  \n'${placeholder14}', place holder matched

    Page Should Contain Element     ${recipientfirstnameTF}
    ${placeholder15}     Get Text   ${recipientfirstnameTF}
    Should Be Equal As Strings  ${placeholder15}  ${firstnameplaceholder}
    Log To Console  \n'${placeholder15}', place holder matched

    Page Should Contain Element     ${recipientlastnameTF}
    ${placeholder16}     Get Text   ${recipientlastnameTF}
    Should Be Equal As Strings  ${placeholder16}  ${lastnameplaceholder}
    Log To Console  \n'${placeholder16}', place holder matched

    Click Element   ${appbackButton}
    Wait Until Element Is Visible   ${addresspageTitle}  10
    ${page}     Get Text    ${addresspageTitle}
    Log To Console  \nRedirected back to '${page}' page
    Log To Console      \nAdd new address page content verified successfully

Save empty from and expect error
    Click Element   ${addnewaddressIcon}
    Log To Console  \nClicked on add new address icon
    Wait Until Element Is Visible   ${addnewaddresspageTitle}   10
    ${page}     Get Text    ${addnewaddresspageTitle}
    Log To Console  \nRedirected to '${page}' page

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe   350     830     350     690
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe   350     830     350     690     1000
    ...     ELSE    Log To Console  \nDevice isnt in above list
    Click Element   ${save/doneButton}
    Log To Console  \nClicked save

    Wait Until Element Is Visible   ${addresstitleError}
    ${error1}   Get Text    ${addresstitleError}
    Log To Console  \nError message is '${error1}'

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe   400     1250    400     280
    ...     ELSE IF  '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe   400     1700    400     900
    ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe   720     2500    720     700
    ...     ELSE IF  '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Swipe   720     2500    720     800
    ...     ELSE IF  '${DEVICE_NAME}'=='an_google_pixeltwo'
    ...     Swipe   520     1760    520     500
    ...     ELSE    Log To Console  \nNone of the devices matched
    Log To Console  \nScrolling through address form
    Sleep   1

    Wait Until Element Is Visible   ${streetnoError}    10
    ${error2}   Get Text    ${streetnoError}
    Log To Console  \nError message is '${error2}'

    Wait Until Element Is Visible   ${streetnameError}
    ${error3}   Get Text    ${streetnameError}
    Log To Console  \nError message is '${error3}'


    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200
    Sleep   1

    Wait Until Element Is Visible   ${town/cityError}
    ${error4}   Get Text    ${town/cityError}
    Log To Console  \nError message is '${error4}'

    Wait Until Element Is Visible   ${countyError}
    ${error5}   Get Text    ${countyError}
    Log To Console  \nError message is '${error5}'

    Wait Until Element Is Visible   ${postcodeError}
    ${error6}   Get Text    ${postcodeError}
    Log To Console  \nError message is '${error6}'

    Wait Until Element Is Visible   ${phonenumberError}
    ${error7}   Get Text    ${phonenumberError}
    Log To Console  \nError message is '${error7}'

    Wait Until Element Is Visible   ${recipientfirstnameError}
    ${error8}   Get Text    ${recipientfirstnameError}
    Log To Console  \nError message is '${error8}'

    Wait Until Element Is Visible   ${recipientlastnameError}
    ${error9}   Get Text    ${recipientlastnameError}
    Log To Console  \nError message is '${error9}'

    Click Element   ${appbackButton}
    Wait Until Element Is Visible   ${addresspageTitle}  10
    ${page}     Get Text    ${addresspageTitle}
    Log To Console  \nRedirected back to '${page}' page
    Log To Console      \nAll mandatory fields display error messages successfully

Input credentials for UK address
    Click Element   ${addnewaddressIcon}
    Log To Console  \nClicked on add new address icon

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe   350     830     350     690
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe   350     830     350     690     1000
    ...     ELSE    Log To Console  \nDevice isnt in above list

    Wait Until Element Is Visible   ${addnewaddresspageTitle}   10
    ${page}     Get Text    ${addnewaddresspageTitle}
    Log To Console  \nRedirected to '${page}' page

    ${string}   Generate Random String  3   [LETTERS]
    ${addressTitle}     Catenate    ${string}-${DEVICE_NAME}-address
    Set Global Variable     ${newaddresstitle}   ${addressTitle}
    Sleep   1
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF  '${DEVICE_NAME}'=='an_google_pixeltwo'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE    Log     Device name didnt match
    Log To Console      \nAddress title '${newaddresstitle}' added successfully

    Page Should Contain Element     ${selectedCountryname}
    ${placeholder1}  Get Text       ${selectedCountryname}
    Should Be Equal As Strings  ${placeholder1}     ${ukcountryplaceholder}
    Log To Console  \nCountry selected is '${placeholder1}'

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe   400     1240    400     290
    ...     ELSE IF  '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe   400     1700    400     800
    ...     ELSE IF  '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe   720     2500    720     800
    ...     ELSE IF  '${DEVICE_NAME}'=='an_google_pixeltwo'
    ...     Swipe   520     1760    520     550
    ...     ELSE    Swipe  720  2500    720     800
    Log To Console  \nScrolling through address form
    Sleep   1

    ${num}   Generate Random String  3   [NUMBERS]
    Log To Console  \nFlat/house/building/street number is ${num}
    Set Global Variable     ${streetNum}   ${num}
    Wait Until Element Is Visible   ${streetnumberTF}  10
    Input Text  ${streetnumberTF}   ${streetNum}
    Log To Console  \nEntered '${streetNum}' street number successfully

    Input Text  ${streetnameTF}     ${ukstreetname}
    Log To Console  \nEntered '${ukstreetname}' street name successfully

    Input Text  ${town/cityTF}     ${uktowncityname}
    Log To Console  \nEntered '${uktowncityname}' town/city name successfully

    Input Text  ${countyTF}     ${ukcounty}
    Log To Console  \nEntered '${ukcounty}' county successfully

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200
    Sleep   1

    # Page Should Contain Element    ${postcountryPH}
    # ${placeholder2}     Get Text   ${postcountryPH}
    # Log To Console  \n'${placeholder2}', place holder matched

    # Page Should Contain Element    ${postcountryValue}
    # ${value}    Get Text    ${postcountryValue}
    # Log To Console  \n'${value}', is post country

    Input Text  ${postcodeTF}     ${postcode}
    Log To Console  \nEntered '${postcode}' postcode successfully

    ${phno}     Generate Random String      8   [NUMBERS]
    Log To Console  \nGenerated UK phone number is '${phno}'
    Set Global Variable     ${randomphoneno}     ${phno}
    ${ukphoneNo}    Catenate    ${ukcode}${randomphoneno}
    Input Text  ${phonenumberTF}     ${ukphoneNo}
    Log To Console  \nEntered contact number '${ukphoneNo}' successfully

    Input Text  ${recipientfirstnameTF}     ${recipientfirstName}
    Log To Console  \nEntered '${recipientfirstName}' first name

    Input Text  ${recipientlastnameTF}      ${recipientlastName}
    Log To Console  \nEntered '${recipientlastName}' last name

    Click Element   ${save/doneButton}
    Log To Console  \nClicked save button

    Wait Until Element Is Visible   ${addresspageTitle}  15
    ${page}    Get Text    ${addresspageTitle}
    Log To Console  \nRedirected back to '${page}' page

    # Click Element   ${appbackButton}
    # Wait Until Element Is Visible   ${addresspageTitle}     10
    # ${page}     Get Text    ${addresspageTitle}
    # Log To Console  \nRedirected back to '${page}' page

Validate addresss after adding
    Wait Until Element Is Visible   ${addresstitleTF}  15
    ${title}    Get Text    ${addresstitleTF}
    Log To Console  \nAddress saved successfully, title is '${title}'

# Check address page content prior selecting country
#     Page Should Contain Element     ${selectedCountryname}
#     ${placeholder2}  Get Text    ${selectedCountryname}
#     Should Be Equal As Strings  ${placeholder2}     ${ukcountryplaceholder}
#     Log To Console  \nPlaceholder text matched, selected country is '${ukcountryplaceholder}'

#     # Element Text Should Be  ${formaddresstitleLocator}   ${addresstitle}
#     # Element Text Should Be  ${countrydropdownbuttonLocator}     ${selectcountrydropdown}
#     Page Should Contain Element     ${flat/houseTF}
#     Page Should Contain Element     ${save/doneButton}
#     Page Should Contain Element     ${appbackButton}
#     Page Should Contain Element     ${floorTF}
#     Click Element   ${appbackButton}
#     Log To Console      \nChecked page content prior selecting country

# Select UK and check page content
#     Click Element   ${countrydropdownbuttonLocator}
#     Click Element   ${searchcountrybuttonLocator}
#     Input Text      ${entersearchCountryLocator}    ${countrynamuk}
#     Click Element   ${chooseCountrynameLocator}
#     Log To Console      \nSelected country is UK

#     Page Should Contain Element     ${selectedcountrynameLocator}
#     ${placeholder3}     Get Text    ${selectedcountrynameLocator}
#     Should Be Equal As Strings  ${placeholder3}      ${ukcountryplaceholder}
#     Log To Console  \n${placeholder3} - country selected placeholder matched

#     Page Should Contain Element     ${selectedpostcodeLocator}
#     ${placeholder4}     Get Text    ${selectedpostcodeLocator}
#     Should Be Equal As Strings  ${placeholder4}     ${ukpostcodeplaceholder}
#     Log To Console  \n${placeholder4} - post code placeholder matched as per selected country

#     Page Should Contain Element     ${phonenumberLocator}
#     ${placeholder17}     Get Text    ${phonenumberLocator}
#     Should Be Equal As Strings  ${placeholder17}    ${ukphonenumberplaceholder}
#     Log To Console  \n${placeholder17} - place holder matched

#     Page Should Contain Element     ${firstnameLocator}
#     ${placeholder5}     Get Text    ${firstnameLocator}
#     Should Be Equal As Strings  ${placeholder5}           ${firstnameplaceholder}
#     Log To Console  \n${placeholder5} - place holder matched

#     Page Should Contain Element     ${lastnameLocator}
#     ${placeholder6}     Get Text    ${lastnameLocator}
#     Should Be Equal As Strings  ${placeholder6}            ${lastnameplaceholder}
#     Log To Console  \n${placeholder6} - place holder matched

#     Page Should Contain Element     ${flat/housenoLocator}
#     ${placeholder7}     Get Text    ${flat/housenoLocator}
#     Should Be Equal As Strings  ${placeholder7}        ${flathousenoplaceholder}
#     Log To Console  \n${placeholder7} - place holder matched

#     Page Should Contain Element     ${floornoLocator}
#     ${placeholder8}     Get Text    ${floornoLocator}
#     Should Be Equal As Strings  ${placeholder8}             ${floornoplaceholder}
#     Log To Console  \n${placeholder8} place holder matched

#     Page Should Contain Element     ${house/buildingnoLocator}
#     ${placeholder9}     Get Text    ${house/buildingnoLocator}
#     Should Be Equal As Strings  ${placeholder9}    ${housebuildingnoplaceholder}
#     Log To Console  \n${placeholder9} - place holder matched

#     Page Should Contain Element     ${house/buildingnameLocator}
#     ${placeholder10}     Get Text   ${house/buildingnameLocator}
#     Should Be Equal As Strings  ${placeholder10}  ${housebuildingnameplaceholder}
#     Log To Console  \n${placeholder10} - place holder matched

#     Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   230     780     230     200
#     ...     ELSE    Swipe  550  1170    550     200     1000
#     Sleep   2

#     Page Should Contain Element     ${streetnoLocator}
#     ${placeholder11}     Get Text   ${streetnoLocator}
#     Should Be Equal As Strings  ${placeholder11}    ${streetnoplaceholder}
#     Log To Console  \n${placeholder11} place holder matched

#     Page Should Contain Element     ${streetnameLocator}
#     ${placeholder12}     Get Text   ${streetnameLocator}
#     Should Be Equal As Strings  ${placeholder12}  ${streetnameplaceholder}
#     Log To Console  \n${placeholder12} place holder matched

#     Page Should Contain Element     ${uktownLocator}
#     ${placeholder13}     Get Text   ${uktownLocator}
#     Should Be Equal As Strings  ${placeholder13}  ${towncityplaceholder}
#     Log To Console  \n${placeholder13} place holder matched

#     Page Should Contain Element     ${ukwardLocator}
#     ${placeholder14}     Get Text    ${ukwardLocator}
#     Should Be Equal As Strings  ${placeholder14}  ${wardplaceholder}
#     Log To Console  \n${placeholder14} place holder matched

#     Page Should Contain Element     ${districtTF}
#     ${placeholder15}     Get Text    ${districtTF}
#     Should Be Equal As Strings  ${placeholder15}  ${districtplaceholder}
#     Log To Console  \n${placeholder15} place holder matched

#     Page Should Contain Element     ${postcodeTF}
#     ${placeholder16}     Get Text    ${postcodeTF}
#     Should Be Equal As Strings  ${placeholder16}  ${postcodeplaceholder}
#     Log To Console  \n${placeholder16} place holder matched
#     Click Element   ${appbackButton}
#     Log To Console      \nPage content verification successful
#----------#
# Select HK and check page content
#     Click Element   ${countrydropdownbuttonLocator}
#     Click Element   ${searchcountrybuttonLocator}
#     Input Text      ${entersearchCountryLocator}    ${countrynamehk}
#     Click Element   ${chooseCountrynameLocator}
#     Log To Console      \nSelected country is HK

#     Page Should Contain Element     ${selectedcountrynameLocator}
#     ${1placeholder}     Get Text    ${selectedcountrynameLocator}
#     Should Be Equal As Strings  ${1placeholder}     ${hkcountryplaceholder}
#     Log To Console  \n${1placeholder} - country selected placeholder matched

#     Page Should Contain Element     ${selectedpostcodeLocator}
#     ${2placeholder}     Get Text    ${selectedpostcodeLocator}
#     Should Be Equal As Strings  ${2placeholder}     ${hkpostcodeplaceholder}
#     Log To Console  \n${2placeholder} - post code placeholder matched as per selected country

#     Page Should Contain Element     ${firstnameLocator}
#     ${3placeholder}     Get Text    ${firstnameLocator}
#     Should Be Equal As Strings  ${3placeholder}     ${firstnameplaceholder}
#     Log To Console  \n${3placeholder} - place holder matched

#     Page Should Contain Element     ${lastnameLocator}
#     ${4placeholder}     Get Text    ${lastnameLocator}
#     Should Be Equal As Strings  ${4placeholder}     ${lastnameplaceholder}
#     Log To Console  \n${4placeholder} - place holder matched

#     Page Should Contain Element     ${flat/housenoLocator}
#     ${5placeholder}     Get Text    ${flat/housenoLocator}
#     Should Be Equal As Strings  ${5placeholder}     ${flathousenoplaceholder}
#     Log To Console  \n${5placeholder} - place holder matched

#     Page Should Contain Element     ${floornoLocator}
#     ${6placeholder}     Get Text    ${floornoLocator}
#     Should Be Equal As Strings  ${6placeholder}     ${floornoplaceholder}
#     Log To Console  \n${6placeholder} - place holder matched

#     Page Should Contain Element     ${house/buildingnoLocator}
#     ${7placeholder}     Get Text    ${house/buildingnoLocator}
#     Should Be Equal As Strings  ${7placeholder}     ${housebuildingnoplaceholder}
#     Log To Console  \n${7placeholder} - place holder matched

#     Page Should Contain Element     ${house/buildingnameLocator}
#     ${8placeholder}     Get Text    ${house/buildingnameLocator}
#     Should Be Equal As Strings  ${8placeholder}     ${housebuildingnameplaceholder}
#     Log To Console  \n${8placeholder} - place holder matched

#     Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   230     780     230     200
#     ...     ELSE    Swipe  550  1170    550     200     1000
#     Sleep   2

#     Page Should Contain Element     ${streetnoLocator}
#     ${9placeholder}     Get Text    ${streetnoLocator}
#     Should Be Equal As Strings  ${9placeholder}     ${streetnoplaceholder}
#     Log To Console  \n${9placeholder} - place holder matched

#     Page Should Contain Element     ${streetnameLocator}
#     ${10placeholder}     Get Text   ${streetnameLocator}
#     Should Be Equal As Strings  ${10placeholder}     ${streetnameplaceholder}
#     Log To Console  \n${10placeholder} - place holder matched

#     Page Should Contain Element     ${hkareadropdownLocator}
#     ${11placeholder}     Get Text   ${hkareadropdownLocator}
#     Log To Console  \n${11placeholder}
#     Should Be Equal As Strings  ${11placeholder}     ${hkareadropdownplaceholder}
#     Log To Console  \n${11placeholder} - place holder matched

#     Page Should Contain Element     ${hkcitydropdownLocator}
#     ${12placeholder}     Get Text   ${hkcitydropdownLocator}
#     Should Be Equal As Strings  ${12placeholder}     ${hkcityplaceholder}
#     Log To Console  \n${12placeholder} - place holder matched
#     Click Element   ${APP_BACK_BUTTON}
#     Log To Console      \nPage content verified as per selected country HK

# Enter correct HK address
#     ${string1}   Generate Random String  3   [LETTERS]
#     ${addressTitle}     Catenate    ${string1}-${DEVICE_NAME}address
#     Set Global Variable     ${newaddresstitle}   ${addressTitle}
#     Log To Console  \nFinal address title is ${newaddresstitle}

#     Run Keyword If      '${DEVICE_NAME}'=='note'
#     ...     Input Text      ${formaddresstitleLocator}    ${newaddresstitle}
#     ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Input Text      ${formaddresstitleLocator}    jone-address
#     ...     ELSE IF     '${DEVICE_NAME}'=='an__sony_xp'
#     ...     Input Text      ${formaddresstitleLocator}    xperia-aaddress
#     ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
#     ...     Input Text      ${formaddresstitleLocator}    prime-address
#     ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_seight'
#     ...     Input Text      ${formaddresstitleLocator}    galaxyseight-address
#     ...     ELSE    Log     Device name didnt match
#     Log To Console      \nAddress title added
#     Click Element   ${countrydropdownbuttonLocator}
#     Click Element   ${searchcountrybuttonLocator}
#     Input Text      ${entersearchCountryLocator}    ${countrynamehk}
#     Click Element   ${chooseCountrynameLocator}
#     Log To Console      \nSelected country is HK

#     ${phoneno}     Generate Random String      8   [NUMBERS]
#     Log To Console  \n HK phone number is ${phoneno}
#     Set Global Variable     ${randomphoneno}     ${phoneno}
#     Input Text   ${phonenumberLocator}   ${randomphoneno}
#     Log To Console  \nEntered phone number

#     Input Text  ${firstnameLocator}     ${firstnamehk}
#     Log To Console  \nEntered first name
#     # ${firstName}    Get Text    ${firstnameLocator}
#     # Should Be Equal As Strings  ${firstName}    ${firstnamehk}
#     Input Text  ${lastnameLocator}  ${lastnamehk}
#     Log To Console  \nEntered last name

#     ${ffhs-no}   Generate Random String  3   [NUMBERS]
#     Log To Console  \n Flat/house/building/street number is ${ffhs-no}
#     Set Global Variable     ${ffhsNo}   ${ffhs-no}
#     Input Text  ${flat/housenoLocator}  ${ffhsNo}
#     Log To Console  \nEntered flathouse number
#     Input Text  ${floornoLocator}   ${ffhsNo}
#     Log To Console  \nEntered floor number
#     Input Text  ${house/buildingnoLocator}  ${ffhsNo}
#     Log To Console  \nEntered housebuilding number

#     ${ffhs-name}   Generate Random String  6   [LETTERS]
#     Log To Console  \n Flat/house/building/street is ${ffhs-name}
#     Set Global Variable     ${ffhsNAME}   ${ffhs-name}
#     Input Text   ${house/buildingnameLocator}    ${ffhsNAME}
#     Log To Console  \nEntered housebuilding name

#     Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   230     780     230     200
#     ...     ELSE    Swipe  550  1170    550     200     1000
#     Sleep   2

#     Input Text   ${streetnoLocator}     ${ffhsNo}
#     Log To Console  \nEntered street number
#     Input Text   ${streetnameLocator}   ${streetnamehk}
#     Log To Console  \nEntered street name
#     Click Element   ${DONE_BUTTON}
#     # Wait Until Element Is Visible   ${addresspagetitleLocator}  15
#     # Log To Console  \nHK address saved

Delete address
    Run Keyword If      '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Long Press      ${addresstitleTF}
    # ...     Long Press      xpath=//android.widget.TextView[contains(@text,'${newaddresstitle}')]
    # ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    # ...     Long Press      xpath=//android.widget.TextView[contains(@text,'jone-address')]
    # ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    # ...     Long Press      xpath=//android.widget.TextView[contains(@text,'xperia-address')]
    # ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    # ...     Long Press      xpath=//android.widget.TextView[contains(@text,'prime-address')]
    ...     ELSE    Log     Device name didnt match, hence failed to delete shipping address
    Sleep   2
    Click Element   ${delete}
    Sleep   2
    Run Keyword If      '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Page Should Not Contain Element     ${addresstitleTF}
    # ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'${newaddresstitle}')]
    # ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    # ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'jone-address')]
    # ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    # ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'xperia-address')]
    # ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    # ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'prime-address')]
    ...     ELSE    Log     Device name didnt match, hence failed to verify deleted shipping address
    Log To Console  \nAddress deleted successfully
