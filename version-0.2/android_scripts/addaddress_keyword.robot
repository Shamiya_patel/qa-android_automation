#ADD ADDRESS
*** Variables ***

#Input data
${COUNTRY_NAME_HK}      Hong Kong
${COUNTRY_NAME_UK}      United Kingdom
${UK_CODE}              077
${TOWNCITY_NAME}        Denton
${WARD}                 Greater Manchester
${DISTRICT}             Manchester
${POSTCODE}             M34 6FE


*** Keywords ***
Generate random firstname
    ${FNAME}     Generate Random String      7   [LETTERS]
    Log To Console  \n Recipient first name is ${FNAME}
    Set Global Variable     ${FIRST-NAME}     ${FNAME}

Generate random lastname
    ${LNAME}     Generate Random String      5   [LETTERS]
    Log To Console  \n Recipient last name is ${LNAME}
    Set Global Variable     ${LAST-NAME}     ${LNAME}

Generate random flat/house/building/street number for address
    ${NO}   Generate Random String  3   [NUMBERS]
    Log To Console  \n Flat/house/building/street number is ${NO}
    Set Global Variable     ${FHBS_NUM}   ${NO}

Generate random flat/house/building/street name for address
    ${NM}   Generate Random String  6   [LETTERS]
    Log To Console  \n Flat/house/building/street is ${NM}
    Set Global Variable     ${FHBS_NAME}   ${NM}

Generate random HK phone number
    ${PH_NO}     Generate Random String      8   [NUMBERS]
    Log To Console  \n HK phone number is ${PH_NO}
    Set Global Variable     ${HK_PH-NO}     ${PH_NO}

Generate random UK phone number
    # [Arguments]     ${number}
    ${PH_NU}     Generate Random String      8   [NUMBERS]
    ${NUM}      Catenate    ${UK_CODE}${PH_NU}
    Log To Console  \n HK phone number is ${NUM}
    Set Global Variable     ${UK_PH-NU}     ${NUM}
