*** Settings ***
Documentation       This unit test helps to verify logout.

Library             AppiumLibrary

Resource            common_resource.robot
Resource            create-signin_locator.robot
Resource            signin_variable.robot
Resource            account_locator.robot
Resource            logout_locator.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

# Library             Screenshot       ${SAVE_SCREENSHOT}
# Suite Teardown  Remove Application  ${APP_PACKAGE}

*** Variables ***


*** Test Cases ***

1. Logout

    Wait Until Element Is Visible   ${ACCOUNT_ICON}    20
    Click Element   ${ACCOUNT_ICON}
    Wait Until Element Is Visible   ${ACCOUNT_PAGE_HEADER}  10
    Click Element   ${PROFILE_ICON}
    Wait Until Element Is Visible   ${PROFILE_PAGE_HEADER}  10
    Click Element   ${LOGOUT_BUTTON}
    Wait Until Element Is Visible   ${ZWOOP_LOGO_LOGINPAGE}    15
    Log To Console  \nLogout successful