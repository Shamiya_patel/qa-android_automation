#LANDING PAGE LOCATORS
*** Variables ***
# ${appLogo}              id=action_account
${notificationIcon}     accessibility_id=Notifications
${accountIcon}          accessibility_id=Account

${shopTab}              xpath=//android.widget.TextView[contains(@text,'SHOP')]
${primeshopTab}         xpath=//android.widget.TextView[contains(@text,'Shop')]

# ${shop}     xpath=android.support.v7.app.ActionBar$Tab[contains(@index,'0')]

${watchlistTab}         xpath=//android.widget.TextView[contains(@text,'WATCH LIST')]
${primewatchlistTab}    xpath=//android.widget.TextView[contains(@text,'Watch list')]


${sameproducterror}     xpath=//android.widget.TextView[contains(@text,'Product is already in watchlist')]