*** Variables ***
#Input data
# ${gmail}            katieee.more@gmail.com
# ${gmailpassword}    zwooptest

# ${gmail}            lorenz.janew@gmail.com
# ${gmailpassword}    zwooptest
# qa.automationeOiTo@zwoop.biz, qa.automationwTJeD@zwoop.biz
${email}        qa.automationdypYj@zwoop.biz
${password}     123456

#Notification title
${addressNotSupportedTitle}           Address Not Supported
${totalpriceupdateTitle}              Total Price Update
${purchasecompletedTitle}             Purchase Completed
${purchasefailedTitle}                Purchase Failed
${wrongcardinfoTitle}                 Wrong Card Information
${merchantnotrespondingTitle}         Merchant Not Responding
${merchantoutofstockTitle}            Merchant Out Of Stock
${unknown}                            Zwoop Notification

#Notification body
${addressNSBody}                 Mock body text for notification




*** Keywords ***
Add a new google account to login
    Click Element   ${loginwithGoogle}
    Log To Console  \nClicked google button

    Wait Until Element Is Visible   ${addnewgoogleaccountButton}    10
    Log To Console  \nElement found

    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Click Element      ${joneaddnewgoogleaccountButton}
    ...     ELSE     Click Element      ${addnewgoogleaccountButton}
    Log To Console  \nClicked add new account button

    Wait Until Element Is Visible   ${googlesigninpageTitle}    10
    Log To Console  \nRedirected to google sign in page

    Input Text      ${entergmailemailField}      ${gmail}
    Click Element   ${emailnextButton}
    Log To Console  \nEntered email and choose next button

    Wait Until Element Is Visible   ${entergmailpassword}   10
    Input Text      ${entergmailpassword}       ${gmailpassword}
    Click Element   ${passwordnextButton}
    Log To Console  \nEntered password and choose next button

    ${T&Clink}   Get Text    ${termsandconditionLink}
    Log To Console  \n${T&Clink}

    Run Keyword If      '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Click Element      ${HTCagreegoogleButton}
    ...     ELSE     Click Element      ${NoteagreegoogleButton}
    # ...
    # '${DEVICE_NAME}'=='an_ss_notefour'
    # ...     ELSE    Log     Device name didnt match

    # Click Element   ${agreegoogleButton}
    Log To Console  \nClicked agree button

    Wait Until Element Is Visible   ${confirmgoogleaccountButton}   10
    Click Element   ${confirmgoogleaccountButton}
    Log To Console  \nClicked on allow button

    Wait Until Element Is Visible       ${successloginCheck}      20
    Log To Console  \nSuccessfully registered on Zwoop with google account
#--------------------------------------------------------------------------------------------------#
Go to back-door
    Click Element   ${BackdoorIcon}
    Wait Until Element Is Visible   ${backdoorpageTitle}    10
    ${page}     Get Text    ${backdoorpageTitle}
    Log To Console  \nRedirected to '${page}' page
    Sleep   1
    # Wait Until Element Is Visible   ${allowButton}  10
    # Click Element   ${allowButton}
    # Sleep   1
    Page Should Contain Element     ${infoTab}
    ${element}  Get Text    ${infoTab}
    Log To Console  \n${element} tab visible on the page
    Sleep   1
#PLATFORM_VERSION
    Run Keyword If      '${PLATFORM_VERSION}'=='6.0.1'
    ...     Page Should Contain Element     ${primeactionTab}
    ...     ELSE IF     '${PLATFORM_VERSION}'>'7.0'
    ...     Page Should Contain Element     ${actionTab}
    ...     ELSE IF     '${PLATFORM_VERSION}'=='5.1.1'
    ...     Page Should Contain Element     ${primeactionTab}
    ...     ELSE    Log To Console  \nDevice platform version not ment
    Log To Console  \nActions tab is visible
    Sleep   1
    # ${elementpresent}  Get Text    ${primeactionTab}
    # Log To Console  \nElement '${elementpresent}' found

    Run Keyword If      '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click Element   ${primeactionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Click Element   ${actionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Click Element   ${primeactionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Click Element   ${actionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click Element   ${primeactionTab}
    ...     ELSE    Log To Console  \nDevice isnt listed above
    Log To Console  \nSwitched to actions tab
    Sleep   1

Trigger notification first time
    [Arguments]     ${locator1}     ${locator1}     ${locator2}     ${locator2}
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  750     240     300
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Swipe    720   2470    720     1185
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe      350  1215     350     600
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe      540  1640     540     900
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe      720  2500     720     1100
    ...     ELSE     Log    Device isnt J1

    Wait Until Element Is Visible       ${locator1}   10
    Click Element   ${locator1}
    # Click Element   ${unknownnotificationradioButton}
    Log To Console  \nSelected radion button
    # Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    # ...     Swipe      240  250     240     490
    # ...     ELSE     Log    Device isnt J1
    Sleep   1
    Wait Until Element Is Visible   ${locator2}     10
    Click Element   ${locator2}
    Log To Console  \nNotification triggred
    Background App  seconds=5
    Log To Console  \nPressed device home button

Trigger notification second time
    [Arguments]     ${locator1}     ${locator1}     ${locator2}     ${locator2}
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_prime'
    ...     Click Element   ${primeactionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Click Element   ${actionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Click Element   ${primeactionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Click Element   ${actionTab}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Click Element   ${primeactionTab}
    ...     ELSE    Log To Console  \nDevice isnt listed above
    Log To Console  \nSwitched to actions tab
    Sleep   1
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  750     240     300
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Swipe    720   2470    720     1185
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Swipe      350  1215     350     600
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Swipe      540  1640     540     900
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe      720  2500     720     1300
    ...     ELSE     Log    Device isnt J1
    Sleep   2
    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  750     240     600
    ...     ELSE    Log     Device other then J1

    Wait Until Element Is Visible       ${locator1}   10
    Click Element   ${locator1}
    Wait Until Element Is Visible       ${locator1}   10
    Click Element   ${locator1}
    Log To Console  \nSelected radion button
    Sleep   1

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe      240  250     240     550
    ...     ELSE    Log     Device other then J1

    Wait Until Element Is Visible   ${locator2}     10
    Click Element   ${locator2}
    Log To Console  \nNotification triggred again
    Background App  seconds=5
    Log To Console  \nPressed device home button

Go back to backdoor from notifications
    Click Element   ${appbackButton}
    Wait Until Element Is Visible   ${backdoorpageTitle}    10
    ${page}     Get Text    ${backdoorpageTitle}
    Log To Console  \nRedirected to '${page}' page
#----------------------------------------------XXX-------------------------------------------------------------#

Verify notification
    [Arguments]     ${notificationtitle}    ${value}
    Run Keyword If     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe   720     20  720     1300
    ...     ELSE    Swipe   230     15  230     780
    Log To Console  \nScroll open notification drawer
    # Sleep   2
    Wait Until Element Is Visible   ${notificationIcon}     10
    Log To Console  \nNotification visible
    Sleep   1

    Page Should Contain Element     ${notificationtitle}
    ${titletext}     Get Text    ${notificationtitle}
    Should Be Equal As Strings  ${value}     ${titletext}
    Log To Console  \nNotification title text is '${titletext}'
    Set Global Variable     ${NotificationType}    ${titletext}

    Page Should Contain Element     ${notificationTime}
    ${time}  Get Text    ${notificationTime}
    Log To Console  \nNotification time is '${time}'
    Sleep   1
    Run Keyword If  '${PLATFORM_VERSION}'>='7.0'
    ...     Click Element   ${notificationIcon}
    ...     ELSE    Click Element   ${notificationTime}
    Log To Console  \nClicked notification from drawer
    Sleep   1
    Wait Until Element Is Visible   ${notificationpageTitle}    10
    ${page}     Get Text    ${notificationpageTitle}
    Log To Console  \nRedirected to '${page}' page
    Sleep   1

Notification redirection test
    [Arguments]     ${appnotificationtitle}
    Page Should Contain Element     ${bluedotNotifications}

    Page Should Contain Element     ${appnotificationTimestamp}
    ${appnotiftime}     Get Text    ${appnotificationTimestamp}
    Log To Console  \nNotification timing is '${appnotiftime}'

    Page Should Contain Element     ${appnotificationtitle}
    ${title}    Get Text    ${appnotificationtitle}
    Log To Console  \nNotification blue dot is visible and title is '${title}'
    Should Be Equal As Strings  ${NotificationType}     ${title}
    Sleep   1

    Click Element   ${appnotificationtitle}
    Log To Console  \nClicked app notification

    Wait Until Element Is Visible    ${notificationRedirectionCheck}    10
    ${pageis}   Get Text    ${notificationdetailPage}
    Log To Console  \nRedirected to '${pageis}' page

    Sleep   1
    Click Element   ${appbackButton}
    Wait Until Element Is Visible    ${notificationpageTitle}
    Log To Console  \nRedirected back to notification page

    Page Should Not Contain Element     ${bluedotNotifications}
    Log To Console  \nNotification badge not visible not visible on page

Purchase completed notification redirection test
    Page Should Contain Element     ${bluedotNotifications}
    ${appnotiftime}     Get Text    ${appnotificationTimestamp}
    Log To Console  \nNotification timing is '${appnotiftime}'
    Page Should Contain Element     ${purchasecompletedText}
    ${title}    Get Text    ${purchasecompletedText}
    Log To Console  \nNotification blue dot is visible and title is '${title}'
    Should Be Equal As Strings  ${NotificationType}     ${title}
    Sleep   1
    Click Element   ${purchasecompletedText}
    Log To Console  \nClicked purchase completed app notification

    Wait Until Element Is Visible    ${purchasecompletedCheck}    10
    ${merchantID}   Get Text    ${purchasecompletedCheck}
    Log To Console  \nRedirected to purchase completed success page, '${merchantID}' displayed successfully

    Click Element   ${appbackButton}
    Wait Until Element Is Visible    ${notificationpageTitle}
    Log To Console  \nRedirected back to notification page

    Page Should Not Contain Element     ${bluedotNotifications}
    Log To Console  \nNotification badge nor visible not visible on page

Trigger all notifications simultaneously and verify
    Run Keyword If     '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Swipe   720     20  720     1300
    ...     ELSE    Swipe   230     15  230     780
    Log To Console  \nScroll open notification drawer

    Wait Until Element Is Visible   ${notificationIcon}     10
    Log To Console  \nNotifications are visible

    Page Should Contain Element     ${addresstitleText}
    Page Should Contain Element     ${totalpriceupdateText}
    Page Should Contain Element     ${purchasecompletedText}
    Page Should Contain Element     ${purchsefailedText}
    Page Should Contain Element     ${wrongcardinfoText}
    Page Should Contain Element     ${merchantnotrespondingText}
    Page Should Contain Element     ${merchantoutofstockText}
    Log To Console  \nAll notifications present

    Click Element   ${addresstitleText}
    # Run Keyword If  '${PLATFORM_VERSION}'>='7.0'
    # ...     Click Element   ${notificationIcon}
    # ...     ELSE    Click Element   ${notificationTime}
    Log To Console  \nClicked a notification from drawer

    Page Should Contain Element     ${bluedotNotifications}
    Log To Console  \nNotification blue dot is visible

    Click Element   ${addresstitleText}
    Log To Console  \nRedirected to notification detail page

    Click Element   ${appbackButton}
    Wait Until Element Is Visible    ${notificationpageTitle}
    Log To Console  \nRedirected back to notification page

    Page Should Not Contain Element     ${bluedotNotifications}
    Log To Console  \nNotification badge nor visible not visible on page

Delete notification
    [Arguments]     ${notificationtype}
    Long Press  ${notificationtype}
    Wait Until Element Is Visible    ${deletenotificationButton}
    Page Should Contain Element     ${canceldeletenotificationButton}
    Log To Console  \nBoth buttons visible

    Click Element   ${deletenotificationButton}
    Log To Console  \nClicked delete element

    Page Should Not Contain Element     ${notificationtype}
    Log To Console  \nElement not present on the page, hence delete notification is successful






