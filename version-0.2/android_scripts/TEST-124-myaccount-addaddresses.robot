*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            add-address_locators.robot
Resource            account_locators.robot

Resource            TEST-124-myaccount-addaddresses-resource.robot
Resource            TEST-144-email-registration-resource.robot
Resource            TEST-email-login-resource.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Launch app and login to an existing account
#LOGIN
    # Verify login page content
    # Valid login test    ${correctSigninemail}    ${loginPassword}   ${register/loginButton}
    # Log To Console  \nLogged in successful
#REGISTER
    ${emailName} =    Generate Random String    5   [LETTERS]
    ${correctEmail}     Catenate    ${string1}${string2}${emailName}${zwoopmail}
    Log To Console    \nEmail to be used for registration is ${correctEmail}
    Set Global Variable    ${correctEmail}

    ${name}=   Generate Random String  2   [LETTERS]
    ${firstName}    Catenate    ${firstname}${name}
    Log To Console    \nFirst name to be used for registration is ${firstName}
    Set Global Variable    ${firstName}
    Choose create account link
    Input correct credentials       ${firstName}     ${lastName}     ${correctEmail}     ${registerPassword}   ${register/loginButton}
    Log To Console  \nSuccessfully created a new account and landed on landing Page

2. Go to Account > Addresses
    Go to addresses and verify
    # Choose add icon and verify page content

3. Save empty address form
    Save empty from and expect error

4. Fill all mandatory fields in form and save
    Input credentials for UK address

5. Verify address
    Validate addresss after adding

# 6. Delete address after adding
#     Delete address
# 2. Select countries and verify page content
#     Go to add new address page
#     Select UK and check page content
#     Go to add new address page
#     Select HK and check page content

# 3. Enter correct hong kong address and validate
#     Go to add new address page
#     Enter correct HK address
#     Validate addresss after adding

# # 4. Delete hk address
# #     Delete address

# 5. Enter correct UK address and validte
#     Go to add new address page
#     Enter correct UK address
#     Validate addresss after adding

# 6. Delete uk addess
#     Delete address

