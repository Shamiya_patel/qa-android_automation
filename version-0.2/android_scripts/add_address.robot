*** Settings ***
Documentation       This unit test helps to verify add address from accounts section unit test

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

Library             AppiumLibrary

Resource            common_resource.robot
Resource            common-variable.robot
Resource            landingpage_locators.robot
Resource            addaddress_locators.robot
Resource            account_locator.robot
Resource            addaddress_keyword.robot

*** Variables ***


*** Test Cases ***
1. Go to settings and choose add address
    Wait Until Element Is Visible   ${SETTINGS}     15
    Click Element   ${SETTINGS}
    Click Element   ${ADDRESSES_ICON}

2. Choose '+' to add a new address
    Click Element   ${ADD+ADDRESS}
    # Page Should Contain Element     ${NEW_ADDRESS_PAGE_TITLE}   loglevel=INFO
    # Wait Until Element Is Visible   ${NEW_ADDRESS_PAGE_TITLE}

3. Add address title in form
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Input Text      ${ADDRESS_TITLE}    mi-address
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Input Text      ${ADDRESS_TITLE}    jone-address
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Input Text      ${ADDRESS_TITLE}    xperi-aaddress
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Input Text      ${ADDRESS_TITLE}    prime-address
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy S8'
    ...     Input Text      ${ADDRESS_TITLE}    galaxyseight-address
    ...     ELSE    Log     Device name didnt match
    Log To Console      \nAddress title added

4. Select country
    Click Element   ${COUNTRY_DROPDOWN}
    Click Element   ${SEARCH_COUNTRY_ICON}
    Input Text      ${SEARCH_COUNTRY}    ${COUNTRY_NAME_UK}
    Click Element   ${CHOOSE_COUNTRY_NAME}
    Log To Console      \nCountry selected

5. Add a phone number
    # Generate random HK phone number
    Generate random UK phone number
    Input Text      ${PHONENUMBER}    ${UK_PH-NU}
    Log To Console      \nPhone number added

5. Enter first and last name
    Generate random firstname
    Input Text      ${FIRST_NAME}   ${FIRST-NAME}
    Log To Console      \nFirst name added

    Generate random lastname
    Input Text      ${LAST_NAME}    ${LAST-NAME}
    Log To Console      \nLast name added

6. Add flathouse no. & floor no.
    Generate random flat/house/building/street number for address

    Input Text      ${FLAT/HOUSE_NO}        ${FHBS_NUM}
    Log To Console      \nFlathouse no. added

    Input Text      ${FLOOR_NO}             ${FHBS_NUM}
    Log To Console      \nFloor no. added

    Run Keyword If  '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    # ...     Swipe   230     710     230     205
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200     1000
    Sleep   2

7. Add housebuilding no.
    Input Text      ${HOUSE/BUILDING_NO}    ${FHBS_NUM}
    Log To Console      \nHouse building no. added

8. Add housebuilding name
    Generate random flat/house/building/street name for address
    Input Text      ${HOUSE/BUILDING_NAME}  ${FHBS_NAME}
    Log To Console      \nHousebuilding name added

9. Add street number
    Input Text      ${STREET_NO}    ${FHBS_NUM}
    Log To Console      \nStreet no. added

    # Run Keyword If  '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    # ...     Swipe   230     710     230     205
    # ...     ELSE    Swipe  550  1170    550     200     1000
10. Add street name
    Input Text      ${STREET_NAME}  ${FHBS_NAME}
    Log To Console      \nStreet name added

11. Add towncity
    Input Text      ${TOWN_UK}      ${TOWNCITY_NAME}
    Log To Console      \nTowncity added

12. Add ward
    Input Text      ${WARD_UK}      ${WARD}
    Log To Console      \nWard added

    Run Keyword If  '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Swipe   230     780     230     355
    # Run Keyword If  '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    # ...     Swipe   230     780     230     355
    ...     ELSE    Log     Different device used
    Sleep   1

13. Add district
    Input Text      ${DISTRICT_UK}  ${DISTRICT}
    Log To Console      \nDestrict added

14. Add postcode
    Input Text      ${POST_UK}      ${POSTCODE}
    Log To Console      \nPostcode added

15. Save address form
    Click Element   ${DONE_BUTTON}
    Wait Until Element Is Visible    ${ADDRESSES_TITLE}     15
    Log To Console      \nAddress form saved successfully
    # Log To Console  \nShipping address added successfully


*** Keywords ***

# Generate random firstname
#     ${FNAME}     Generate Random String      7   [LETTERS]
#     Log To Console  \n Recipient first name is ${FNAME}
#     Set Global Variable     ${FIRST-NAME}     ${FNAME}

# Generate random lastname
#     ${LNAME}     Generate Random String      5   [LETTERS]
#     Log To Console  \n Recipient last name is ${LNAME}
#     Set Global Variable     ${LAST-NAME}     ${LNAME}

# Generate random flat/house/building/street number for address
#     ${NO}   Generate Random String  3   [NUMBERS]
#     Log To Console  \n Flat/house/building/street number is ${NO}
#     Set Global Variable     ${FHBS_NUM}   ${NO}

# Generate random flat/house/building/street name for address
#     ${NM}   Generate Random String  6   [LETTERS]
#     Log To Console  \n Flat/house/building/street is ${NM}
#     Set Global Variable     ${FHBS_NAME}   ${NM}

# Generate random HK phone number
#     ${PH_NO}     Generate Random String      8   [NUMBERS]
#     Log To Console  \n HK phone number is ${PH_NO}
#     Set Global Variable     ${HK_PH-NO}     ${PH_NO}

# Generate random UK phone number
#     # [Arguments]     ${number}
#     ${PH_NU}     Generate Random String      8   [NUMBERS]
#     ${NUM}      Catenate    ${UK_CODE}${PH_NU}
#     Log To Console  \n HK phone number is ${NUM}
#     Set Global Variable     ${UK_PH-NU}     ${NUM}







