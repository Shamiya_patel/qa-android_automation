*** Settings ***
Documentation       This unit test helps to verify login with an existing account

Library             AppiumLibrary

Resource            common_resource.robot
Resource            signin_locator.robot
Resource            signin_variable.robot
Resource            create_account.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***
${EMAIL}    qa.automationfrgM@zwoop.biz

*** Test Cases ***
1. Verify landing page
    Wait Until Element Is Visible   ${ZWOOP_LOGO_LOGINPAGE}     20
    Log To Console  \n Lunched and landed on landing page

2. Enter existing email, password and choose login
    # Generate random string for registration and login
    # Generate demo registration email    ${STRING}
    Input Text      ${EMAIL_FEILD}      ${EMAIL}
    Input Text      ${PASSWORD_FEILD}   ${PASSWORD}
    Click Element   ${LOGIN_BUTTON}
    Wait Until Element Is Visible       ${SUCCESS_LOGIN_CHECK}      20

*** Keywords ***
Generate demo registration email
    [Arguments]     ${string}
    ${EMAIL}    Catenate    ${demo_str1}${demo_str2}${string}${demo_str3}
    Log To Console  \n Email generated is ${EMAIL}
    Set Global Variable     ${DEMO_REGISTER_EMAIL}    ${EMAIL}

Generate random string for registration and login
    ${STR}   Generate Random String  4  [LETTERS]
    Log To Console  \n String generated is ${STR}
    Set Global Variable     ${STRING}   ${STR}

