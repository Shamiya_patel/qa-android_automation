*** Variables ***

# Input data
${DEMO-JL}         https://www.johnlewis.com/john-lewis-organic-cotton-t-shirt/p3350062?colour=White
${DEV-TESCO}       https://www.tesco.com/direct/ff-soft-touch-turn-up-tapered-trousers/333-2041.prd?skuId=167-0134
${jl}              https://www.johnlewis.com/
${m&s}             https://www.marksandspencer.com/


*** Keywords ***
Go to browse
    Sign-in
    Click Element   ${SUCCESS_LOGIN_CHECK}
    Log To Console  \nSuccessfully redirected to browse page

Verify browse page
    Page Should Contain Element     ${browsepageTitle}
    Page Should Contain Element     ${moreOptions}
    Page Should Contain Element     ${clearUrl}
    Page Should Contain Element     ${homeBrowser}
    Page Should Contain Element     ${APP_BACK_BUTTON}
    Log To Console  \nChecked browse page content

Choose more option and verify
    Click Element   ${moreOptions}
    Page Should Contain Element     ${addBookmarks}
    Page Should Contain Element     ${listoutbookmark}
    Page Should Contain Element     ${goForward}
    Page Should Contain Element     ${goBackward}
    Page Should Contain Element     ${reloadPage}
    Page Should Contain Element     ${changebrowseMode}
    Log To Console  \nMore option list verified
    Press Keycode   4
    Log To Console  \nMore options closed

Bookmark a page test
    Click Element   ${clearUrl}
    Run Keyword If      '${DEVICE_NAME}'=='note'
    ...     Input Text      ${browseinputUrl}   ${m&s}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE    Log     Device name didnt match
    Click Element   ${browsesearchIcon}
    Log To Console      \nRedirected to merchant page
    Click Element   ${moreOptions}
    Click Element   ${addBookmarks}
    Wait Until Element Is Visible   ${addbookmarkSuccess}    10
    Log To Console  \nBookmarked page successfully

Go to google home
    Click Element   ${homeBrowser}
    Log To Console  \nRedirected back to home browser

Verify bookmark options
    Click Element   ${moreOptions}
    Log To Console  \nMore option list appears
    Sleep   1
    Click Element   ${listoutbookmark}
    Log To Console  \nClicked bookmarks
    Wait Until Element Is Visible   ${bookmarkUrl1}     10
    Long Press  ${bookmarkUrl1}
    Log To Console  \nLong pressed url

    Page Should Contain Element     ${deleteBookmark}
    Page Should Contain Element     ${shareBookmark}
    Page Should Contain Element     ${copyBookmarkurl}
    Log To Console  \nBookmark options available

Delete a bookmark
    Wait Until Element Is Visible   ${bookmarkUrl1}     10
    Long Press  ${bookmarkUrl1}
    Log To Console  \nLong pressed url
    # Click Element   ${listoutbookmark}
    Click Element   ${deleteBookmark}
    Log To Console  \nDeleted first bookmark

Check share bookmark options
    Wait Until Element Is Visible   ${bookmarkUrl1}     10
    Long Press  ${bookmarkUrl1}
    Log To Console  \nLong pressed url
    Click Element   ${shareBookmark}
    Run Keyword If      '${DEVICE_NAME}'=='note'
    ...     Click Element      ${discarddraftMessage}
    ...     ELSE    Press Keycode   4
    Wait Until Element Is Visible   ${moreOptions}     10
    Log To Console  \nRedirected back to bookmarks list from share page

Copy url
    Click Element   ${copyBookmarkurl}
    Wait Until Element Is Visible   ${copyurlSuccess}
    Log To Console  \nCopied url successfully

 Search for products
    Click Element   ${browsepageTitle}
    Click Element   ${clearUrl}
    Run Keyword If      '${DEVICE_NAME}'=='note'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    ...     Input Text      ${browseinputUrl}   ${DEMO-JL}
    ...     ELSE    Log     Device name didnt match
    Click Element   ${browsesearchIcon}
    Log To Console      \nRedirected to product page

Send product to assistant
    Wait Until Element Is Visible   ${SEND_ASSISTANT}     20
    Click Element   ${SEND_ASSISTANT}
    Wait Until Element Is Visible   ${CREATED_CHATROOM}      10
    Log To Console  \nSuccessfully browsed a product and sent to assistant
    Sleep   1

Go back to chat list page
    Click Element   ${APP_BACK_BUTTON}
    Reset Application
