*** Settings ***
Documentation       This test help to add second address and card from profile page for existing user.
...                 Trigger another buy now and choose newly added address and card when buying.
...                 Check successful logout
Library             AppiumLibrary
Resource            android_resource.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application
Library             Screenshot       ${SAVE_SCREENSHOT}
# Suite Teardown  Remove Application  ${APP_PACKAGE}

*** Variables ***
${SHIPPING_ADD}
${PAYMENT_METHOD}

*** Test Cases ***
1. Go to accounts
    Wait Until Element Is Visible   id=action_account   15
    Click Element   id=action_account

2. Choose address
    Wait Until Element Is Visible   id=addresses_wrapper
    Click Element   id=addresses_wrapper

3. Choose '+' to add another address
    Click Element   id=fab_new_address

4. Add another shipping address
    Generate flat/house/building/street number for address
    Generate flat/house/building/street name for address
    Generate random number for addresstitle and cardname
    Generate address phone number
    Generate random recipientname/nickname
    Add address from accounts page
    Sleep   1

5. Go back to profile accounts page and select cards
    Click Element   ${APP_BACK_BUTTON}
    Click Element   id=cards_wrapper

6. Choose '+' to add another card
    Click Element   id=fab_new_card

7. Add another card
    # Generate random name/surname for sign-up
    Generate a random CVV
    Add card from accounts page
    Wait Until Element Is Visible   id=fab_new_card

10. Go back to accounts page
    Click Element   ${APP_BACK_BUTTON}

11. Go back to ideas page
    Click Element   ${APP_BACK_BUTTON}

12. Choose browse
    Click Element   accessibility_id=Browse

# [For 28th and 3rd add base url www.johnlewis.com, change context to web and choose any random product to buy]
13. Choose any random product form the list and send it to assistant
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT5}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT6}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT7}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'${PRODUCT2}')]
    ...     ELSE    Log     No product url found

    Wait Until Element Is Visible   id=send_to_assistant_button     20
    Click Element   id=send_to_assistant_button
    Sleep   1

14. Verify chat bot messages, newly added card and address should get displayed in chat messages
    Capture Page Screenshot     /Users/apple/Documents/qa-android-automation/test_screenshot/buy_now_shipping_card<counter>.png
    # Sleep   1
    Confirm new shipping address in chat bot messages
    Verify chat bot messages for existing user with card and address

15. Change quantity
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT6}')]
    Click Element   xpath=//android.widget.TextView[contains(@text,'${TEXT6}')]
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'2')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'2')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'3')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'5')]
    ...     ELSE    Log     Quantity not available in the list
    Sleep   2

16. Change size
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'${TEXT4}')]
    Click Element   xpath=//android.widget.TextView[contains(@text,'Tops_Size:')]
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Select Tops_Size')]
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'M')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'XL')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Click Element   xpath=//android.widget.RelativeLayout[contains(@index,'2')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Click Element   xpath=//android.widget.TextView[contains(@text,'L')]
    ...     ELSE    Log     Size not available
    Sleep   2

17. Choose buy now from chat room
    Click Element   id=middle_button
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,${BUYNOW_SUCCESS_TEXT}')]
    Log To Console  \nBuy now success message received in chat room

18. Go back to chat page
    Click Element   ${APP_BACK_BUTTON}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Chat')]
    Log To Console  \nRedirected successfully to Chat page

19. Select account icon
    Click Element   id=action_account
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Account')]
    Log To Console  \nSuccessfully landed to account page

20. Choose address and delete one address
    Click Element   id=addresses_icon
    Delete address
    Click Element   ${APP_BACK_BUTTON}

21. Choose cards and delete one card
    Click Element   id=cards_icon
    Delete card
    Click Element   ${APP_BACK_BUTTON}

22. Select profile
    Click Element   id=profile_icon

23. Choose logout
    Click Element   id=log_out_button


*** Keywords ***

Generate random number for addresstitle and cardname
    ${NM}   Generate Random String  3   [NUMBERS]
    Log To Console  \n Number to be used with addresstitle and cardname is ${NM}
    Set Global Variable     ${NO_L}   ${NM}

Add address from accounts page
    Wait Until Element Is Visible   id=address_title
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Input Text      id=address_title    miaddress${NO_L}
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Input Text      id=address_title    j1address${NO_L}
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Input Text      id=address_title    xperiaaddress${NO_L}
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Input Text      id=address_title    primeaddress${NO_L}
    ...     ELSE    Log     Device name didnt match

    Click Element   id=drop_down_arrow_country
    Click Element   id=search_button
    Input Text      id=search_src_text    Hong Kong
    Click Element   id=country_name
    Input Text      id=phone_number     ${PH.NO}
    Input Text      id=recipient_name   ${RECIPIENT-NICK_NAME}
    Input Text      xpath=//android.widget.EditText[contains(@text,'flatHouseNumber')]      ${FHBS_NUM}
    Input Text      xpath=//android.widget.EditText[contains(@text,'floorNumber')]          ${FHBS_NUM}
    Run Keyword If  '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Swipe   230     710     230     205
    ...     ELSE    Swipe  550  1170    550     200     1000

    Wait Until Element Is Visible   xpath=//android.widget.EditText[contains(@text,'houseBuildingNumber')]
    Input Text      xpath=//android.widget.EditText[contains(@text,'houseBuildingNumber')]  ${FHBS_NUM}

    Wait Until Element Is Visible   xpath=//android.widget.EditText[contains(@text,'houseBuildingName')]
    Input Text      xpath=//android.widget.EditText[contains(@text,'houseBuildingName')]    ${FHBS_NAME}

    Input Text      xpath=//android.widget.EditText[contains(@text,'streetNumber')]         ${FHBS_NUM}

    Input Text      xpath=//android.widget.EditText[contains(@text,'streetName')]           ${FHBS_NAME}

    Click Element   ${DONE_BUTTON}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Address saved')]

Confirm new shipping address in chat bot messages
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Page Should Contain Text      Shipping To: miaddress${NO_L}
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Page Should Contain Text      Shipping To: j1address${NO_L}
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Page Should Contain Text      Shipping To: xperiaaddress${NO_L}
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Page Should Contain Text      Shipping To: primeaddress${NO_L}
    ...     ELSE    Log     Device name didnt match, hence failed to match shipping address

Add card from accounts page
    Input Text      id=nickname         ${RECIPIENT-NICK_NAME}
    Input Text      id=name_on_card     card${NO_L}
    Input Text      id=card_number      ${MASTER_CARD}
    Input Text      id=expiry_date      ${MASTER_EXPIRY}
    Input Text      id=cvv              ${CVV}
    Click Element   ${DONE_BUTTON}
    Log To Console      First card added successfully

Delete address
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'miaddress${NO_L}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'j1address${NO_L}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'xperiaaddress${NO_L}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'primeaddress${NO_L}')]
    ...     ELSE    Log     Device name didnt match, hence failed to delete shipping address

    Click Element   accessibility_id=delete
    Sleep   1
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'miaddress${NO_L}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'j1address${NO_L}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'xperiaaddress${NO_L}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'primeaddress${NO_L}')]
    ...     ELSE    Log     Device name didnt match, hence failed to verify deleted shipping address
    Log To Console  \nAddress deleted successfully

Delete card
    Run Keyword If      '${DEVICE_NAME}'=='Mi Phone'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${RECIPIENT-NICK_NAME}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Samsung Galaxy J1 (2016)'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${RECIPIENT-NICK_NAME}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${RECIPIENT-NICK_NAME}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='Galaxy J5 Prime'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${RECIPIENT-NICK_NAME}')]
    ...     ELSE    Log     Device name didnt match, hence failed to delete card
    Click Element   accessibility_id=delete
    Sleep   1
    Log To Console  \nCard deleted successfully
    # Long Press   xpath=//android.widget.TextView[contains(@text,'${RECIPIENT-NICK_NAME}')]

Verify chat bot messages for existing user with card and address
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Based on what I know you, I Have selected these options for you.')]     10

    Page Should Contain Text    ${TEXT1}    loglevel=INFO
    Log To Console   \n The message '${TEXT1}' is present

    Page Should Contain Element    ${IMAGE}            loglevel=INFO
    Log To Console  \n Image with ${IMAGE} id present

    Page Should Contain Text    ${TEXT4}               loglevel=INFO
    Log To Console  \n Text found is '${TEXT4}'

    Page Should Contain Text    ${TEXT5}               loglevel=INFO
    Log To Console  \n Text found is '${TEXT5}'

    Page Should Contain Text    ${TEXT6}               loglevel=INFO
    Log To Console  \n Text found is '${TEXT6}'

    # Page Should Contain Text    Confirm shipping address               loglevel=INFO
    # Log To Console  \n Text found is 'Confirm shipping address'

    Page Should Contain Text    ${RECIPIENT-NICK_NAME}               loglevel=INFO
    Log To Console  \n Text found is '${RECIPIENT-NICK_NAME}'

