*** Settings ***
Library             AppiumLibrary
Resource            common_resource.robot
Resource            create-signin_locator.robot
Resource            add_card_locator.robot
Resource            TEST-118-myaccount-add-card-resource.txt
Resource            account_locators.robot
# Resource            logout_locator.txt
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Go to cards page and verify the content
    ${randomnameonCard}    Generate Random String    3    [LETTERS]

    ${nameOnCard}     Catenate    ${randomnameonCard} ${nameonCard1}${DEVICE_NAME}
    Log To Console    \nName on card is ${nameOnCard}
    Set Global Variable     ${nameOnCard}

    ${cardNickname}   Catenate      ${nicknameonCard} ${randomnameonCard}
    Log To Console    \nCard nickame is ${cardNickname}
    Set Global Variable     ${cardNickname}
    Go to cards page
    Check page content

# Add card - no address [call add address testcase in add card ]
#     Add card without adding address
#     Click Element   ${APP_BACK_BUTTON}
#     Click Element   ${+BUTTON}
#     Wait Until Element Is Visible   ${ADDCARDPAGETITLE}
#     Log To Console  \nRedirected to add card page

#Add address to add card

2. Add card without expiry month
    Add card with empty expiry month    ${cardNickname}    ${nameOnCard}   ${visaCard}     ${validCvv}     ${emptyexpirymonthError}

3. Add card without expiry year
    Add card with empty expiry year     ${cardNickname}    ${nameOnCard}   ${visaCard}     ${validCvv}     ${emptyexpiryyearError}

4. Add card without nickname
    Again go back to cards page
    Add card with one empty field   ${EMPTY}    ${nameOnCard}   ${visaCard}     ${validCvv}     ${emptynicknameError}

5. Add card without name
    Again go back to cards page
    Add card with one empty field   ${cardNickname}    ${EMPTY}   ${visaCard}     ${validCvv}     ${emptynameoncardError}

6. Add card without card number
    Again go back to cards page
    Add card with one empty field   ${cardNickname}    ${nameOnCard}   ${EMPTY}     ${validCvv}     ${emptycardnumberError}

7. Add card without cvv
    Again go back to cards page
    Add card with one empty field   ${cardNickname}    ${nameOnCard}   ${visaCard}     ${EMPTY}     ${emptycvvError}

# #Long / short nick name ----- TO BE DONE

8. Add card - invalid nickname
    Again go back to cards page
    Add card with invalid nickname  ${invalidNickname}

#Skip step 9 & 10 as no checking om client side
# 9. Add card - long name
#     Again go back to cards page
#     Add card with long/short name on card   ${longNameoncard}

# 10. Add card - short name
#     Again go back to cards page
#     Add card with long/short name on card   ${shortNameoncard}

11. Add card - invalid name
    Again go back to cards page
    Add card with invalid name    ${invalidNameoncard}

12. Add card - short cvv
    Again go back to cards page
    Add card with short cvv     ${shortCvv}

13. Add card - long cvv
    Again go back to cards page
    Add card with long cvv     ${longCvv}

14. Add card - invalid number
    Again go back to cards page
    Add invalid card number     ${invalidCard}

15. Add card - expired
    Again go back to cards page
    Add expired card

16. Add card - valid and verify
    Again go back to cards page
    Add card info and validate      ${visaCard}
    Successful card added check

17. Delete added card
    Click Element   ${APP_BACK_BUTTON}
    Delete card




