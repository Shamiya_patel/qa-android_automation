*** Variables ***
#INPUT DATA
#HK
${countrynamehk}     Hong Kong
${streetnamehk}      Jervois Street
#UK
${countrynamuk}      United Kingdom
${ukcode}            077
${ukstreetname}      Carew Terrace
${uktowncityname}    Denton
${ward}              Greater Manchester
${district}          Manchester
${postcode}          M34 6FE
${firstnamehk}       Mario
${lastnamehk}        Bross


#PLACEHOLDERS
${addressplaceholder}               Address Title

${countrytitle}                     Country
${ukcountryplaceholder}             United Kingdom

${flathousenoplaceholder}           Flat / House Number
${floornoplaceholder}               Floor Number

${housebuildingnoplaceholder}       House / Building Number
${housebuildingnameplaceholder}     House / Building Name

${ukpostcodeplaceholder}            +44
${ukphonenumberplaceholder}         Contact number e.g. 07700900515

${firstnameplaceholder}             First name
${lastnameplaceholder}              Last name

${streetnoplaceholder}              Street Number
${streetnameplaceholder}            Street Name

${towncityplaceholder}              Town / City

${wardplaceholder}                  Ward

${districtplaceholder}              District

${postcodeplaceholder}              Post Code, e.g. N1 9LH

${recipientFirstname}               Recipient first name
${recipientLastname}                Recipient last name

#HK
# ${hkcountryplaceholder}             Hong Kong
# ${hkpostcodeplaceholder}            +852
# # ${hkphonenumberplaceholder}
# ${hkareadropdownplaceholder}        Central and Western
# ${hkcityplaceholder}                Hong Kong Island

*** Keywords ***
# Go to add new address page
#     Click Element   ${addnewaddressLocator}
#     Wait Until Element Is Visible   ${addnewaddresspagetitleLocator}   10
#     Log To Console  \nEnter add address page

Go addresses and verify
    Click Element   ${accountsettingsIcon}
    Wait Until Element Is Visible   ${accountpageTitle}  10
    ${page}     Get Text    ${accountpageTitle}
    Log To Console  \nRedirected to '${page}' page

    Click Element   ${addressesIcon}
    Wait Until Element Is Visible   ${addresspageTitle}  10
    ${page}     Get Text    ${addresspageTitle}
    Log To Console  \nRedirected to '${page}' page

    Page Should Contain Element     ${addnewaddressIcon}
    Page Should Contain Element     ${emptyaddresspageimage}
    Log To Console  \nAccount cosnsit no address yet

Choose add icon and verify page content
    Click Element   ${addnewaddressIcon}
    Wait Until Element Is Visible   ${addnewaddresspageTitle}   10
    ${page}     Get Text    ${addnewaddresspageTitle}
    Log To Console  \nRedirected to '${page}' page

    Page Should Contain Element     ${addresstitleTF}
    ${placeholder1}  Get Text    ${addresstitleTF}
    Should Be Equal As Strings  ${placeholder1}     ${addressplaceholder}
    Log To Console  \n${placeholder1}, placeholder text matched

    Page Should Contain Element     ${countryTitle}
    Page Should Contain Element     ${selectedCountryname}
    ${placeholder2}  Get Text    ${selectedCountryname}
    Should Be Equal As Strings  ${placeholder2}     ${ukcountryplaceholder}
    Log To Console  \nCountry selected is ${placeholder2}, placeholder text matched

    Page Should Contain Element     ${flat/houseTF}
    ${placeholder3}  Get Text    ${flat/houseTF}
    Should Be Equal As Strings  ${placeholder3}     ${flathousenoplaceholder}
    Log To Console  \n${placeholder3}, placeholder text matched

    Page Should Contain Element     ${floorTF}
    ${placeholder4}  Get Text    ${floorTF}
    Should Be Equal As Strings  ${placeholder4}     ${floornoplaceholder}
    Log To Console  \n${placeholder4}, placeholder text matched

    Page Should Contain Element     ${house/buildingnoTF}
    ${placeholder5}     Get Text    ${house/buildingnoTF}
    Should Be Equal As Strings  ${placeholder5}    ${housebuildingnoplaceholder}
    Log To Console  \n${placeholder5} - place holder matched

    Page Should Contain Element     ${house/buildingnameTF}
    ${placeholder6}     Get Text   ${house/buildingnameTF}
    Should Be Equal As Strings  ${placeholder6}  ${housebuildingnameplaceholder}
    Log To Console  \n${placeholder6} - place holder matched

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200     1000
    Sleep   2

    Page Should Contain Element     ${streetnumberTF}
    ${placeholder7}     Get Text   ${streetnumberTF}
    Should Be Equal As Strings  ${placeholder7}    ${streetnoplaceholder}
    Log To Console  \n${placeholder7} place holder matched

    Page Should Contain Element     ${streetnameLocator}
    ${placeholder8}     Get Text   ${streetnameLocator}
    Should Be Equal As Strings  ${placeholder8}  ${streetnameplaceholder}
    Log To Console  \n${placeholder8} place holder matched

    Page Should Contain Element     ${town/cityTF}
    ${placeholder9}     Get Text   ${town/cityTF}
    Should Be Equal As Strings  ${placeholder9}  ${towncityplaceholder}
    Log To Console  \n${placeholder9} place holder matched

    Page Should Contain Element     ${wardTF}
    ${placeholder10}     Get Text    ${wardTF}
    Should Be Equal As Strings  ${placeholder10}  ${wardplaceholder}
    Log To Console  \n${placeholder10} place holder matched

    Page Should Contain Element     ${ukdistrictLocator}
    ${placeholder15}     Get Text    ${ukdistrictLocator}
    Should Be Equal As Strings  ${placeholder15}  ${districtplaceholder}
    Log To Console  \n${placeholder15} place holder matched

    Page Should Contain Element     ${ukpostcodeLocator}
    ${placeholder16}     Get Text    ${ukpostcodeLocator}
    Should Be Equal As Strings  ${placeholder16}  ${postcodeplaceholder}
    Log To Console  \n${placeholder16} place holder matched
    Click Element   ${APP_BACK_BUTTON}
    Log To Console      \nPage content verified as per selected country UK

Enter all credentials for UK address
    ${string}   Generate Random String  3   [LETTERS]
    ${addressTitle}     Catenate    ${string}-${DEVICE_NAME}address
    Set Global Variable     ${newaddresstitle}   ${addressTitle}
    Log To Console  \nInput address title as '${newaddresstitle}''

    Run Keyword If      '${DEVICE_NAME}'=='an_ss_notefour'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE IF     '${DEVICE_NAME}'=='an_htc_ueleven'
    ...     Input Text      ${addresstitleTF}    ${newaddresstitle}
    ...     ELSE    Log     Device name didnt match
    Log To Console      \nAddress title added

    Page Should Contain Element     ${selectedCountryname}
    ${placeholder2}  Get Text    ${selectedCountryname}
    Should Be Equal As Strings  ${placeholder2}     ${ukcountryplaceholder}
    Log To Console  \nCountry selected is '${placeholder2}'

    Input Text      ${entersearchCountryLocator}    ${countrynamuk}
    Click Element   ${chooseCountrynameLocator}
    Log To Console      \nSelected country is UK

    ${phno}     Generate Random String      8   [NUMBERS]
    Log To Console  \n UK phone number is ${phno}
    Set Global Variable     ${randomphoneno}     ${phno}

    ${ukphoneNo}    Catenate    ${ukcode}${randomphoneno}
    Input Text   ${phonenumberLocator}   ${ukphoneNo}
    Log To Console  \nEnetered ${ukphoneNo} as phone number

    Input Text  ${firstnameLocator}     ${firstnamehk}
    Log To Console  \nEntered first name

    Input Text  ${lastnameLocator}  ${lastnamehk}
    Log To Console  \nEntered last name

    ${ffhs-num}   Generate Random String  3   [NUMBERS]
    Log To Console  \n Flat/house/building/street number is ${ffhs-num}
    Set Global Variable     ${ffhsNo}   ${ffhs-num}

    Input Text  ${flat/housenoLocator}  ${ffhsNo}
    Log To Console  \nEntered flathouse number

    Input Text  ${floornoLocator}   ${ffhsNo}
    Log To Console  \nEntered floor number

    Input Text  ${house/buildingnoLocator}  ${ffhsNo}
    Log To Console  \nEntered housebuilding number

    ${ffhs-name}   Generate Random String  6   [LETTERS]
    Log To Console  \n Flat/house/building/street is ${ffhs-name}
    Set Global Variable     ${ffhsNAME}   ${ffhs-name}

    Input Text   ${house/buildingnameLocator}    ${ffhsNAME}
    Log To Console  \nEntered housebuilding name

    Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   230     780     230     200
    ...     ELSE    Swipe  550  1170    550     200     1000
    Sleep   2

    Input Text   ${streetnoLocator}     ${ffhsNo}
    Log To Console  \nEntered street number

    Input Text  ${streetnameLocator}    ${ukstreetname}
    Log To Console  \nEntered street name

    Input Text  ${uktownLocator}    ${uktowncityname}
    Log To Console  \nEntered town/city name

    Input Text  ${ukwardLocator}    ${ward}
    Log To Console  \nEntered ward name

    Input Text  ${ukdistrictLocator}    ${district}
    Log To Console  \nEntered district name

    Input Text  ${ukpostcodeLocator}    ${postcode}
    Log To Console  \nEntered postcode name

    Click Element   ${DONE_BUTTON}

Validate addresss after adding
    Wait Until Element Is Visible   ${addresspagetitleLocator}  15
    Log To Console  \nAddress saved

# Check address page content prior selecting country
#     Page Should Contain Element     ${selectedCountryname}
#     ${placeholder2}  Get Text    ${selectedCountryname}
#     Should Be Equal As Strings  ${placeholder2}     ${ukcountryplaceholder}
#     Log To Console  \nPlaceholder text matched, selected country is '${ukcountryplaceholder}'

#     # Element Text Should Be  ${formaddresstitleLocator}   ${addresstitle}
#     # Element Text Should Be  ${countrydropdownbuttonLocator}     ${selectcountrydropdown}
#     Page Should Contain Element     ${flat/houseTF}
#     Page Should Contain Element     ${save/doneButton}
#     Page Should Contain Element     ${appbackButton}
#     Page Should Contain Element     ${floorTF}
#     Click Element   ${appbackButton}
#     Log To Console      \nChecked page content prior selecting country

# Select UK and check page content
#     Click Element   ${countrydropdownbuttonLocator}
#     Click Element   ${searchcountrybuttonLocator}
#     Input Text      ${entersearchCountryLocator}    ${countrynamuk}
#     Click Element   ${chooseCountrynameLocator}
#     Log To Console      \nSelected country is UK

#     Page Should Contain Element     ${selectedcountrynameLocator}
#     ${placeholder3}     Get Text    ${selectedcountrynameLocator}
#     Should Be Equal As Strings  ${placeholder3}      ${ukcountryplaceholder}
#     Log To Console  \n${placeholder3} - country selected placeholder matched

#     Page Should Contain Element     ${selectedpostcodeLocator}
#     ${placeholder4}     Get Text    ${selectedpostcodeLocator}
#     Should Be Equal As Strings  ${placeholder4}     ${ukpostcodeplaceholder}
#     Log To Console  \n${placeholder4} - post code placeholder matched as per selected country

#     Page Should Contain Element     ${phonenumberLocator}
#     ${placeholder17}     Get Text    ${phonenumberLocator}
#     Should Be Equal As Strings  ${placeholder17}    ${ukphonenumberplaceholder}
#     Log To Console  \n${placeholder17} - place holder matched

#     Page Should Contain Element     ${firstnameLocator}
#     ${placeholder5}     Get Text    ${firstnameLocator}
#     Should Be Equal As Strings  ${placeholder5}           ${firstnameplaceholder}
#     Log To Console  \n${placeholder5} - place holder matched

#     Page Should Contain Element     ${lastnameLocator}
#     ${placeholder6}     Get Text    ${lastnameLocator}
#     Should Be Equal As Strings  ${placeholder6}            ${lastnameplaceholder}
#     Log To Console  \n${placeholder6} - place holder matched

#     Page Should Contain Element     ${flat/housenoLocator}
#     ${placeholder7}     Get Text    ${flat/housenoLocator}
#     Should Be Equal As Strings  ${placeholder7}        ${flathousenoplaceholder}
#     Log To Console  \n${placeholder7} - place holder matched

#     Page Should Contain Element     ${floornoLocator}
#     ${placeholder8}     Get Text    ${floornoLocator}
#     Should Be Equal As Strings  ${placeholder8}             ${floornoplaceholder}
#     Log To Console  \n${placeholder8} place holder matched

#     Page Should Contain Element     ${house/buildingnoLocator}
#     ${placeholder9}     Get Text    ${house/buildingnoLocator}
#     Should Be Equal As Strings  ${placeholder9}    ${housebuildingnoplaceholder}
#     Log To Console  \n${placeholder9} - place holder matched

#     Page Should Contain Element     ${house/buildingnameLocator}
#     ${placeholder10}     Get Text   ${house/buildingnameLocator}
#     Should Be Equal As Strings  ${placeholder10}  ${housebuildingnameplaceholder}
#     Log To Console  \n${placeholder10} - place holder matched

#     Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   230     780     230     200
#     ...     ELSE    Swipe  550  1170    550     200     1000
#     Sleep   2

#     Page Should Contain Element     ${streetnoLocator}
#     ${placeholder11}     Get Text   ${streetnoLocator}
#     Should Be Equal As Strings  ${placeholder11}    ${streetnoplaceholder}
#     Log To Console  \n${placeholder11} place holder matched

#     Page Should Contain Element     ${streetnameLocator}
#     ${placeholder12}     Get Text   ${streetnameLocator}
#     Should Be Equal As Strings  ${placeholder12}  ${streetnameplaceholder}
#     Log To Console  \n${placeholder12} place holder matched

#     Page Should Contain Element     ${uktownLocator}
#     ${placeholder13}     Get Text   ${uktownLocator}
#     Should Be Equal As Strings  ${placeholder13}  ${towncityplaceholder}
#     Log To Console  \n${placeholder13} place holder matched

#     Page Should Contain Element     ${ukwardLocator}
#     ${placeholder14}     Get Text    ${ukwardLocator}
#     Should Be Equal As Strings  ${placeholder14}  ${wardplaceholder}
#     Log To Console  \n${placeholder14} place holder matched

#     Page Should Contain Element     ${districtTF}
#     ${placeholder15}     Get Text    ${districtTF}
#     Should Be Equal As Strings  ${placeholder15}  ${districtplaceholder}
#     Log To Console  \n${placeholder15} place holder matched

#     Page Should Contain Element     ${postcodeTF}
#     ${placeholder16}     Get Text    ${postcodeTF}
#     Should Be Equal As Strings  ${placeholder16}  ${postcodeplaceholder}
#     Log To Console  \n${placeholder16} place holder matched
#     Click Element   ${appbackButton}
#     Log To Console      \nPage content verification successful
#----------#
# Select HK and check page content
#     Click Element   ${countrydropdownbuttonLocator}
#     Click Element   ${searchcountrybuttonLocator}
#     Input Text      ${entersearchCountryLocator}    ${countrynamehk}
#     Click Element   ${chooseCountrynameLocator}
#     Log To Console      \nSelected country is HK

#     Page Should Contain Element     ${selectedcountrynameLocator}
#     ${1placeholder}     Get Text    ${selectedcountrynameLocator}
#     Should Be Equal As Strings  ${1placeholder}     ${hkcountryplaceholder}
#     Log To Console  \n${1placeholder} - country selected placeholder matched

#     Page Should Contain Element     ${selectedpostcodeLocator}
#     ${2placeholder}     Get Text    ${selectedpostcodeLocator}
#     Should Be Equal As Strings  ${2placeholder}     ${hkpostcodeplaceholder}
#     Log To Console  \n${2placeholder} - post code placeholder matched as per selected country

#     Page Should Contain Element     ${firstnameLocator}
#     ${3placeholder}     Get Text    ${firstnameLocator}
#     Should Be Equal As Strings  ${3placeholder}     ${firstnameplaceholder}
#     Log To Console  \n${3placeholder} - place holder matched

#     Page Should Contain Element     ${lastnameLocator}
#     ${4placeholder}     Get Text    ${lastnameLocator}
#     Should Be Equal As Strings  ${4placeholder}     ${lastnameplaceholder}
#     Log To Console  \n${4placeholder} - place holder matched

#     Page Should Contain Element     ${flat/housenoLocator}
#     ${5placeholder}     Get Text    ${flat/housenoLocator}
#     Should Be Equal As Strings  ${5placeholder}     ${flathousenoplaceholder}
#     Log To Console  \n${5placeholder} - place holder matched

#     Page Should Contain Element     ${floornoLocator}
#     ${6placeholder}     Get Text    ${floornoLocator}
#     Should Be Equal As Strings  ${6placeholder}     ${floornoplaceholder}
#     Log To Console  \n${6placeholder} - place holder matched

#     Page Should Contain Element     ${house/buildingnoLocator}
#     ${7placeholder}     Get Text    ${house/buildingnoLocator}
#     Should Be Equal As Strings  ${7placeholder}     ${housebuildingnoplaceholder}
#     Log To Console  \n${7placeholder} - place holder matched

#     Page Should Contain Element     ${house/buildingnameLocator}
#     ${8placeholder}     Get Text    ${house/buildingnameLocator}
#     Should Be Equal As Strings  ${8placeholder}     ${housebuildingnameplaceholder}
#     Log To Console  \n${8placeholder} - place holder matched

#     Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   230     780     230     200
#     ...     ELSE    Swipe  550  1170    550     200     1000
#     Sleep   2

#     Page Should Contain Element     ${streetnoLocator}
#     ${9placeholder}     Get Text    ${streetnoLocator}
#     Should Be Equal As Strings  ${9placeholder}     ${streetnoplaceholder}
#     Log To Console  \n${9placeholder} - place holder matched

#     Page Should Contain Element     ${streetnameLocator}
#     ${10placeholder}     Get Text   ${streetnameLocator}
#     Should Be Equal As Strings  ${10placeholder}     ${streetnameplaceholder}
#     Log To Console  \n${10placeholder} - place holder matched

#     Page Should Contain Element     ${hkareadropdownLocator}
#     ${11placeholder}     Get Text   ${hkareadropdownLocator}
#     Log To Console  \n${11placeholder}
#     Should Be Equal As Strings  ${11placeholder}     ${hkareadropdownplaceholder}
#     Log To Console  \n${11placeholder} - place holder matched

#     Page Should Contain Element     ${hkcitydropdownLocator}
#     ${12placeholder}     Get Text   ${hkcitydropdownLocator}
#     Should Be Equal As Strings  ${12placeholder}     ${hkcityplaceholder}
#     Log To Console  \n${12placeholder} - place holder matched
#     Click Element   ${APP_BACK_BUTTON}
#     Log To Console      \nPage content verified as per selected country HK

# Enter correct HK address
#     ${string1}   Generate Random String  3   [LETTERS]
#     ${addressTitle}     Catenate    ${string1}-${DEVICE_NAME}address
#     Set Global Variable     ${newaddresstitle}   ${addressTitle}
#     Log To Console  \nFinal address title is ${newaddresstitle}

#     Run Keyword If      '${DEVICE_NAME}'=='note'
#     ...     Input Text      ${formaddresstitleLocator}    ${newaddresstitle}
#     ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Input Text      ${formaddresstitleLocator}    jone-address
#     ...     ELSE IF     '${DEVICE_NAME}'=='an__sony_xp'
#     ...     Input Text      ${formaddresstitleLocator}    xperia-aaddress
#     ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime'
#     ...     Input Text      ${formaddresstitleLocator}    prime-address
#     ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_seight'
#     ...     Input Text      ${formaddresstitleLocator}    galaxyseight-address
#     ...     ELSE    Log     Device name didnt match
#     Log To Console      \nAddress title added
#     Click Element   ${countrydropdownbuttonLocator}
#     Click Element   ${searchcountrybuttonLocator}
#     Input Text      ${entersearchCountryLocator}    ${countrynamehk}
#     Click Element   ${chooseCountrynameLocator}
#     Log To Console      \nSelected country is HK

#     ${phoneno}     Generate Random String      8   [NUMBERS]
#     Log To Console  \n HK phone number is ${phoneno}
#     Set Global Variable     ${randomphoneno}     ${phoneno}
#     Input Text   ${phonenumberLocator}   ${randomphoneno}
#     Log To Console  \nEntered phone number

#     Input Text  ${firstnameLocator}     ${firstnamehk}
#     Log To Console  \nEntered first name
#     # ${firstName}    Get Text    ${firstnameLocator}
#     # Should Be Equal As Strings  ${firstName}    ${firstnamehk}
#     Input Text  ${lastnameLocator}  ${lastnamehk}
#     Log To Console  \nEntered last name

#     ${ffhs-no}   Generate Random String  3   [NUMBERS]
#     Log To Console  \n Flat/house/building/street number is ${ffhs-no}
#     Set Global Variable     ${ffhsNo}   ${ffhs-no}
#     Input Text  ${flat/housenoLocator}  ${ffhsNo}
#     Log To Console  \nEntered flathouse number
#     Input Text  ${floornoLocator}   ${ffhsNo}
#     Log To Console  \nEntered floor number
#     Input Text  ${house/buildingnoLocator}  ${ffhsNo}
#     Log To Console  \nEntered housebuilding number

#     ${ffhs-name}   Generate Random String  6   [LETTERS]
#     Log To Console  \n Flat/house/building/street is ${ffhs-name}
#     Set Global Variable     ${ffhsNAME}   ${ffhs-name}
#     Input Text   ${house/buildingnameLocator}    ${ffhsNAME}
#     Log To Console  \nEntered housebuilding name

#     Run Keyword If  '${DEVICE_NAME}'=='an_ss_jone'
#     ...     Swipe   230     780     230     200
#     ...     ELSE    Swipe  550  1170    550     200     1000
#     Sleep   2

#     Input Text   ${streetnoLocator}     ${ffhsNo}
#     Log To Console  \nEntered street number
#     Input Text   ${streetnameLocator}   ${streetnamehk}
#     Log To Console  \nEntered street name
#     Click Element   ${DONE_BUTTON}
#     # Wait Until Element Is Visible   ${addresspagetitleLocator}  15
#     # Log To Console  \nHK address saved

Delete address
    Run Keyword If      '${DEVICE_NAME}'=='note'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'${newaddresstitle}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'jone-address')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'xperia-address')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    ...     Long Press      xpath=//android.widget.TextView[contains(@text,'prime-address')]
    ...     ELSE    Log     Device name didnt match, hence failed to delete shipping address
    Sleep   2
    Click Element   accessibility_id=Delete
    Sleep   2
    Run Keyword If      '${DEVICE_NAME}'=='note'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'${newaddresstitle}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_jone'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'jone-address')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'xperia-address')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    ...     Page Should Not Contain Element     xpath=//android.widget.TextView[contains(@text,'prime-address')]
    ...     ELSE    Log     Device name didnt match, hence failed to verify deleted shipping address
    Log To Console  \nAddress deleted successfully
