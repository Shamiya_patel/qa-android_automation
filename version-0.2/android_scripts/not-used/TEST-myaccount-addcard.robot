*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            add_card_locators.robot
Resource            account_locators.robot

Resource            TEST-my-account-addcard-resource.robot
Resource            TEST-144-email-registration-resource.robot
Resource            TEST-email-login-resource.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Launch app and login to an existing account
    Verify login page content
    Valid login test    ${correctSigninemail}    ${password}            ${register/loginButton}
    Log To Console  \nLogged in successful

2. Go to accounts > Cards
    Fill in card form and save
    # Go to cards page and verify
