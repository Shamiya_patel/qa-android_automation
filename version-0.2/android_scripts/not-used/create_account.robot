*** Settings ***
Documentation       This test help to install app and create a new user.

Library             AppiumLibrary
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

Resource            common_resource.robot
Resource            create-signin_locator.robot
Resource            signin_variable.robot
# Library             Screenshot       ${SAVE_SCREENSHOT}
# Suite Teardown  Remove Application  ${APP_PACKAGE}

*** Variables ***


*** Test Cases ***
1. Install app and choose create account link
    Wait Until Element Is Visible   ${ZWOOP_LOGO_LOGINPAGE}     30
    Click Element   ${CREATE_NEW_ACCOUNT}

2. Enter name surname email password and choose create
    Generate random string for registration and login

    Generate demo surname   ${STRING}

    Generate demo registration email    ${STRING}

    Input Text      ${ENTER_NAME}         qa
    Input Text      ${ENTER_SURNAME}      ${DEMO_SURNAME}
    Input Text      ${EMAIL_FEILD}        ${DEMO_REGISTER_EMAIL}
    Input Text      ${PASSWORD_FEILD}     123
    Click Element   ${CREATE/LOGIN_BUTTON}
    Run Keyword If  '${DEVICE_NAME}'=='Xperia Z5 Premium Dual'
    ...     Wait Until Element Is Visible   ${SUCCESS_LOGIN_CHECK}    30
    ...     ELSE
    ...     Wait Until Element Is Visible   ${SUCCESS_LOGIN_CHECK}    30
    # ...     Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Ideas')]    25
    Log To Console  \nSuccessfully created a new account and landed on landing Page



*** Keywords ***
# Generate random number
#     ${NUM}   Generate Random String  4   [NUMBERS]
#     Log To Console  \n Number generated is ${NUM}
#     Set Global Variable     ${NO}   ${NUM}

Generate random string for registration and login
    ${STR}   Generate Random String  4  [LETTERS]
    Log To Console  \n String generated is ${STR}
    Set Global Variable     ${STRING}   ${STR}

Generate demo surname
    [Arguments]     ${string}
    ${SURNAME}     Catenate    ${demo_str2}${string}
    Log To Console      \n Sign-up surname is ${SURNAME}
    Set Global Variable     ${DEMO_SURNAME}    ${SURNAME}

Generate demo registration email
    [Arguments]     ${string}
    ${EMAIL}    Catenate    ${demo_str1}${demo_str2}${string}${demo_str3}
    Log To Console  \n Email to be used for registration is ${EMAIL}
    Set Global Variable     ${DEMO_REGISTER_EMAIL}    ${EMAIL}
