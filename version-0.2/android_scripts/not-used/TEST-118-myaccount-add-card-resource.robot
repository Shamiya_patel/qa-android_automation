*** Variables ***
#INPUT DATA
${shortNickname} 	a
${longNickname} 		My name is very very very longggg
${invalidNickname}		Vis@1
${nicknameonCard} 		My-Card

${shortNameoncard}		a
${longNameoncard}		My name is very very very extremely longggg
${invalidNameoncard}	Kati3
${nameonCard1}			Jazline-

${visaCard} 			4111111111111111
${masterCard}			5105105105105100
${AMERICAN_EX}      	378282246310005
${invalidCard} 			4567890123456789

${validCvv} 			442
${longCvv}				2345
${shortCvv}				98

#PLACEHOLDER
${addcardplaceholder} 		Add card
${nicknameplaceholder} 		Nickname
${nameoncardplaceholder} 	Name on card
${cardnumbarplaceholder}	Card number
${expirydateplaceholder}	Expiry Date
${monthplaceholder}			MM
${yearplaceholder} 			YYYY
${cvvplaceholder}			CVV
${addressplaceholder} 		Address

#MESSAGE
${noaddressError}			xpath=//android.widget.TextView[contains(@text,'Add an address')]
${emptynicknameError} 		xpath=//android.widget.TextView[contains(@text,'Add nickname')]
${emptynameoncardError}		xpath=//android.widget.TextView[contains(@text,'Add name on card')]
${emptycardnumberError}		xpath=//android.widget.TextView[contains(@text,'Add card number')]
${emptyexpirymonthError}	xpath=//android.widget.TextView[contains(@text,'Add expiry month')]
${emptyexpiryyearError}		xpath=//android.widget.TextView[contains(@text,'Add expiry year')]
${emptycvvError} 			xpath=//android.widget.TextView[contains(@text,'Add cvv')]

${shortlongNameError}		xpath=//android.widget.TextView[contains(@text,'Name on card needs to be 2 - 30 letters')]
${wrongnameformatError} 	xpath=//android.widget.TextView[contains(@text,'Name can incl letters')]


${invalidCarderror} 		xpath=//android.widget.TextView[contains(@text,'Enter valid card number')]
${expiredCarderror} 		xpath=//android.widget.TextView[contains(@text,'Card is expired')]
${duplicateCard} 			xpath=//android.widget.TextView[contains(@text,'Card number already used')]

${longCvverror} 			xpath=//android.widget.TextView[contains(@text,'t exceed 3 digits')]
${shortCvverror}			xpath=//android.widget.TextView[contains(@text,'t be less than 3 digits')]


*** Keywords ***
Go to cards page
 	Sign-in
 	Click Element 	${ACCOUNT_ICON}
 	Wait Until Element Is Visible 	${ACCOUNT_PAGE_HEADER}	10
 	Log To Console 	\nRedirected to accounts page
 	Click Element 	${CARDS_ICON}
 	Wait Until Element Is Visible 	${CARDSPAGETITLE}
 	Log To Console 	\nRedirected to cards page

 	Click Element 	${+BUTTON}
 	Wait Until Element Is Visible 	${ADDCARDPAGETITLE}
 	Log To Console 	\nRedirected to add card page

Again go back to cards page
 	Click Element   ${APP_BACK_BUTTON}
 	Click Element   ${+BUTTON}
 	Wait Until Element Is Visible   ${ADDCARDPAGETITLE}
 	Log To Console  \nRedirected to add card page

Check page content
 	Element Text Should Be 	${ADDCARDPAGETITLE} 	${addcardplaceholder}
 	Element Text Should Be 	${TEXTNICKNAME} 		${nicknameplaceholder}
 	Element Text Should Be	${TEXTNAMEONCARD} 		${nameoncardplaceholder}
 	Element Text Should Be	${TEXTCARDNUMBER} 		${cardnumbarplaceholder}

 	Element Text Should Be	${TEXTEXPIRYDATE}		${expirydateplaceholder}
 	Element Text Should Be	${EXPIRYMONTHPLACEHOLDER} 	${monthplaceholder}
 	Element Text Should Be	${EXPIRYYEARPLACEHOLDER} 	${yearplaceholder}

 	Element Text Should Be	${TEXTCVV} 	${cvvplaceholder}
 	Element Text Should Be	${TEXTADDRESS}	${addressplaceholder}

 	Page Should Contain Element 	${TEXTNICKNAME}
 	Page Should Contain Element		${TEXTNAMEONCARD}
 	Page Should Contain Element		${TEXTCARDNUMBER}
 	Page Should Contain Element		${TEXTEXPIRYDATE}
 	Page Should Contain Element		${TEXTCVV}
 	Page Should Contain Element		${EXPIRYMONTHPLACEHOLDER}
 	Page Should Contain Element		${EXPIRYYEARPLACEHOLDER}
 	Page Should Contain Element 	${TEXTADDRESS}
 	Log To Console 	\nPage content verified

Add card with one empty field
	[Arguments]    ${nick}    ${cardName}    ${cardNb}    ${cvv} 	${error}
 	Input Text 	${CARDNICKNAMELOCATOR}		${nick}
 	Input Text 	${NAMEONCARDLOCATOR}		${cardName}
 	Input Text 	${CARDNOLOCATOR} 			${cardNb}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${cvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible 	${error} 	10
 	Sleep 	1

Add card with empty expiry month
 	[Arguments]    ${nick}    ${cardName}    ${cardNb}    ${cvv} 	${error}
 	Input Text 	${CARDNICKNAMELOCATOR}		${nick}
 	Input Text 	${NAMEONCARDLOCATOR}		${cardName}
 	Input Text 	${CARDNOLOCATOR} 			${cardNb}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${cvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible 	${error}
 	Sleep 	1

Add card with empty expiry year
 	[Arguments]    ${nick}    ${cardName}    ${cardNb}    ${cvv} 	${error}
 	Click Element 	${APP_BACK_BUTTON}
 	Click Element 	${+BUTTON}
 	Wait Until Element Is Visible 	${ADDCARDPAGETITLE}
 	Log To Console 	\nRedirected to add card page
 	Input Text 	${CARDNICKNAMELOCATOR}		${nick}
 	Input Text 	${NAMEONCARDLOCATOR}		${cardName}
 	Input Text 	${CARDNOLOCATOR} 			${cardNb}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Input Text 	${CVVLOCATOR} 				${validCvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible 	${error} 	10
 	Sleep 	1

Add invalid card number
 	[Arguments]    ${invalidcard}
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR}		${nameOnCard}
 	Input Text 	${CARDNOLOCATOR} 			${invalidcard}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${validCvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible 	${invalidCarderror} 	10

Add expired card
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR}		${nameOnCard}
 	Input Text 	${CARDNOLOCATOR} 			${visaCard}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'9')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'0')]
 	Input Text 	${CVVLOCATOR} 				${validCvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible 	${expiredCarderror} 	10

#Add card with long/short nickname

Add card with long/short name on card
 	[Arguments]    ${longshortName}
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR}		${longshortName}
 	Input Text 	${CARDNOLOCATOR} 			${visaCard}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${validCvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible	${shortlongNameError} 	10
 	Sleep 	1

Add card with invalid nickname
 	[Arguments]    ${invalidnickname}
 	Input Text 	${CARDNICKNAMELOCATOR}		${invalidnickname}
 	${wrongnameerror} 	Get Text 	${wrongnameformatError}
 	Log To Console 	\nError message is ${wrongnameerror}
 	Should Be Equal As Strings 	Name can incl letters, space and hyphen, only 	${wrongnameerror}
	#Wait Until Element Is Visible	${wrongnameformatError}

Add card with invalid name
 	[Arguments]    ${invalidname}
 	Input Text 	${NAMEONCARDLOCATOR}		${invalidname}
 	${wrongnameerror} 	Get Text 	${wrongnameformatError}
 	Log To Console 	\nError message is ${wrongnameerror}
 	Should Be Equal As Strings 	Name can incl letters, space and hyphen, only 	${wrongnameerror}

Add card with long cvv
 	[Arguments]    ${longcvv}
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR}		${nameOnCard}
 	Input Text 	${CARDNOLOCATOR} 			${visaCard}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${longcvv}
 	${errorlongcvv} 	Get Text 	${longCvverror}
 	Log To Console 	\nError message is ${errorlongcvv}
 	Should Be Equal As Strings	CVV can't exceed 3 digits 	${errorlongcvv}

Add card with short cvv
 	[Arguments]    ${shortcvv}
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR}		${nameOnCard}
 	Input Text 	${CARDNOLOCATOR} 			${visaCard}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${shortcvv}
 	Click Element 	${DONE_BUTTON}
 	${errorshortcvv} 	Get Text 	${shortCvverror}
 	Log To Console 	\nError message is ${errorshortcvv}
 	Should Be Equal As Strings	CVV can't be less than 3 digits 	${errorshortcvv}

Add card without adding address
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR} 		${nameOnCard}
 	Input Text 	${CARDNOLOCATOR} 			${visaCard}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${validCvv}
 	Click Element 	${DONE_BUTTON}
 	Wait Until Element Is Visible 	${noaddressError} 	10
 	Sleep 	1

Add card info and validate
 	[Arguments]    ${card}
 	Input Text 	${CARDNICKNAMELOCATOR}		${cardNickname}
 	Input Text 	${NAMEONCARDLOCATOR}		${nameOnCard}
 	Input Text 	${CARDNOLOCATOR} 			${card}
 	Page Should Contain Element 	${RADIOCREDITLOCATOR}
 	Page Should Contain Element 	${RADIODEBITLOCATOR}
 	Click Element 	${EXPIRYMONTHLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'3')]
 	Click Element 	${EXPITYYEARLOCATOR}
 	Click Element 	xpath=//android.widget.RelativeLayout[contains(@index,'4')]
 	Input Text 	${CVVLOCATOR} 				${validCvv}
 	Click Element 	${DONE_BUTTON}
 	Log To Console 	\nCard added cuccessfully

Successful card added check
 	Sleep 	1
	#Should Be Equal As Strings	CVV can't be less than 3 digits 	${errorshortcvv}
	#Element Text Should Be 		${CARDNAME} 	xpath=//android.widget.TextView[contains(@text,'${nameOnCard}')]
 	Wait Until Element Is Visible 	xpath=//android.widget.RelativeLayout[contains(@index,'0')]

#Check default card (TBD)


Delete card
 	${cardnickName} 	Get Text 	xpath=//android.widget.TextView[contains(@text,'${cardNickname}')]
 	Log To Console 	\nNickname on card is ${cardnickName}
 	Should Be Equal As Strings 	${cardNickname} 	${cardnickName}
 	Log To Console 	\nCard name matched

 	Run Keyword If      '${DEVICE_NAME}'=='note'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${cardnickName}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_j1_1'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${cardnickName}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_sony_xp_1'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${cardnickName}')]
    ...     ELSE IF     '${DEVICE_NAME}'=='an_ss_prime_1'
    ...     Long Press   xpath=//android.widget.TextView[contains(@text,'${cardnickName}')]
    ...     ELSE    Log     Device name didnt match, hence failed to delete card
    Click Element   ${DELETE_CARD}
    Sleep   1
    Log To Console  \nCard deleted successfully
    #Verify card deleted successfully

