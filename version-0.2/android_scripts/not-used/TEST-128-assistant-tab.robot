*** Settings ***
Library             AppiumLibrary
Resource            common_resource.robot
Resource            create-signin_locator.txt
Resource            addaddress_locators.robot
Resource            TEST-124-my-account-addaddresses-resource.robot
Resource            TEST-128-assistant-tab-resource.robot
Resource            TEST-118-myaccount-add-card-resource.txt
Resource            assistant-chat-locators.robot
Resource            browse.txt
Resource            browse_locators.robot
Resource            account_locator.txt
Resource            add_card_locator.robot
Resource            landingpage_locators.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Login and go to browse page
    Go to browse

2. Browse a product and send to assistant
    Search for products
    Send product to assistant

3. Check chat messages
    Verify chat room page content
    Verify chat bot messages

# 4. Add colour, size, shipping address, payment method and quantity
#     Edit colour
#     Edit size
#     Add shipping address from chatroom
#     Add payment method from chatroom
#     Edit quantity
