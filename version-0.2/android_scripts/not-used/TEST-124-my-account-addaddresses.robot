*** Settings ***
Library             AppiumLibrary
Resource            android-common-resource.robot

Resource            create-signin_locator.robot
Resource            add-address_locators.robot
Resource            account_locator.robot

Resource            TEST-124-my-account-addaddresses-resource.robot
Resource            TEST-144-email-registration-resource.robot
Resource            TEST-email-login-resource.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Launch app and login to an existing account
    Verify login page content
    Valid login test    ${correctSigninemail}    ${password}            ${register/loginButton}
    Log To Console  \nLogged in successful

2. Go to Account > Addresses
    Go addresses and verify
    Choose add icon and verify page content

# 2. Select countries and verify page content
#     Go to add new address page
#     Select UK and check page content
#     Go to add new address page
#     Select HK and check page content

# 3. Enter correct hong kong address and validate
#     Go to add new address page
#     Enter correct HK address
#     Validate addresss after adding

# # 4. Delete hk address
# #     Delete address

# 5. Enter correct UK address and validte
#     Go to add new address page
#     Enter correct UK address
#     Validate addresss after adding

# 6. Delete uk addess
#     Delete address

