#Assistant page locators

*** Variables ***
${chatroomTitle}        xpath=//android.widget.TextView[contains(@index,'1')]
${zwoopavatarImage}     id=avatar_image
${chatroomcreateTime}   id=created_time
${itemImage}            id=product_image

#Messages
${message1}     xpath=//android.widget.TextView[contains(@text,'Based on what I know you')]
${message2}     xpath=//android.widget.TextView[contains(@text,'t have any payment information, please add one.')]
${message4}     xpath=//android.widget.TextView[contains(@text,'Color:')]
${message5}     xpath=//android.widget.TextView[contains(@text,'_Size: ???')]
# ${message}     xpath=//android.widget.TextView[contains(@text,'Bottom_Size: ???')]
# ${message5}     xpath=//android.widget.LinearLayout[contains(@index,'0')]
${message6}     xpath=//android.widget.TextView[contains(@text,'Price:')]
${message7}     xpath=//android.widget.TextView[contains(@text,'Quantity: 1')]
${message8}     xpath=//android.widget.TextView[contains(@text,'Shipping To:')]
${message9}     xpath=//android.widget.TextView[contains(@text,'Payment Method:')]
${message10}    xpath=//android.widget.TextView[contains(@text,'I can’t proceed without payment information')]

${selectColour}         xpath=//android.widget.TextView[contains(@text,'Select Color')]
${chooseColourSize}     xpath=//android.widget.RelativeLayout[contains(@index,'0')]
${chooseQuantity}       xpath=//android.widget.RelativeLayout[contains(@index,'2')]

${chatroomproductName}  xpath=//android.widget.TextView[contains(@index,'1')]
${todayTimestamp}       xpath=//android.widget.TextView[contains(@text,'Today')]