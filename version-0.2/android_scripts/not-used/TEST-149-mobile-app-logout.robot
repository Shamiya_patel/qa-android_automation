*** Settings ***
Documentation       This test help to verify successful logout for a pre-logged user
Library             AppiumLibrary
Resource            android_resource.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application
Library             Screenshot       ${SAVE_SCREENSHOT}

*** Variables ***

*** Test Cases ***
1. Go to Settings
    Click Element   id=action_account

2. Go to profile icon
    Click Element   id=profile_icon

3. Choose logout
    Click Element   id=log_out_button