*** Variable***

#Chat messages list
${TEXT1}        Based on what I know you, I Have selected these options for you.
${TEXT2}        I don't have any payment information, please add one.
${colour}       Color: ???
${topSize}      Top_Size: ???
${bottomSize}   Bottoms_Size: ???
${TEXT5}        Price:
${quantity}     Quantity: 1
${TEXT7}        Shipping To:
${TEXT8}        Payment Method:
${TEXT9}        I can’t proceed without payment information

*** Keywords ***
# Sign-in and go to chats page
#     Sign-in
#     Click Element   ${CHAT_TAB}

Verify chat room page content
    Page Should Contain Element     ${chatroomproductName}
    ${productName}  Get Text    ${chatroomproductName}
    Log To Console  \nProduct name is ${productName}
    Page Should Contain Element     ${todayTimestamp}

Verify chat bot messages
    Run Keyword If      '${DEVICE_NAME}'=='an_ss_jone'
    ...     Swipe   330     210     330     310
    ...     ELSE    Log     Device isnt J1
    Wait Until Element Is Visible   ${message1}     15

    ${cb1}     Get Text    ${message1}
    Should Be Equal As Strings  ${TEXT1}    ${cb1}
    Log To Console   \n '${cb1}' - received in chat room

    ${cb2}     Get Text    ${message2}
    Should Be Equal As Strings  ${TEXT2}   ${cb2}
    Log To Console  \n '${cb2}' - received in chat room

    #Below path need to be saved
    Capture Page Screenshot     /Users/apple/Documents/qa-android-automation/test_screenshot/product_image1.png
    Page Should Contain Element    ${itemImage}            loglevel=INFO
    Log To Console  \n '${itemImage}' - received in chat room

    ${cb4}     Get Text    ${message4}
    # Should Be Equal As Strings  ${colour}  ${cb4}
    Log To Console  \n '${cb4}' - received in chat room

    ${cb5}     Get Text    ${message5}
    # Should Be Equal As Strings  ${bottomSize}  ${cb5}
    Log To Console  \n '${cb5}' - received in chat room

    # ${cb}     Get Text    xpath=//android.widget.TextView[contains(@text,'${BOTTOM}')]
    # Log To Console  \n '${cb}' - received in chat room

    ${cb6}     Get Text    ${message6}
    # Should Be Equal As Strings  ${TEXT5}  ${cb6}
    Log To Console  \n '${cb6}' - received in chat room

    ${cb7}     Get Text    ${message7}
    Should Be Equal As Strings  ${quantity}   ${cb7}
    Log To Console  \n '${cb7}' - received in chat room

    ${cb8}     Get Text    ${message8}
    # Should Be Equal As Strings  ${TEXT7}    ${cb8}
    Log To Console  \n '${cb8}' - received in chat room

    ${cb9}     Get Text    ${message9}
    # Should Be Equal As Strings  ${TEXT8}     ${cb9}
    Log To Console  \n '${cb9}' - received in chat room

    ${cb10}     Get Text    ${message10}
    Should Be Equal As Strings  ${TEXT9}     ${cb10}
    Log To Console  \n '${cb10}' - received in chat room

# Edit colour
#     Click Element   ${message4}
#     Wait Until Element Is Visible   ${selectColour}     15
#     Sleep   5
#     Click Element   ${chooseColourSize}
#     Log To Console      \n${colour} selected successfully
#     Sleep   5

# Edit size
#     Wait Until Element Is Visible   ${message5}   15
#     Click Element   ${message5}
#     Sleep   5
#     Click Element   ${chooseColourSize}
#     Log To Console  \nSuccessfully selected ${bottomSize} size
#     Sleep   5

# Edit quantity
#     Wait Until Element Is Visible   ${message7}   15
#     Click Element   ${message7}
#     Sleep   5
#     Click Element   ${chooseQuantity}
#     Log To Console  \nSuccessfully updated ${quantity}
#     Sleep   5

# Add shipping address from chatroom
#     Wait Until Element Is Visible   ${message8}     15
#     Click Element   ${message8}
#     # Wait Until Element Is Visible       id=title_address_selection      10
#     # Click Element   id=add_address_button
#     Wait Until Element Is Visible       ${formaddresstitleLocator}    15
#     Enter correct UK address and validate
#     # Enter correct HK address and validate

# Add payment method from chatroom
#     Wait Until Element Is Visible   ${TEXT8}     10
#     Click Element   ${TEXT8}
#     Add card info and validate




