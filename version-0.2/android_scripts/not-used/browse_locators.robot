#BROWSE LOCATORS
*** Variables ***
${browsepageTitle}        xpath=//android.widget.TextView[contains(@text,'Browse')]

${clearUrl}               id=clear_imageview
${browseinputUrl}         id=browser_search
${browsesearchIcon}       id=browser_search_button

${homeBrowser}            id=browser_home_button

${moreOptions}            accessibility_id=More options

${addBookmarks}           xpath=//android.widget.TextView[contains(@text,'Add bookmark')]
${addbookmarkSuccess}     xpath=//android.widget.TextView[contains(@text,'Bookmarked')]

${listoutbookmark}        xpath=//android.widget.TextView[contains(@text,'Bookmarks')]
# ${}              xpath=//android.widget.TextView[contains(@index,'1')]
${deleteBookmark}         xpath=//android.widget.TextView[contains(@text,'Delete')]

${shareBookmark}          xpath=//android.widget.TextView[contains(@text,'Share')]
${sharebookmarkOptions}   id=title_default

${copyBookmarkurl}        xpath=//android.widget.TextView[contains(@text,'Copy url')]
${copyurlSuccess}         xpath=//android.widget.TextView[contains(@text,'Url copied to clipboard')]

${bookmarkUrl1}           xpath=//android.widget.TextView[contains(@text,'https://www.google.co.uk/')]
# //android.widget.TextView[contains(@text,'https://www.google.co.uk/')]

${goForward}              xpath=//android.widget.TextView[contains(@text,'Go forward')]

${goBackward}             xpath=//android.widget.TextView[contains(@text,'Go backward')]

${reloadPage}             xpath=//android.widget.TextView[contains(@text,'Reload')]

${changebrowseMode}       xpath=//android.widget.TextView[contains(@text,'Set browse mode')]

${discarddraftMessage}    id=button1

${GOOGLE_SEARCH_FIELD}    id=lst-ib
${GOOGLE_SEARCH_ICON}     id=Google Search

${SEND_ASSISTANT}         id=menu_wrapper
${CREATED_CHATROOM}       id=avatar_image