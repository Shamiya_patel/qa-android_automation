*** Settings ***
Documentation       This test help to install app and create a new user.

Library             AppiumLibrary
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

Resource            common_resource.robot
Resource            common-variable.robot
Resource            create-signin_locator.robot
Resource            signin_variable.robot
Resource            signin.robot

*** Variables ***

*** Test Cases ***
1. Sigin in the app
