*** Settings ***
Documentation     This test suite helps to test different login cases.

Library         AppiumLibrary
Resource        android_resource.robot
Suite Setup     Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown  Remove Application  ${APP_PACKAGE}

*** Variables ***
# ${SAVE_SCREENSHOT}      /Users/apple/Desktop/Study_scripts/Android/automation_screenshots
# ${LOG_FILE}             /Users/apple/Desktop/Study_scripts/Android/automation_logs

*** Test Cases ***
    # Log       ${LOG_FILE}
    # Log     "Step 1: Launch Zwoop application"
    # Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
#Add error messages for below cases surrent version error messages doesnt match with requirement doc[PENDING]
1. Login with empty credentials
    Click Element   ${EMAIL_FIELD}
    Click Element   ${PASSWORD_FIELD}
    Click Element   ${ACCESS}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Not a valid email address')]
    ${EMPTY_EMAILPASWWORD_ERROR}    Get Text    xpath=//android.widget.TextView[contains(@text,'Not a valid email address')]
    Set Global Variable     ${EMPTYFIELDS_ERROR}    ${EMPTY_EMAILPASWWORD_ERROR}
    Log To Console     \n Missing mandatory fields error: ${EMPTYFIELDS_ERROR}
    Reset Application

2. Login with valid email only
    Input Text  ${EMAIL_FIELD}      ${VALID_EMAIL_LOGIN}
    Click Element   ${ACCESS}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Not a valid password. Needs to be 3 characters long.')]
    ${EMPTY_PASWWORD_ERROR}    Get Text    xpath=//android.widget.TextView[contains(@text,'Not a valid password. Needs to be 3 characters long.')]
    Set Global Variable     ${EMPTYPASSWORD_ERROR}    ${EMPTY_PASWWORD_ERROR}
    Log To Console     \n Password missing error: ${EMPTYPASSWORD_ERROR}
    Reset Application

3. Login with valid password only
    Generate random valid password
    Input Text  ${PASSWORD_FIELD}   ${VALID_EMAIL_PASSWORD}
    Click Element   ${ACCESS}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Not a valid email address')]
    ${EMPTY_EMAIL_ERROR}    Get Text    xpath=//android.widget.TextView[contains(@text,'Not a valid email address')]
    Set Global Variable     ${EMPTYEMAIL_ERROR}    ${EMPTY_EMAIL_ERROR}
    Log To Console     \n Email missing error: ${EMPTYEMAIL_ERROR}
    Reset Application
    # Add error for empty credential sign-in [PENDING]

4. Login using incorrect email format/ incorrect email and correct password
    Generate random valid password
    Login Test      ${EMAIL_FIELD}     ${INVALID_EMAIL}      ${PASSWORD_FIELD}      ${VALID_EMAIL_PASSWORD}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Not a valid email address')]
    ${INCORRECT_EMAIL_ERROR}    Get Text    xpath=//android.widget.TextView[contains(@text,'Not a valid email address')]
    Set Global Variable     ${INCORRECTEMAIL_ERROR}    ${INCORRECT_EMAIL_ERROR}
    Log To Console     \n Email format incorrect error: ${INCORRECTEMAIL_ERROR}
    Reset Application

5. Login with correct email format and incorrect password/ incorrect password format
    Generate random invalid password
    Login Test      ${EMAIL_FIELD}     ${VALID_EMAIL_LOGIN}  ${PASSWORD_FIELD}      ${INVALID_PASSWORD}
#For version 0.2 client doesnt check server response and allow existing user to login with any password[TO DO]
    Reset Application
    #Add error for invalid inputs [PENDING]
# [SKIP BELOW 2 CASES]
# 6. Login with valid email and incorrect password(mismatch case)
#     Login Test      ${EMAIL_FIELD}     ${VALID_EMAIL_LOGIN}  ${PASSWORD_FIELD}      ${INCORRECT_PASSWORD}
# #For version 0.2 client doesnt check server response and allow existing user to login with any password[TO DO]
#     Reset Application

# 7. Login with incorrect email and valid password(mismatch case)
#     Generate random valid password
#     Login Test      ${EMAIL_FIELD}     ${INCORRECT_EMAIL}    ${PASSWORD_FIELD}      ${VALID_EMAIL_PASSWORD}
#     Reset Application
#     #Add errors for incorrect inputs [PENDING]

8. Login with valid email and password
    Login Test      ${EMAIL_FIELD}     ${VALID_EMAIL_LOGIN}  ${PASSWORD_FIELD}      ${VALID_EMAIL_PASSWORD}
    Wait Until Element Is Visible   xpath=//android.widget.TextView[contains(@text,'Ideas')]    60
    ${LANDING_PAGE}     Get Text    xpath=//android.widget.TextView[contains(@text,'Ideas')]
    Set Global Variable     ${LANDED_ON_IDEAS_PAGE}     ${LANDING_PAGE}
    Log To Console      \n Login successful. Landed on ${LANDED_ON_IDEAS_PAGE} page.
    #Add display landing page for successful sign-in case PENDING

# Sign-in with non-registered email
#     Generate random valid password
#     Login Test      ${EMAIL_FIELD}      ${NON-REGISTERED_EMAIL}      ${PASSWORD_FIELD}       ${VALID_PASSWORD}
#     #Add error for sign-in with non-registered email [PENDING]

# For non-registered email, check if name and surname fields appear to began with registration
#     Input Text      ${EMAIL_FIELD}        ${VALID_EMAIL}
#     Generate random valid password
#     Input Text      ${PASSWORD_FIELD}     ${VALID_PASSWORD}
#     Click Element   text_input_password_toggle
#     Click Element   ${ACCESS}
#     Check element presence    ${NAME_FEILD}
#     Check element presence    ${SURNAME_FEILD}
    #Add verify landing page for successful registration

*** Keywords ***






