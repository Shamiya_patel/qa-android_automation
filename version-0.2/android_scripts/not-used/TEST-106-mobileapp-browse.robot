*** Settings ***
Library             AppiumLibrary
Resource            common_resource.robot
Resource            create-signin_locator.robot
Resource            browse_locators.robot
Resource            TEST-106-mobileapp-browse-resource.robot
Resource            account_locators.robot
# Resource            logout_locator.txt
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***

*** Test Cases ***
1. Launch app and go to browse page
    Go to browse
    Verify browse page

2. Validate more options on browse page
    Choose more option and verify

3. Bookmark a page
    Bookmark a page test

4. Go back to home browser
    Go to google home

5. Validate bookmark page content
    Verify bookmark options

6. Validate copy, share and delete bookmarks
    Copy url
    Check share bookmark options
    Delete a bookmark

7. Browse for another product and send it to assistant
    Search for products
    Send product to assistant