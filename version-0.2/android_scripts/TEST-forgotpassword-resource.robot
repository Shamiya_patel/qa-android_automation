*** Variables ***
#Input data
${invalidemail}                  qa.automationZwoop.biz
${nonregisteredemail}            qaautomationtest1234@zwoop.biz
${unconfirmedEmail}              qa.zwoopautomation+HDkwE@zwoop.biz
${forgotpasswordvalidEmail}      qa.zwoopautomation+etSQb@zwoop.biz
# qa.zwoopautomation+ggfik@zwoop.biz


*** Keywords ***
Choose forgot password link and verify
    Click Element  ${forgotPasswordbutton}
    Wait Until Element Is Visible   ${zwoopLogo}    10
    Log To Console  \nRedirected to next page

    Page Should Contain Element     ${backtologinpageButton}
    Page Should Contain Element     ${intromessageText}
    Page Should Contain Element     ${enteremailField}
    Page Should Contain Element     ${resetpasswordbutton}
    Log To Console  \nVerified page content

Empty email test
    [Arguments]     ${expectmessage}
    Click Element   ${resetpasswordbutton}
    Wait Until Element Is Visible   ${expectmessage}    10
    ${Warningmessage}    Get Text   ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'

Enter email and send
    [Arguments]     ${email}    ${expectmessage}
    Input Text      ${enteremailField}  ${email}
    Click Element   ${resetpasswordbutton}
    Wait Until Element Is Visible   ${expectmessage}    10
    ${Warningmessage}    Get Text   ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'
    Click Element   ${backtologinpageButton}
    Click Element   ${forgotPasswordbutton}

Enter valid email
    Input Text      ${enteremailField}  ${forgotpasswordvalidEmail}
    Click Element   ${resetpasswordbutton}
    Wait Until Element Is Visible   ${successMessage}  15
    ${message}  Get Text    ${successMessage}
    Log To Console  \n'${message}' message received
    Wait Until Element Is Visible   ${forgotPasswordbutton}     15
    Click Element   ${forgotPasswordbutton}

Scroll page to see email notification
    Swipe   230     15  230     780
    Log To Console  \nOpen notification pannel
    Sleep   2
    Wait Until Element Is Visible   ${notificationIcon}     10
    Log To Console  \nNotification visible
    Sleep   1