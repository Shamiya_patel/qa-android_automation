*** Variables ***
#Test Data
${newEmail}                 freshemailtest@gamil.com
${unverifiedEmail}          qa.zwoopautomation+unbjU@zwoop.biz
# qa.zwoopautomation+DbAZE@zwoop.biz
${verifiedEmail}            qa.zwoopautomation+DbAZE@zwoop.biz


*** Keywords ***
Select forgot password and verify page content
    Click Element  ${forgotpasswordLink}
    Log To Console  \nRedirected to reset password page
    Wait Until Element Is Visible   ${forgotpasswordPage}      10
    Page Should Contain Element     ${forgotpasswordBackButton}
    Page Should Contain Element     ${forgetpasswordemailField}
    Page Should Contain Element     ${forgotpasswordsendemailButton}
    Log To Console  \nVerified reeset password page content

Empty email, forget password test
    [Arguments]     ${expectmessage}
    Click Element   ${forgetpasswordemailField}
    Click Element   ${forgotpasswordsendemailButton}
    Wait Until Element Is Visible   ${expectmessage}    10
    ${Warningmessage}    Get Text   ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'

Enter email
    [Arguments]     ${email}    ${expectmessage}
    Input Text      ${forgetpasswordemailField}  ${email}
    Click Element   ${forgotpasswordsendemailButton}
    Wait Until Element Is Visible   ${expectmessage}    10
    ${Warningmessage}    Get Text   ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'
    Click Element   ${forgotpasswordBackButton}
    Click Element   ${forgotpasswordLink}

Valid forgot my password test
    [Arguments]     ${email}
    Input Text      ${forgetpasswordemailField}  ${email}
    Click Element   ${forgotpasswordsendemailButton}
    Sleep   5
    Click Element   ${okButton}