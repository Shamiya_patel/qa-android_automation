#Account page locators

*** Variables ***
#ACCOUNT
${accountTab}               accessibility_id=masterFooter_account_btn
${Pagetitle}                accessibility_id=masterHeader_title_lbl

${orders}                   accessibility_id=account-orders
# name=account_table_cell_title_lbl
# accessibility_id=Orders
${addresses}                accessibility_id=account_address_row
# account_table_cell_title_lbl
# account-addresses
${cards}                    accessibility_id=account_card_row
${profile}                  accessibility_id=account_profile_row
${profileNameValue}         name=account_table_cell_content_lb

${faq}                      accessibility_id=account-faq
${about}                    accessibility_id=account-about
${logoutButton}             accessibility_id=account_signout_btn

${cancleLogout}             accessibility_id=Cancel
${acceptLogout}             accessibility_id=Okay
# ${backtoaccountpageButton}  accessibility_id=Account

#PROFILE
${profilepage}              accessibility_id=my_profile_tableView
${profileName}              accessibility_id=my_profile_lastname_tf
${profilenameError}         accessibility_id=my_profile_firstname_error_lbl
${profilelastName}          accessibility_id=my_profile_lastname_tf
${profilelastnameError}     accessibility_id=my_profile_lastname_error_lbl
${genderSelector}           accessibility_id=my_profile_gender_btn
${birthdaySelector}         accessibility_id=my_profile_bday_btn
${profileemailField}        accessibility_id=my_profile_email_tf
${contactcountryButton}     accessibility_id=my_profile_country_btn
${profilecontactField}      accessibility_id=my_profile_telephone_tf
${profilecontactError}      accessibility_id=my_profile_telephone_error_lbl
${changepassword}           accessibility_id=my_profile_change_password_btn

