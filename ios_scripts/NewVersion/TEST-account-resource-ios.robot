*** Variables ***
#Input data





*** Keywords ***
Go to accounts tab
    Click Element   ${accountTab}
    Wait Until Element Is Visible   ${Pagetitle}    10
    ${redirectedTo}     Get Text    ${Pagetitle}
    Log To Console  \nRedirected to '${redirectedTo}' page successfully
    Page Should Contain Element     ${orders}
    Page Should Contain Element     ${addresses}
    Page Should Contain Element     ${cards}
    Page Should Contain Element     ${profile}
    Page Should Contain Element     ${faq}
    Page Should Contain Element     ${about}
    Log To Console  \nVerified accounts page
    # ${name}   Get Text  ${profilenameValue}
    # Log To Console  \nProfile name is '${name}'

Go to profile
    Click Element   ${profile}
    Wait Until Element Is Visible   ${Pagetitle}     10
    ${page}     Get Text    ${Pagetitle}
    Log To Console  \nRedirected to '${page}' page

    Click Element   ${headerleftButton}
    Wait Until Element Is Visible   ${Pagetitle}     10
    ${goTo}     Get Text    ${Pagetitle}
    Log To Console  \nRedirected back to '${goTo}' page

Go to cards
    Click Element   ${cards}
    Wait Until Element Is Visible   ${Pagetitle}     10
    ${nextpage}     Get Text    ${Pagetitle}
    Log To Console  \nRedirected to '${nextpage}' page

    Click Element   ${headerleftButton}

    Wait Until Element Is Visible   ${Pagetitle}     10
    ${navigate}     Get Text    ${Pagetitle}
    Log To Console  \nRedirected back to '${navigate}' page

Logout
    # Scroll Down     ${faq}
    # Scroll Down     ${about}
    # Scroll Down     ${logoutButton}
    # Log     Scrolled page until logout button
    Wait Until Element Is Visible   ${logoutButton}     10
    Log To Console  \nScrolled successfully, logout button is visible
    Click Element   ${logoutButton}
    Click Element   ${acceptLogout}
    Wait Until Element Is Visible   ${loginbutton}  10
    Log To Console  \nLogout Successful



