*** Variables ***
#Test Data
${emailresetPassword}       qa.zwoopautomation+ZVhBr@zwoop.com
${newpasswordIs}            123456789

*** Keywords ***
Go to profile page
    Click Element   ${profile}
    ${page}     Get Text    ${profile}
    Log To Console  \nRedirected to '${page}' page

Go to change password
    Click Element   ${changepassword}
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${page}     Get Text    ${successLogincheck}
    Log To Console     \nRedirected to '${page}' page

Enter password negative test
    [Arguments]     ${pass1}    ${pass2}
    Input Text   ${oldpasswordField}    ${pass1}
    Input Text   ${newpasswordField}    ${pass2}
    Click Element   ${universalrightButton}

Enter old and new password
    Input Text   ${oldpasswordField}    ${password}
    Input Text   ${newpasswordField}     ${newpasswordIs}
    Log To Console  \nEntered password

Save and confirm
    Click Element   ${universalrightButton}
    Sleep   5
    Click Element   ${okButton}
    Wait Until Element Is Visible   ${changepassword}   10
    Log To Console  \nChanged password

#PENDING CASES FOR INVALID, INCORRECT, SHORT AND EMPTY PASSWORD TBD