*** Variables ***
#Input data





*** Keywords ***
Go to notification tab
    Click Element   ${notificationTab}
    Log To Console  \nClicked notification tab

    Page Should Contain Element     ${notificationPage}
    Page Should Contain Element     ${emptynotificationScreen}
    Log To Console  \nBoth validation elements present. Notification page is empty

    Wait Until Element Is Visible   ${successLogincheck}    10
    ${page}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${page}' page
    Sleep   5

# Choose notification
#     [Arguments]     ${typeofnotification}
#     Click Element  ${typeofnotification}
#     Wait Until Element Is Visible   ${successLogincheck}    10
#     ${nextpage}     Get Text    ${successLogincheck}
#     Log To Console  \nRedirected to '${nextpage}' page

#     Click Element   ${backtonotificationlist}
#     Wait Until Element Is Visible   ${successLogincheck}    10
#     ${goto}     Get Text    ${successLogincheck}
#     Log To Console  \nRedirected to '${goto}' page

Verify notification banner
    Wait Until Element Is Visible   ${genericnotificationTitle}
    ${type}     Get Text    ${genericnotificationTitle}
    Log to Console  \nNotification banner '${type}' is visible

    Click Element   ${genericnotificationTitle}
    Log To Console  \nClicked '${type}' notification

    Page Should Contain Element     ${notificationPage}
    Wait Until Element Is Visible   ${successLogincheck}     10
    ${page}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${page}' page
    Sleep   2

Choose notifiation and verify
    [Arguments]         ${typeofnotif}      ${logo}
    Page Should Contain Element     ${notificationblueIcon}
    Log To Console  \nNotification blue dot present

    Page Should Contain Element     ${notificationTimestamp}
    ${timestamp}    Get Text    ${notificationTimestamp}
    Log To Console  \nNotification time is '${timestamp}'
    Sleep   1
    Page Should Contain Element     ${typeofnotif}
    ${type}     Get Text    ${typeofnotif}
    Log To Console  \nNotification type is '${typeofnotif}'
    Sleep   1
    Click Element   ${notificationTitle}
    Log To Console  \nClicked notification title
    Sleep   1
    Wait Until Page Contains Element   ${logo}    10
    Log To Console  \nNotification specific image visible on page

    ${message}  Get Text    ${failbannerMessage}
    Log To Console  \nAddress failed banner message is '${message}'

    Click Element   ${headerleftButton}
    Log To Console  \nRedirected back to notifications page
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${redirect}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${redirect}' page
    Sleep   5

Purchase completed
    Page Should Contain Element     ${notificationblueIcon}
    Log To Console  \nNotification blue dot present

    Page Should Contain Element     ${notificationTimestamp}
    ${timestamp}    Get Text    ${notificationTimestamp}
    Log To Console  \nNotification time is '${timestamp}'

    Page Should Contain Element     ${notificationTitle}
    ${type}     Get Text    ${notificationTitle}
    Log To Console  \nNotification type is '${notificationTitle}'
    Sleep   1
    Click Element   ${purchasecompletedimage}
    Log To Console  \nClicked notification image

    Wait Until Page Contains Element    ${purchaseCompletedlogo}    10
    Log To Console  \nRedirected to purchase complete page, merchant order no is visible

    Click Element   ${headerleftButton}
    Log To Console  \nRedirected back to notifications page
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${redirect}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${redirect}' page
    Sleep   5

Delete notification
    Swipe   325     114     220     114
    Log To Console  \nSwipe from right to left

    Wait Until Page Contains Element    ${deletenotification}   10
    Log To Console  \nDelete element is visible

    Click Element   ${deletenotification}
    Log To Console  \nDeleted notification

    Page Should Not Contain Element     ${notificationTitle}
    Log To Console  \nNo notifications present on page
    Sleep   2
    Page Should Contain Element     ${emptynotificationScreen}
    Log To Console  \nEmpty notification image present on page




