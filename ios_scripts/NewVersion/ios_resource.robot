*** Settings ***
Documentation     A resource file with reusable keywords and variables to test iOS app.
Library     AppiumLibrary
# Library     Screenshot
Library     BuiltIn
Library     String
# Library     DateTime
# Library     OperatingSystem
Resource    create-login-ios-locators.robot
Resource    account-ios-locators.robot
Resource    TEST-account-resource-ios.robot



*** Variables ***
#-----------------------------APPIUM CONFIGURATION-----------------------------
${APP}                  /mobile_automation/zwoop_automation/iOS_automation/iOS_builds/ThePro2-0.1.0copy.app
${APPIUM_SERVER}        http://localhost:${PORT}/wd/hub
${PLATFORM_NAME}        iOS

#-----------------------------DEVICE LIST-----------------------------
#iPhone 6 [Appium doesnt support 11.3 yet]
# ${UDID}                 02210bbfcf008bee48ea3d6fae25cdcc6ac1026e
# ${PLATFORM_VERSION}     11.3
# ${DEVICE_NAME}          QAiphone-six

# #iPhone 7plus
# ${UDID}                 6a6edbcff0b06e25028ff607f861a8acde935f05
# ${PLATFORM_VERSION}     10.2.1
# ${DEVICE_NAME}          QAiPhone-sevenplus

# iPhone 7plus [NEW - works for automation]
# ${UDID}                 f31cb799e4bca6e18dc10c785eae743556506922
# ${PLATFORM_VERSION}     11.2.2
# ${DEVICE_NAME}          QAiPhone-sevenplus-second

# iPhone 8 [Works for automation]
${UDID}                 ce6369d9e302300c0ae0d7e8b3aa9432312d9b00
${PLATFORM_VERSION}     11.0.2
${DEVICE_NAME}          QAiPhone-eight

#iPhone 5C
# ${UDID}                 453713a60a782ac0f05af06ec0b5af90fd6ba4ca
# ${PLATFORM_VERSION}     10.3.2
# ${DEVICE_NAME}          QAiphone-fiveC

${BUNDLE_ID}            biz.zwoop.ios
${AUTOMATION_NAME}      Appium
# XCUITest
${ORG_ID}               QZZC6KS99Q

#-----------------------------UNIVERSAL BUTTONS-------------------------------
#PAGE HEADER
${successLogincheck}          accessibility_id=masterHeader_title_lbl
${headerleftButton}           accessibility_id=masterHeader_back_btn
${universalrightButton}       accessibility_id=masterHeader_right_btn

#########################
#SUB-PAGE HEADER
# ${pageHeadersecondlayer}      accessibility_id=masterHeader_title_lbl
# ${sublayerLeftbutton}         accessibility_id=masterHeader_back_btn
# ${sublayerRightButton}        accessibility_id=masterHeader_right_btn

#BACK & RIGHT keys
######################


#-----------------------------POP-UP BUTTONS-----------------------------
${okButton}                   accessibility_id=Okay
${cancel}                     accessibility_id=Cancel
# ${universalappbackButton}     accessibility_id=universal_back_button

${appBackbutton}                accessibility_id=Back
${doneButton}                   accessibility_id=Done

#-----------------------------CREDENTIALS  qa.zwoopautomation+DbAZE@zwoop.biz, qa.zwoopautomation+ZVhBr@zwoop.biz......IjXSQ
${correctSigninemail}           qa.zwoopautomation+fdOyu@zwoop.biz
${password}                     123457

#-----------------------------GMAIL CREDENTIALS-----------------------------
${correctGmail}         zwoopautomation@gmail.com


${TYPE OF FILE}              .png
${IOS_DEVICELOGS}           /usr/local/lib/node_modules/deviceconsole/deviceconsole
${XCODE_CONFIG_FILE}        /mobile_automation/wdaxcode.xcconfig

*** Keywords ***
Launch Application
    [Arguments]         ${APPIUM_SERVER}   ${PLATFORM_VERSION}   ${DEVICE_NAME}
    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}   platformVersion=${PLATFORM_VERSION}
    ...                 deviceName=${DEVICE_NAME}   app=${APP}  udid=${UDID}    bundleId=${BUNDLE_ID}   autoDismissAlerts=True
    ...                 automationName=${AUTOMATION_NAME}   fullReset=False  clearSystemFiles=True   xcodeOrgId=${ORG_ID}
    ...                 xcodeSigningId=iPhone Developer     usePrebuiltWDA=False     showXcodeLog=True   iosInstallPause=1000
    ...                 realDeviceLogger=${IOS_DEVICELOGS}      xcodeConfigFile=${XCODE_CONFIG_FILE}    shouldUseSingletonTestManager=False
    Log To Console  \nApp launched, build used is located at '${APP}'
# usePrebuiltWDA=True. useNewWDA autoGrantPermissions=True
Successful login
    Log To Console  \nLogin email is '${correctSigninemail}'
    Input Value          ${loginEmailfield}       ${correctSigninemail}
    Input Value          ${loginPasswordfield}    ${password}
    Click Element        ${loginbutton}
    Wait Until Element Is Visible   ${successLogincheck}      20
    ${Landingpagename}  Get Text    ${successLogincheck}
    Log To Console  \nSign-in successful, landed on '${Landingpagename}'  page.

Account logout
    Go to accounts tab
    Logout
    # Click Element   ${accountTab}
    # Wait Until Element Is Visible   ${accountPagetitle}    10
    # ${redirectedTo}     Get Text    ${accountPagetitle}
    # Log To Console  \nRedirected to '${redirectedTo}' page successfully
    # Scroll Down     ${addresses}
    # Scroll Down     ${profile}
    # Scroll Down     ${feedback}
    # Scroll Down     ${faq}
    # Scroll Down     ${about}
    # Scroll Down     ${preferences}
    # Scroll Down     ${logoutButton}
    # Log     Scrolled page until logout button
    # Wait Until Element Is Visible   ${logoutButton}     10
    # Log To Console  \nScrolled successfully, logout button is visible
    # Click Element   ${logoutButton}
    # Click Element   ${acceptLogout}
    # Wait Until Element Is Visible   ${loginbutton}  10
    # Log To Console  \nLogout Successful






