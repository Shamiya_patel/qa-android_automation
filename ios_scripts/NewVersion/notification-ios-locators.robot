#NOTIFICATION PAGE LOCATORS

*** Variables ***
${notificationTab}          accessibility_id=masterFooter_notification_btn
${notificationPage}         accessibility_id=notification_page
${emptynotificationScreen}  accessibility_id=default-main-notification
${failbannerMessage}        accessibility_id=order_banner_message_lbl
${deletenotification}       accessibility_id=row icon delete

#ROW ELEMENTS - TYPES OF NOTIFICATION [notification page list-cannot be controlled as its a dynamic list]
# ${bestpriceupdated}         accessibility_id=notification_tableViewCell_0_0
# ${wrongcardinformation}     accessibility_id=notification_tableViewCell_0_1
# ${addressnotsupported}      accessibility_id=notification_tableViewCell_0_2
# ${totalpriceupdate}         accessibility_id=notification_tableViewCell_0_3
# ${merchantoutofstock}       accessibility_id=notification_tableViewCell_0_4
# ${purchasecompleted}        accessibility_id=notification_tableViewCell_0_5
# ${purchasefailed}           accessibility_id=notification_tableViewCell_0_6

#NOTIFICATION DETAILS
${notificationTitle}        accessibility_id=k_AID_notification_tableViewCell_title_lbl
${notificationBody}         accessibility_id=k_AID_notification_tableViewCell_desc_lbl
${notificationTimestamp}    accessibility_id=k_AID_notification_tableViewCell_timestamp_lbl
${notificationblueIcon}     accessibility_id=k_AID_notification_tableViewCell_blue_dot_view

#NOTIFICATION BANNER
${genericnotificationTitle}    accessibility_id=dropDownPanel_title_lbl
${notifImage}                  accessibility_id=dropDownPanel_imgView
# ${}

#NOTIFICATION PAGE VALIDATIONS By TITLE <-----------------------OPTIONAL
# ${addressnotsupportedText}  accessibility_id=Address Not Supported
# ${totalpriceupdate}         accessibility_id=Total Price Update
# ${purchasecompleted}        accessibility_id=Purchase Completed
# ${purchasefailed}           accessibility_id=Purchase Failed
# ${wrongcardinformation}     accessibility_id=Wrong Card Information
# ${merchantnotresponding}    accessibility_id=Merchant Not Responding
# ${merchantoutofstock}       accessibility_id=Merchant Out Of Stock
# ${bestpriceupdated}         accessibility_id=

#UNIQUE IDENTIFICATION FOR NOTIFICATIONS [as per Notification Image]
${addressnotsupportedimage}      accessibility_id=address-not-supported
${totalPriceupdateimage}         accessibility_id=total-price-updated
${purchasecompleteimage}         accessibility_id=purchase-complete
${purchasefailedimage}           accessibility_id=purchase-failed
${wrongcardinformationimage}     accessibility_id=card-rejected
${merchantnotrespondingimage}    accessibility_id=amazon-is-not-responding
${merchantoutofstockimage}       accessibility_id=out-of-stock

#REDIRECTION VALIDATION FORM NOTIFICATIONS
${addressFailedlogo}            accessibility_id=order-fail-invalid-address

${priceupdateFaillogo}          accessibility_id=order-fail-price-updated

${purchaseCompletelogo}        accessibility_id=order_merchant_info_merchant_orderId_lbl

${purchaseFailedlogo}           accessibility_id=order-fail-missing-info

${wrongcardInfologo}            accessibility_id=order-fail-card-rejected

${merchantNotrespondinglogo}    accessibility_id=order-fail-missing-info

${merhcantOutofstocklogo}       accessibility_id=order-fail-out-of-stock
