*** Variables ***

${continueFacebooklogin}    accessibility_id=Continue
${facebookUsername}         accessibility_id=Shamiya Zw

*** Keywords ***
Login with facebook
#Check for user with facebook.app and without for adding required cases
    Click Element   ${landingpagefacebookButton}
    Sleep   5
    # Wait Until Element Is Visible   ${continueFacebooklogin}   10
    #eight
    # Click Element At Coordinates    30  220
    Click A Point   x=30  y=340
    Log To Console  \nClicked
    # Click A Point   x=30  y=222
    # ${context}   Get Current Context
    # Log To Console  \nCurrent contaxt is '${context}'

    # Click Element   ${continueFacebooklogin}
    # Wait Until Element Is Visible   ${successLogincheck}      20
    # ${Landingpagename}  Get Text    ${successLogincheck}
    # Log To Console  \nLogin with facebook successful, landed on '${Landingpagename}' page.

Verify facebook login
    Go to accounts tab
    Page Should Contain Element     ${facebookUsername}
    Log To Console  \nFacebook name appear under accounts

#Check how to fix the session time out
Verify session timeout
    Background App  milliseconds=2700
