*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            account-ios-locators.robot
Resource            account-changepassword-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot
Resource            TEST-forgot-my-password-resource-ios.robot
Resource            TEST-account-resource-ios.robot
Resource            TEST-account-changepassword-resource-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Login to existing account and go to accounts
    Accept notification pop-up alert and verify login page[ios-11]
    Valid login test      ${emailresetPassword}    ${password}      ${loginbutton}
    Go to accounts tab

2. Redirect to change password page from profile page
    Go to profile page
    Go to change password

3. Update pasword and confirm
    Enter old and new password
    Save and confirm