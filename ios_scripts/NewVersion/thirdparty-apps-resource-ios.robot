#3rd party apps
*** Variables ***
${appium1}=  Open Application    http://localhost:4755/wd/hub    platformName=iOS   platformVersion=${PLATFORM_VERSION}
    ...     deviceName=${DEVICE_NAME}  udid=${UDID}    bundleId=com.apple.mobilemail   autoAcceptAlerts=True
    # ...     automationName=${AUTOMATION_NAME}   fullReset=True  clearSystemFiles=True   xcodeOrgId=${ORG_ID}
    # ...     xcodeSigningId=iPhone Developer     usePrebuiltWDA=False     showXcodeLog=True   iosInstallPause=1000
    # ...     realDeviceLogger=${IOS_DEVICELOGS}      xcodeConfigFile=${XCODE_CONFIG_FILE}    shouldUseSingletonTestManager=False

${inbox}                name=Inbox
${mailsubject}          name=subjectLabel
${mailsummarylabel}     name=summaryLabel

*** Keywords ***
Launch mail app
    Switch Application  ${appium1}
    Page Should Contain Element     ${inbox}
    Page Should Contain Element     ${mailsubject}
    Log To Console  \nRedirected to 'inbox' and email reset password email present

Choose first email
    Click Element   ${mailsubject}
    Sleep   2