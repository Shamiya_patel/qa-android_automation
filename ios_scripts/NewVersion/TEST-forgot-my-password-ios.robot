*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            forgot-password-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot
Resource            TEST-forgot-my-password-resource-ios.robot
Resource            thirdparty-apps-resource-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Choose forget password and verify
    Accept notification pop-up alert and verify login page[ios-11]
#     Run Keyword If  '${DEVICE_NAME}'=='QAiPhone-eight'
#     ...     Accept notification pop-up alert and verify login page[ios-11]
#     ...     ELSE    Verify login page without pop-up alert[ios-10]
#     Log To Console  \Verified
    Select forgot password and verify page content

2. Send email with empty email for forget password
   Empty email, forget password test    ${forgotpasswordError}

2. Send email with an invalid email
    Enter email     ${incorrectemail}   ${forgotpasswordError}

3. Send email with a non-registered email
    Enter email     ${newEmail}         ${forgotpasswordError}

4. Send email to a registered non-verified email
    Enter email     ${unverifiedEmail}         ${forgotpasswordError}

5. Send email with a valid registered email
   Valid forgot my password test    ${verifiedEmail}

a. Switch app now
    Launch mail app
    Choose first email

6. Re-send email to same email address
    Click Element  ${forgotpasswordLink}
    Enter email     ${verifiedEmail}         ${forgotpasswordError}

    # Add redirection to mail, add password and confirm once new email account is created.