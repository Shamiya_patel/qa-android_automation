*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            account-ios-locators.robot
Resource            account-address-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot
Resource            TEST-account-resource-ios.robot
Resource            TEST-account-address-resource-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Login app and go to accounts > address
    # Accept notification pop-up alert and verify login page[ios-11]
    # Successful login
    Click Element   ${alertallowButton}
    Sleep   1

2. Go to addresses and verify
    Choose addresses and verify

3. Go to add address and save empty form
    Choose add and verify
