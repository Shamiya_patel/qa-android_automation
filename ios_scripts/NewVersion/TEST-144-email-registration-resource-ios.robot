*** Variables ***
#Input data
${firstname}        Maddy ios autom
${lastName}         Kalvin Kevin Vinchi
${longName}         mynameisveryyyyyyyyylong
${longSurname}      mysurnameisveryyyyyylong
${wrongemail1}      jazleen@com
${wrongemail2}      richard23.com
${string1}          qa.zwoopautomation
${string2}          +
${zwoopmail}        @zwoop.biz
# ${realemailpassword}    qaautomation23Jan2018
${shortpassword}    1234
${password}         123456


*** Keywords ***
Accept notification pop-up and verify landing page
    Click Element   ${alertallowButton}
    Sleep  1
    Page Should Contain Element     ${createloginappLogo}
    Page Should Contain Element     ${landingpagefacebookButton}
    Page Should Contain Element     ${loginEmailfield}
    Page Should Contain Element     ${inputpasswordPlaceholder}
    Page Should Contain Element     ${forgotpasswordLink}
    Page Should Contain Element     ${loginbutton}
    Page Should Contain Element     ${registerLink}
    Log To Console   \nVerified landing page(sign-in) content

Choose register link
    Click Element   ${registerLink}

# Go back to login page
#     Click Element   ${universalappbackButton}
#     Log To Console  \nRedirect back to login page

Verify create new account page
    Page Should Contain Element     ${createloginappLogo}
    Page Should Contain Element     ${inputnamePlaceholder}
    Page Should Contain Element     ${landingpagegoogleButton}
    Page Should Contain Element     ${inputnamePlaceholder}
    Page Should Contain Element     ${inputSurname}
    Page Should Contain Element     ${signupEmailfield}
    Page Should Contain Element     ${registerButton}
    Log To Console   \nVerified create account page content

Clear all fields
    Clear Text      ${inputName}
    Clear Text      ${inputSurname}
    Clear Text      ${signupEmailfield}
    Clear Text      ${signupPasswordfield}
    Scroll Up       ${inputSurname}

Empty credentials
    [Arguments]         ${name}     ${surname}     ${email}     ${password}   ${locator}   ${expectmessage1}   ${expectmessage2}   ${expectmessage3}   ${expectmessage4}
    Input Text          ${inputName}             ${name}
    Input Text          ${inputSurname}          ${surname}
    Input Text          ${signupEmailfield}      ${email}
    Input Text          ${signupPasswordfield}   ${password}
    Click Element       ${locator}
    Wait Until Element Is Visible   ${expectmessage1}    10
    ${Warningmessage}    Get Text    ${expectmessage1}
    Log To Console  \nExpected error message is '${Warningmessage}'

    Wait Until Element Is Visible   ${expectmessage2}    10
    ${Warningmessage}    Get Text    ${expectmessage2}
    Log To Console  \nExpected error message is '${Warningmessage}'

    Wait Until Element Is Visible   ${expectmessage3}    10
    ${Warningmessage}    Get Text    ${expectmessage3}
    Log To Console  \nExpected error message is '${Warningmessage}'

    Wait Until Element Is Visible   ${expectmessage4}    10
    ${Warningmessage}    Get Text    ${expectmessage4}
    Log To Console  \nExpected error message is '${Warningmessage}'

Input credentials
    Clear all fields
    [Arguments]         ${name}     ${surname}     ${email}      ${password}   ${locator}   ${expectmessage}
    Input Text          ${inputName}             ${name}
    Input Text          ${inputSurname}          ${surname}
    Input Text          ${signupEmailfield}      ${email}
    Input Text          ${signupPasswordfield}   ${password}
    Click Element       ${locator}
    Wait Until Element Is Visible   ${expectmessage}    10
    ${Warningmessage}    Get Text    ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'

Input correct credentials
    Clear all fields
    [Arguments]         ${name}     ${surname}     ${email}      ${password}   ${locator}
    Input Text          ${inputName}             ${name}
    Input Text          ${inputSurname}          ${surname}
    Input Text          ${signupEmailfield}      ${email}
    Input Text          ${signupPasswordfield}   ${password}
    Click Element       ${locator}

    Wait Until Element Is Visible   ${successLogincheck}      30
    # Get Element Location    ${successLogincheck}

    ${Landingpagename}  Get Text    ${successLogincheck}
    Log To Console  \nLanded on '${Landingpagename}'  page, registration successful



