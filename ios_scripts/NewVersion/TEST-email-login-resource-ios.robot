*** Variables ***
#Input data
${incorrectemail}           qaautomationoAUAr@zwoopbiz
${correctSigninemail}       qa.zwoopautomation+RmkVk@zwoop.biz
${password}                 123457
${mismatchPassword}         123456

#Sign-in error messages text
# ${emptyEmailsigninerror}        accessibility_id=Email a mandatory field.
# ${emptyPasswordsigninerror}     accessibility_id=Password a mandatory field.
# ${invalidEmailsigninerror}      accessibility_id=Invalid email address
# ${credentialmisMatcherror}      accessibility_id=Login fail, either email or password is incorrect!



*** Keywords ***
Verify login page without pop-up alert[ios-10]
    Page Should Contain Element     ${createloginappLogo}
    Page Should Contain Element     ${landingpagefacebookButton}
    Page Should Contain Element     ${landingpagegoogleButton}
    Page Should Contain Element     ${loginEmailfield}
    Page Should Contain Element     ${loginPasswordfield}
    Page Should Contain Element     ${forgotpasswordLink}
    Page Should Contain Element     ${loginbutton}
    Page Should Contain Element     ${registerLink}
    Log To Console   \nVerified landing page content prior login

Accept notification pop-up alert and verify login page[ios-11]
    Wait Until Element Is Visible   ${alertallowButton}     10
    Click Element   ${alertallowButton}
    Log To Console  \nClicked on notification alert
    Sleep   2
    Page Should Contain Element     ${createloginappLogo}
    Page Should Contain Element     ${landingpagefacebookButton}
    Page Should Contain Element     ${landingpagegoogleButton}
    Page Should Contain Element     ${loginEmailfield}
    Page Should Contain Element     ${loginPasswordfield}
    Page Should Contain Element     ${forgotpasswordLink}
    Page Should Contain Element     ${loginbutton}
    Page Should Contain Element     ${registerLink}
    Log To Console   \nVerified landing page content prior login

Clear login credentials
    Clear Text  ${loginEmailfield}
    Clear Text  ${loginPasswordfield}

Empty credentials
    [Arguments]         ${email}      ${password}   ${locator}   ${expectmessage1}  ${expectmessage2}
    Input Text          ${loginEmailfield}      ${email}
    Input Text          ${loginPasswordfield}   ${password}
    Click Element       ${loginbutton}
    Wait Until Element Is Visible   ${expectmessage1}    10
    ${Warningmessage}    Get Text    ${expectmessage1}
    Log To Console  \nExpected error message is '${Warningmessage}'

    Wait Until Element Is Visible   ${expectmessage2}    10
    ${Warningmessage}    Get Text    ${expectmessage2}
    Log To Console  \nExpected error message is '${Warningmessage}'

Invalid login test
    [Arguments]         ${email}      ${password}   ${locator}   ${expectmessage}
    Input Text          ${loginEmailfield}      ${email}
    Input Text          ${loginPasswordfield}   ${password}
    Click Element       ${loginbutton}
    Wait Until Element Is Visible   ${expectmessage}    10
    ${Warningmessage}    Get Text    ${expectmessage}
    Log To Console  \nExpected error message is '${Warningmessage}'

Valid login test
    [Arguments]         ${email}      ${password}   ${locator}
    Input Text          ${loginEmailfield}      ${email}
    Input Text          ${loginPasswordfield}   ${password}
    Click Element       ${loginbutton}
    Log To Console  \nEmail used to Login is '${email}'
    Wait Until Element Is Visible   ${successLogincheck}      20
    ${Landingpagename}  Get Text    ${successLogincheck}
    Log To Console  \nSign-in successful, landed on '${Landingpagename}' page.


