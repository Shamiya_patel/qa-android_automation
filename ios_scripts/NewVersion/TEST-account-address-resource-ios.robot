*** Variables ***
#Input data





*** Keywords ***
Choose addresses and verify
    Click Element   ${accountTab}
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${page}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${page}' page

    Wait Until Element Is Visible   ${addresses}    10
    Click Element   ${addresses}
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${page1}    Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${page1}' page

    Page Should Contain Element     ${headerleftButton}
    Page Should Contain Element     ${universalrightButton}
    Page Should Contain Element     ${emptystateaddress}
    Log To Console  \nAll elements present on the page

    Click Element   ${headerleftButton}
    Log To Console  \nNavigated back to addresses page
    Sleep   1

Choose add and verify
    Click Element   ${addresses}
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${page2}    Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${page2}' page

    Wait Until Element Is Visible   ${universalrightButton}    10
    Click Element   ${universalrightButton}
    Log To Console  \nClicked add new address button

    Page Should Contain Element     ${addaddressPage}
    Log To Console  \nRedirected to add new address page
    # Wait Until Element Is Visible   ${successLogincheck}    10
    # ${page3}     Get Text    ${successLogincheck}
    # Log To Console  \nRedirected to '${page3}' page
    # Click Element   ${universalrightButton}

    Page Should Contain Element     ${addresstitleTF}
    Page Should Contain Element     ${countrydropdown}

    Wait Until Element Is Visible   ${flat/housenoTF}    10
    ${page3}     Get Text    ${flat/housenoTF}
    Log To Console  \n'${page3}' is the field

    Input Text  ${flat/housenoTF}   1009
    # Page Should Contain Element     ${flat/housenoTF}
    Page Should Contain Element     ${floornoTF}
    Input Text  ${floornoTF}            24
    Input Text  ${housebuildingnoTF}    55
    Click Element   ${doneButton}

    Wait Until Element Is Visible   ${streetnoError}    10
    ${error}    Get Text    ${streetnoError}
    Log To Console  \nError message is '${error}'

    Page Should Contain Element     ${streetnameTF}
    Wait Until Element Is Visible   ${streetnoError}    10
    ${error}    Get Text    ${streetnoError}
    Log To Console  \nError message is '${error}'








