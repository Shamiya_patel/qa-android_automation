*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            notification-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot

Resource            TEST-notification-resource-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app and go to notifications
    Click Element   ${alertallowButton}
    Sleep   1
    # Successful login

# 2. Go to notification tab
#     Go to notification tab

2. Wait for notification banner and verify notification Addres not supported
    Verify notification banner
    Choose notifiation and verify   ${addressnotsupportedimage}     ${addressFailedlogo}

3. Delete address not supported notification
    Delete notification

4. Wait for notification banner and verify Total price update test
    Verify notification banner
    Choose notifiation and verify   ${totalPriceupdateimage}        ${priceupdateFaillogo}
    Delete notification
# 5. Purchase complete test
    # Verify notification banner
#     Purchase completed

6. Wait for notification banner and verify Purchase failed test
    Verify notification banner
    Choose notifiation and verify   ${purchasefailedimage}  ${purchaseFailedlogo}

7. Delete purchase failed notifiation
    Delete notification

8. Wait for notification banner and verify Wrong crad info test
    Verify notification banner
    Choose notifiation and verify   ${wrongcardinformationimage}    ${wrongcardInfologo}

9. Delete wrong card info notification
    Delete notification

10. Wait for notification banner and verify Merchant not responding test
    Verify notification banner
    Choose notifiation and verify  ${merchantnotrespondingimage}    ${merchantNotrespondinglogo}

11. Delete merchant not responding error
    Delete notification

12. Wait for notification banner and verify Merchant out of stock test
    Verify notification banner
    Choose notifiation and verify  ${merchantoutofstockimage}  ${merhcantOutofstocklogo}

13. Delete merchant our of stock notification
    Delete notification


    #Swipe the notification drawer and go to notification page TBD