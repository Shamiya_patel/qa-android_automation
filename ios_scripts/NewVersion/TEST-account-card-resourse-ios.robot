*** Variables ***
#Input data
${nickname}




*** Keywords ***
Choose cards and verify page
    Click Element   ${cards}
    Page Should Contain Element     ${cardPage}
    Wait Until Element Is Visible   ${successLogincheck}    10
    ${page1}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected to '${page1}' page

    Page Should Contain Element     ${nocarddiaplayImage}
    Log To Console  \nNo cards present, empty screen image displayd successfully

Choose add a card and go back to card list page
    Click Element   ${universalrightButton}
    Log To Console  \nClicked 'Add button' to add a new card
    Sleep   1
    Wait Until Element Is Visible   ${addnewcradpage}  10
    Log To Console  \nSuccessfully redirected to 'add new card' page

    Click Element   ${appBackbutton}
    Log To Console  \nClicked 'Back button' to go back to cards page
    Sleep   1

    Page Should Contain Element     ${successLogincheck}
    ${page3}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected back to '${page3}' page

Choose add card and verify
    Click Element   ${universalrightButton}
    Log To Console  \nClicked 'Add button' again to go to add a new card page

    Wait Until Element Is Visible   ${addnewcradpage}  10
    Log To Console  \nSuccessfully redirected to 'add new card' page

    Page Should Contain Element     ${nicknameTF}
    Page Should Contain Element     ${nicknameError}
    Page Should Contain Element     ${nameoncardTF}
    Page Should Contain Element     ${nameoncardError}
    Page Should Contain Element     ${cardtemplateImage}
    Page Should Contain Element     ${cardnoTF}
    Page Should Contain Element     ${cardnoError}
    Page Should Contain Element     ${expirydate}
    Page Should Contain Element     ${expirydateError}
    Page Should Contain Element     ${cvvTF}
    Page Should Contain Element     ${cvvError}
    Page Should Contain Element     ${billingaddressButton}
    Log To Console  \nAll elements visible on page

Cards > Add new card
    Click Element   ${appBackbutton}
    Log To Console  \nClicked 'Back button' to go back to cards page
    Sleep   1

    Page Should Contain Element     ${successLogincheck}
    ${page3}     Get Text    ${successLogincheck}
    Log To Console  \nRedirected back to '${page3}' page

    Click Element   ${universalrightButton}
    Log To Console  \nClicked 'Add button' to add a new card
    Sleep   1
    Wait Until Element Is Visible   ${addnewcradpage}  10
    Log To Console  \nSuccessfully redirected to 'add new card' page

Empty card
    Click Element   ${doneButton}
    Log To Console  \nSaved empty card form
    Wait Until Element Is Visible   ${nicknameError}    10
    ${error1}   Get Text    ${nicknameError}
    Log To Console  \nError message is '${error1}'

    Wait Until Element Is Visible   ${nameoncardError}    10
    ${error2}   Get Text    ${nameoncardError}
    Log To Console  \nError message is '${error2}'

    Wait Until Element Is Visible   ${cardnoError}    10
    ${error3}   Get Text    ${cardnoError}
    Log To Console  \nError message is '${error3}'

    Wait Until Element Is Visible   ${expirydateError}    10
    ${error4}   Get Text    ${expirydateError}
    Log To Console  \nError message is '${error4}'

    Wait Until Element Is Visible   ${cvvError}    10
    ${error5}   Get Text    ${cvvError}
    Log To Console  \nError message is '${error5}'

    Wait Until Element Is Visible   ${nameoncardError}    10
    ${error6}   Get Text    ${nameoncardError}
    Log To Console  \nError message is '${error6}'

Input credentials and save
    [Arguments]     ${field}    ${value}    ${element1}     ${element2}     ${element3}     ${element4}     ${element5}
    # ${nickname} =    Generate Random String    5   [LETTERS]
    # ${cardNickname}     Catenate    ${DEVICE_NAME}${nickname}
    # Log To Console    \nCard nickname is '${cardNickname}
    # Set Global Variable    ${cardNickname}

    Input Text  ${field}   ${value}
    Log To Console  \nEntered '${value}' as card name

    Click Element   ${doneButton}
    Log To Console  \nSaved card form with nickname only

    Wait Until Element Is Visible   ${element1}    10
    ${error2}   Get Text    ${element1}
    Log To Console  \nError message is '${error2}'

    Wait Until Element Is Visible   ${element2}    10
    ${error3}   Get Text    ${element2}
    Log To Console  \nError message is '${error3}'

    Wait Until Element Is Visible   ${element3}    10
    ${error4}   Get Text    ${element3}
    Log To Console  \nError message is '${error4}'

    Wait Until Element Is Visible   ${element4}    10
    ${error5}   Get Text    ${element4}
    Log To Console  \nError message is '${error5}'

    Wait Until Element Is Visible   ${element5}    10
    ${error6}   Get Text    ${element5}
    Log To Console  \nError message is '${error6}'

# Fill in card form and save

