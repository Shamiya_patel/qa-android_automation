*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            account-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot
Resource            TEST-account-resource-ios.robot
Resource            TEST-facebook-login-resource-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app and verify page content
    Accept notification pop-up alert and verify login page[ios-11]

2. Choose login with facebook and verify
    Login with facebook
    Verify facebook login

3. Keep app in background and test session timeout
    Verify session timeout