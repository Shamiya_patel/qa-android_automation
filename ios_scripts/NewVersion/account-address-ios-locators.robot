#ADDRESS PAGE LOCATORS

*** Variables ***
${addaddressPage}           accessibility_id=AddAddress_page
${emptystateaddress}        accessibility_id=address_default_view

#ADD ADDRESS FORM
${addresstitleTF}           accessibility_id=AddAddress_addressTitle_tf
${addaddresstitleError}     accessibility_id=AddAddress_addressTitle_error_lbl

${countrydropdown}          accessibility_id=AddAddress_country_btn

${flat/housenoTF}           accessibility_id=AddAddress_tableViewCell_0_0

${floornoTF}                accessibility_id=AddAddress_tableViewCell_0_1
${housebuildingnoTF}        accessibility_id=AddAddress_tableViewCell_0_2

${streetnameTF}             accessibility_id=AddAddress_tableViewCell_0_5
# accessibility_id=AddAddressTableCell_dynamic_tf


${streetnoError}            accessibility_id=AddAddressTableCell_dynamic_error_lbl