#Shop page locators

*** Variables ***
${browseIcon}           accessibility_id=masterHeader_browse_btn
${shopsearchField}      accessibility_id=Enter product name, brand, etc...

${defauldshopEmptystate}    accessibility_id=default-main-shop
${emptystateText}           accessibility_id=Shop for all the product in the world!

${shopTab}          accessibility_id=masterFooter_shop_btn
${watchlistTab}     accessibility_id=masterFooter_watchlist_btn
${notificationTab}  accessibility_id=masterFooter_notification_btn
${accountTab}       accessibility_id=masterFooter_account_btn
# ${chatTab}          accessibility_id=masterFooter_chat_btn


# ${backtopagebutton}   accessibility_id=masterHeader_back_btn

# ${backButton}   accessibility_id=Back

# ${genericTitle}     accessibility_id=masterHeader_title_lbl