*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            account-ios-locators.robot
Resource            account-card-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot
Resource            TEST-account-resource-ios.robot
Resource            TEST-account-card-resourse-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Login app and go to accounts > cards
    # Accept notification pop-up alert and verify login page[ios-11]
    # Successful login
    Click Element   ${alertallowButton}
    Sleep   1

2. Go to accounts
    Go to accounts tab
    Choose cards and verify page

3. Go to add new card page and back to cards page
    Choose add a card and go back to card list page

4. Verify add card page
    Choose add card and verify

5. Save an empty card
    Empty card

6. Enter card nickname and save
    ${nickname} =    Generate Random String    5   [LETTERS]
    ${cardNickname}     Catenate    ${DEVICE_NAME}${nickname}
    Log To Console    \nCard nickname is '${cardNickname}
    Set Global Variable    ${cardNickname}

    Cards > Add new card
    Input credentials and save  ${nicknameTF}   ${cardNickname}     ${nameoncardError}  ${cardnoError}  ${expirydateError}  ${cvvError}     ${nameoncardError}

