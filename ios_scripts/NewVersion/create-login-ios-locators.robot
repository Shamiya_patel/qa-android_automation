#Login / Register page locators

*** Variables ***
#Landing page notification pop-up
${alertallowButton}             accessibility_id=Allow
${allertdontallowButton}        accessibility_id=Don’t Allow

${createloginappLogo}           accessibility_id=launch-image
${loginwithText}                accessibility_id=LOGIN WITH

${landingpagefacebookButton}    accessibility_id=login_facebook_btn
${landingpagegoogleButton}      accessibility_id=login_google_btn

#Placeholder
${inputnamePlaceholder}         accessibility_id=Name
${inputsurnamePlaceholder}      accessibility_id=Surame
${inputemailPlaceholder}        accessibility_id=Email address
${inputpasswordPlaceholder}     accessibility_id=Password

#Signup/Register
${signupPage}                   accessibility_id=signup_page
${inputName}                    accessibility_id=signup_name_tf
${inputSurname}                 accessibility_id=signup_surname_tf
${signupEmailfield}             accessibility_id=signup_email_tf
${signupPasswordfield}          accessibility_id=signup_password_tf
${T&Ctext}                      accessibility_id=signup_termCondition_textView

${registerLink}                 accessibility_id=login_register_btn
${registerButton}               accessibility_id=signup_register_btn


#Login
${loginEmailfield}              accessibility_id=login_email_tf
${loginPasswordfield}           accessibility_id=login_password_tf
${loginbutton}                  accessibility_id=login_login_btn


${forgotpasswordLink}           accessibility_id=login_forgetPassword_btn

${T&CLink}                      accessibility_id=TERMS AND CONDITIONS
${privacypolicyLink}            accessibility_id=PRIVACY POLICY

# ${successLogincheck}          accessibility_id=masterHeader_title_lbl

#Error labels for login page
${emailErrorlabel}              accessibility_id=login_email_error_lbl
${passwordErrorlabel}           accessibility_id=login_password_error_lbl

#Register error labels
${registernameErrorlabel}       accessibility_id=signup_name_error_lbl
${registesurrnameErrorlabel}    accessibility_id=signup_surname_error_lbl
${registeremaileErrorlabel}     accessibility_id=signup_email_error_lbl
${registerpasswordErrorlabel}   accessibility_id=signup_password_error_lbl

#Error messages SIGN-UP
# ${nameEmptyerror}        accessibility_id=Name a mandatory field.
# ${surnmaeEmptyerror}     accessibility_id=Surname a mandatory field.
# ${emailEmptyerror}       accessibility_id=Email a mandatory field.
# ${passwordEmptyerror}    accessibility_id=Password a mandatory field.
# ${invalidEmailerror}     accessibility_id=Email format is not correct.
# ${shortPassworderror}    accessibility_id=Password must be at least 6 digit long.



