#Card page locators

*** Variables ***
${cardpageTitle}            accessibility_id=masterHeader_title_lbl

${cardPage}                 accessibility_id=Cards_page
${nocarddiaplayImage}       accessibility_id=default-main-card


#New card page
${addnewcradpage}           accessibility_id=AddCard_page

${nicknameTF}               accessibility_id=AddCard_nickname_tf
${nicknameError}            accessibility_id=AddCard_nickname_error_lbl

${nameoncardTF}             accessibility_id=AddCard_cardName_tf
${nameoncardError}          accessibility_id=AddCard_cardName_error_lbl

${cardtemplateImage}        accessibility_id=card-template
${cardnoTF}                 accessibility_id=AddCard_cardNumber_tf
${cardnoError}              accessibility_id=AddCard_cardNumber_error_lbl

${expirydate}               accessibility_id=AddCard_expireDate_tf
${expirydateError}          accessibility_id=AddCard_expireDate_error_lbl

${cvvTF}                    accessibility_id=AddCard_cvc_tf
${cvvError}                 accessibility_id=AddCard_cvc_error_lbl

${billingaddressButton}     accessibility_id=AddCard_address_btn
# ${billingaddressError}      accessibility_id=