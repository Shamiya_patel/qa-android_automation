*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot
Resource            create-login-ios-locators.robot
# Resource            TEST-144-email-registration-resource-ios.robot
Resource            TEST-email-login-resource-ios.robot
Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app and verify page content
    Accept notification pop-up alert and verify login page[ios-11]

2. Login with empty email and password
    Empty credentials   ${EMPTY}    ${EMPTY}    ${loginbutton}      ${emailErrorlabel}    ${passwordErrorlabel}
    # Invalid login test    ${EMPTY}    ${EMPTY}    ${loginbutton}      ${emailErrorlabel}    ${passwordErrorlabel}

3. Login with empty email only
    Invalid login test    ${EMPTY}    ${password}   ${loginbutton}    ${emailErrorlabel}

4. Login with empty password only
    Clear Text  ${loginPasswordfield}
    Invalid login test    ${correctSigninemail}    ${EMPTY}     ${loginbutton}  ${passwordErrorlabel}

5. Login with invalid email format
    Clear login credentials
    Invalid login test    ${incorrectemail}        ${password}      ${loginbutton}  ${emailErrorlabel}

6. Login with valid email and incorrect password
    Clear login credentials
    Empty credentials    ${correctSigninemail}    ${mismatchPassword}    ${loginbutton}    ${emailErrorlabel}  ${passwordErrorlabel}

7. Login with correct credentials
    Clear login credentials
    Valid login test      ${correctSigninemail}    ${password}            ${loginbutton}