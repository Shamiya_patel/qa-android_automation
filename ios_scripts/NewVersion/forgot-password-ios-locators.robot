#Forgot my password page locators

*** Variables ***
${forgotpasswordPage}               accessibility_id=forgotPW_page
${forgotpasswordBackButton}         accessibility_id=forgotPW_back_btn
${forgetpasswordemailField}         accessibility_id=forgotPW_email_tf
${forgotpasswordsendemailButton}    accessibility_id=forgotPW_send_btn

#Error message
${forgotpasswordError}              accessibility_id=forgotPW_email_error_lbl

# #Error messages
# ${incorrectEmailerror}      accessibility_id=Invalid email address
# ${nonregisteredEmail}       accessibility_id=This user does not exist