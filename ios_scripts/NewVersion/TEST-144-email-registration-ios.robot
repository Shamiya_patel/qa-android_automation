*** Settings ***
Library             AppiumLibrary

Resource            ios_resource.robot
Resource            TEST-144-email-registration-resource-ios.robot

Resource            create-login-ios-locators.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Launch app and verify page content
    Accept notification pop-up and verify landing page

2. Choose register and verify page content
    Choose register link
    ${emailName} =    Generate Random String    5   [LETTERS]
    ${correctEmail}     Catenate    ${string1}${string2}${emailName}${zwoopmail}
    Log To Console    \nEmail to be used for registration is ${correctEmail}
    Set Global Variable    ${correctEmail}

    ${name}=   Generate Random String  2   [LETTERS]
    ${firstName}    Catenate    ${firstname}-${name}
    Log To Console    \nFirst name to be used for registration is ${firstName}
    Set Global Variable    ${firstName}

    Verify create new account page

3. Sign-up with empty credentials
    Empty credentials  ${EMPTY}     ${EMPTY}    ${EMPTY}    ${EMPTY}    ${registerButton}   ${registernameErrorlabel}   ${registesurrnameErrorlabel}    ${registeremaileErrorlabel}     ${registerpasswordErrorlabel}
    Sleep    1

4. Sign-up with empty name
    Input credentials       ${EMPTY}        ${lastName}     ${correctEmail}     ${password}         ${registerButton}       ${registernameErrorlabel}
    Sleep    1

5. Sign-up with empty surname
    Input credentials       ${firstName}    ${EMPTY}        ${correctEmail}     ${password}         ${registerButton}       ${registesurrnameErrorlabel}
    Sleep    1

6. Sign-up with empty email
    Input credentials       ${firstName}    ${lastName}     ${EMPTY}            ${password}         ${registerButton}       ${registeremaileErrorlabel}
    Sleep    1

7. Sign-up with empty password
    Input credentials       ${firstName}    ${lastName}     ${correctEmail}     ${EMPTY}            ${registerButton}       ${registerpasswordErrorlabel}
    Sleep    1

8. Sign-up with wrong email fromat1
    Input credentials       ${firstName}    ${lastName}     ${wrongemail1}      ${password}         ${registerButton}       ${registeremaileErrorlabel}
    Sleep    1

9. Sign-up with wrong email fromat2
    Input credentials       ${firstName}    ${lastName}     ${wrongemail2}      ${password}         ${registerButton}       ${registeremaileErrorlabel}
    Sleep    1

10. Sign-up with short password
    Input credentials       ${firstName}    ${lastName}     ${correctEmail}      ${shortpassword}   ${registerButton}       ${registerpasswordErrorlabel}
    Sleep    1

11. Sign-up with correct credentials
    Input correct credentials       ${firstName}    ${lastName}     ${correctEmail}      ${password}    ${registerButton}
    Sleep    1


