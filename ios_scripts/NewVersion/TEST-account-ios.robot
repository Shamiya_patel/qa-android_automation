*** Settings ***
Library             AppiumLibrary
Resource            ios_resource.robot

Resource            create-login-ios-locators.robot
Resource            account-ios-locators.robot

Resource            TEST-email-login-resource-ios.robot
Resource            TEST-account-resource-ios.robot

Suite Setup         Launch Application  ${APPIUM_SERVER}    ${PLATFORM_VERSION}   ${DEVICE_NAME}
Suite Teardown      Close Application

*** Variables ***


*** Test Cases ***
1. Login to an existing account
    Accept notification pop-up alert and verify login page[ios-11]
    Successful login

2. Go to accounts tab and verify page content
    # Click Element   ${alertallowButton}
    # Sleep  1
    Go to accounts tab

3. Check profile page
    Go to profile

4. Check cards page
    Go to cards

5. Logout form account
    Logout