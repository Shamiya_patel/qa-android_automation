*** Settings ***
Documentation   This test is to verify facebook signin case
Library         AppiumLibrary
# Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
Resource        ios_resource.robot
Library         BuiltIn
Library         String


*** Variables ***



*** Test Cases ***
# [TEST-147] - Mobile app: Sign-in with facebook(ios)

1. Choose allow and select login with facebook(first time)
    Click Element   accessibility_id=Allow
    Sleep   1
    Click Element   accessibility_id=login_facebook_button
    Wait Until Page Contains Element    xpath=//XCUIElementTypeTextField[contains(@value,'Email address or phone number')]  10

2. Enter email is and password to login with facebook
    Input Value     xpath=//XCUIElementTypeTextField[contains(@value,'Email address or phone number')]  samy_nndpenm_autotest@tfbnw.net
    Input Value     xpath=//XCUIElementTypeSecureTextField[contains(@value,'Facebook password')]        123456test
    Click Element   accessibility_id=Log In
    Sleep   5

# once login with X account login data is cached for easy login in future
#TO DO facebook login need to be refined when dev side update