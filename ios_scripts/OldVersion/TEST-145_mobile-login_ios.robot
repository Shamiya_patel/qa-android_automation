*** Settings ***
Documentation   This test is to verify login with email succeed and
...             fail correctly depending on users input login information
...             see https://zwoopltd.atlassian.net/browse/POC-1133
Metadata        Version     0.1

Library         AppiumLibrary
Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots
Library         BuiltIn
Library         String

# Test Template   Login test with different cases

Resource        ios_resource.robot

Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}


*** Variables ***



*** Test Cases ***
# [TEST-145] - Mobile app: Email login(ios)

1. Login with empty email and password
    Set new screenshot directory
    Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${EMPTY}    ${PASSWORD_FIELD}   ${EMPTY}
    Click Element   ${OKAY_POPUP}
    Log  Login with empty email and password - Test successful
    Reset Application

2. Login with password only
    Set new screenshot directory
    Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${EMPTY}    ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    Click Element   ${OKAY_POPUP}
    Log  Login with password only - Test successful
    Reset Application

3. Login with email only
    Set new screenshot directory
    Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL_SIGNIN}  ${PASSWORD_FIELD}   ${EMPTY}
    Click Element   ${OKAY_POPUP}
    Log  Login with email only - Test successful
    Reset Application

4. Login with wrong email format
    Set new screenshot directory
    Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${INVALID_EMAIL_FORMAT}  ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    Click Element   ${OKAY_POPUP}
    Log  Login with wrong email format - Test successful
    Reset Application

#Skip special cases until API and dev side is ready
    # Log     "Step : Login with wrong password/format"
    # Click Element   accessibility_id=Allow
    # Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}   ${INVALID_INCORRECT_PASSWORD}
    # Click Element   ${OKAY_POPUP}
    # Reset Application

    # Log     "Step : Login with email and password mismatch"
    # Click Element   accessibility_id=Allow
    # Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}   ${INVALID_INCORRECT_PASSWORD}
    # Sleep   2
    # Click Element   ${OKAY_POPUP}
    # Reset Application

5. Login with correct email and password
    Set new screenshot directory
    Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL_SIGNIN}  ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    Wait Until Element Is Visible   accessibility_id=Watch List     40
    # Capture Page Screenshot      login_success${DATE}.png
    ${text}     Get Text    accessibility_id=Watch List
    Log      Login with correct email and password - Test successful. Landing page is ${text}

