*** Settings ***
Documentation   This test help to verify sign up with new email.
Library         AppiumLibrary
# Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
Resource        ios_resource.robot
Library         BuiltIn
Library         String



*** Variables ***
# ${str1}     shamiya.patel
# ${str3}     @zwoop.biz
# ${str2}     +


*** Test Cases ***
    # Log     "Step: Setup screenshot directory"
    # Set Screenshot Directory    /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/screenshot${Date}

    # Register Keyword To Run On Failure      Set new screenshot directory

    # Log     "Step 8: Go to accounts, choose log-out and confirm"
    # Click Element   accessibility_id=Allow
    # Sleep   1
    # Click Element   accessibility_id=item account normal
    # Click Element   accessibility_id=LOGOUT
    # Click Element   ${OKAY_POPUP}
    # Sleep   2

    # ${Date}=  Get DateTime
    # ${timestamp}    Convert Date  ${Date}     epoch
    # Screenshot.Set Screenshot Directory     /mobile_automation/zwoop_automation/iOS_automation/ios_logs/ios_screenshots${timestamp}
    # Capture Page Screenshot   /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot${timestamp}.png

#Include missing fields and other sign up cases once api and dev side is ready
1. Enter non-registered email and sign up
    Click Element   accessibility_id=Allow
    Generate random number
    Generate email  ${NO}
    Login Test  ${EMAIL_FIELD}  ${NEW_EMAIL_REGISTER}  ${PASSWORD_FIELD}   ${NEW_PASSWORD}
    Wait Until Element Is Visible    accessibility_id=signup_first_field     60

2. Test '<'BACK button on create account page
# Empty state
    Click Element   ${BACK_BUTTON}

3. Create an account
# Now choose login and input information to create an account
    Click Element   ${LOGIN_BUTTON}
    Wait Until Element Is Visible    accessibility_id=signup_first_field     30
    Generate random name/surname for sign-up
    Input Value      accessibility_id=signup_first_field    ${SIGN-UP_NAME}
    Generate random name/surname for sign-up
    Input Value      accessibility_id=signup_last_field     ${SIGN-UP_NAME}
    Input Value      accessibility_id=signup_password_field     ${NEW_PASSWORD}
    Click Element   accessibility_id=signup_create_button
    Sleep   40

    # Wait Until Page Contains Element    accessibility_id=item account normal    20

    # Log     "Step 2: Go to accounts, choose log-out and confirm"
    # Click Element   accessibility_id=item account normal
    # Click Element   accessibility_id=LOGOUT
    # Click Element   ${OKAY_POPUP}
    # Sleep   2

    # Run Keyword If Any Tests Failed   Set new screenshot directory  Failed
    # Register Keyword To Run On Failure      Capture Page Screenshot   /mobile_automation/zwoop_automation/iOS_automation/ios_logs/ios_screenshots/fail_ios_image${timestamp}.png
*** Keywords ***
