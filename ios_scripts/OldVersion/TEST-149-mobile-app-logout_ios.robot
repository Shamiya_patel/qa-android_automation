*** Settings ***
Documentation   This test is to verify logout case
Library         AppiumLibrary
Library         Screenshot
Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot.png${Date}
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
Resource        ios_resource.robot
Library         BuiltIn
Library         String


*** Variables ***



*** Test Cases ***
1. Go to accounts tab from watchlist
    Set new screenshot directory
    # Set Screenshot Directory    /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot.png${Date}
    Click Element   accessibility_id=Allow
    Wait Until Element Is Visible    accessibility_id=item account normal
    Click Element   accessibility_id=item account normal
    Log      Redirected successfully to account tab
#Add below lines to test standalone logout test
    # Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL_SIGNIN}  ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    # Sleep   15

    # Log     "Step 2: Choose log-out and confirm"
2. Choose logout and confirm
    Click Element   accessibility_id=LOGOUT
    Click Element   ${OKAY_POPUP}
    Wait Until Element Is Visible   ${LOGIN_BUTTON}
    Log      Logout successful.

