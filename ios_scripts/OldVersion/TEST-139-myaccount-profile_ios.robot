*** Settings ***
Documentation   This test help to verify my accounts ---> profile section
Library         AppiumLibrary
Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
Resource        ios_resource.robot
Library         BuiltIn
Library         String



*** Variables ***


*** Test Cases ***
1. Go to account tab
    Click Element   accessibility_id=Allow
    Wait Until Element Is Visible   accessibility_id=item account normal    10
    Click Element   accessibility_id=item account normal

2. Select profile section
    Click Element   accessibility_id=PROFILE

3. Add a profile picture from gallery
#Check for different devices in case if the coordinates change
    Click A Point   x=144   y=154
    Click Element       accessibility_id=Photo Library
#Permission to access photos pop-up dialogue appears
    Wait Until Element Is Visible   accessibility_id=OK
    Click Element   accessibility_id=OK
    Wait Until Element Is Visible   accessibility_id=All Photos
    Click Element   accessibility_id=All Photos
#Below image hard is coaded [check for better way, if any]
    Click Element   accessibility_id=Photo, Portrait, 03 August, 15:22

4. Edit profile name
    Generate random name/surname for sign-up
    Clear Text      accessibility_id=sub_profile_first_field
    Input Value     accessibility_id=sub_profile_first_field    ${SIGN-UP_NAME}

5. Edit profile surname
    Generate random name/surname for sign-up
    Clear Text      accessibility_id=sub_profile_last_field
    Input Value     accessibility_id=sub_profile_last_field     ${SIGN-UP_NAME}

6. Edit contact number
    Generate random phone number
    Clear Text      accessibility_id=sub_profile_phone_field
    Input Value     accessibility_id=sub_profile_phone_field    ${PHONE_NO}
# Skip country code -- Wait until dev side is ready

7. Update existing password
    Input Value     accessibility_id=sub_profile_oldPW_field    ${VALID_PASSWORD_SIGNIN}
    Input Value     accessibility_id=sub_profile_newPW_field    ${MYACC_NEWPASSWORD}
    Click A Point   x=15    y=200
    Sleep   2

8. Add a profile picture using camera
    Click A Point   x=144   y=154
    Wait Until Element Is Visible   accessibility_id=Camera Roll
    Click Element       accessibility_id=Camera Roll
    Wait Until Element Is Visible   accessibility_id=OK
    Click Element       accessibility_id=OK
    Wait Until Page Contains Element    accessibility_id=PhotoCapture
    Click Element       accessibility_id=PhotoCapture
    Click Element       accessibility_id=Use Photo
#Retake picture from camera
    # Log     "Retake picture"
    # Click A Point   x=144   y=154
    # Wait Until Element Is Visible   accessibility_id=Camera Roll
    # Click Element       accessibility_id=Camera Roll
    # Wait Until Page Contains Element    accessibility_id=PhotoCapture
    # Click Element       accessibility_id=PhotoCapture
    # Click Element       accessibility_id=Retake
    # Click Element       accessibility_id=PhotoCapture
    # Click Element       accessibility_id=Use Photo

9. Save clicked image
    Set new screenshot directory
    Click Element   accessibility_id=sub_profile_save_button
    Capture Page Screenshot     ios_screenshot${DATE}.png
    Sleep   30

10. Click '<' button and go back to accounts page
    # Wait Until Page Contains Element    accessibility_id=back btn white
    Click Element   accessibility_id=back btn white



