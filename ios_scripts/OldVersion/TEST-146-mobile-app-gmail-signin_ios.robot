*** Settings ***
Documentation   This test is to verify gamil signin case
Library         AppiumLibrary
# Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
Resource        ios_resource.robot
Library         BuiltIn
Library         String


*** Variables ***



*** Test Cases ***
# [TEST-146] - Mobile app: Sign-in with google(ios)

1. Choose allow and select login with google(first time add gamil)
    Click Element   accessibility_id=Allow
    Sleep   1
    Click Element   accessibility_id=login_gmail_button

#First time add gmail account <-----
    # Log     "Step 2: Enter email/phone number to login with google"
    # Input Text  accessibility_id=Email or phone     ${GMAIL_SIGNIN}
    # Click Element   accessibility_id=NEXT

    # Log     "Step 3: Enter password and choose next"
    # Input Text  accessibility_id=Enter your password    ${GMAIL_PASSWORD}
    # Click Element   accessibility_id=NEXT

#Gmail login need to refined once dev side is ready
    # Click Element   ${OKAY_POPUP}

#Once gamail added login from here.
2. Select login with google and choose existing gmail account(also use another account option available)
    Sleep   1
    Click Element   accessibility_id=Zoow Pz zwoopautomation@gmail.com

#Gmail login need to refined once dev side is ready
    Click Element   ${OKAY_POPUP}