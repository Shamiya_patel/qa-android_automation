*** Settings ***
Documentation   This test is to verify forget/reset password
Library         AppiumLibrary
Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
Resource        ios_resource.robot
Library         BuiltIn
Library         String


*** Variables ***



*** Test Cases ***
# [TEST-148] - Mobile app: Reset password(ios)

#Add case for input email and select forgot password should go to forgot password page and autofill email
1. Select forget password button without entering email
    Click Element   accessibility_id=Allow
    Sleep   1
    Click Element   accessibility_id=login_forgetPW_button
    # Click Element   accessibility_id=Back
    #Below step should not appear when user didnt enter password and select back to login page
    # Click Element   ${OKAY_POPUP}
    Click Element   accessibility_id=forgetPW_send_button
    Click Element   ${OKAY_POPUP}
    Sleep   5

2. Enter email to reset password
    Input Text      accessibility_id=forgetPW_email_field  ${GMAIL_SIGNIN}
    Click Element   accessibility_id=forgetPW_send_button
    Sleep   30
    Click Element       ${OKAY_POPUP}
    Click Element   accessibility_id=Back

    #Need to add email verification [TBD - as per jira]