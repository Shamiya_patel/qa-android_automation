*** Settings ***
Documentation     A resource file with reusable keywords and variables to test iOS app.
Library     AppiumLibrary
Library     Screenshot
Library     BuiltIn
Library     String
Library     DateTime
Library     OperatingSystem
Resource    create-login-ios-locators.robot


*** Variables ***
#Appium Configuration
${APP}                  /mobile_automation/zwoop_automation/iOS_automation/iOS_builds/ThePro27dec2017.app
# ${APPIUM_SERVER}        http://localhost:4726/wd/hub
${APPIUM_SERVER}        http://localhost:${PORT}/wd/hub
${PLATFORM_NAME}        iOS
${UDID}                 02210bbfcf008bee48ea3d6fae25cdcc6ac1026e
${PLATFORM_VERSION}     10.3.1
${DEVICE_NAME}          QAiphone-six
${BUNDLE_ID}            biz.zwoop.ios
${AUTOMATION_NAME}      XCUITest
${ORG_ID}               QZZC6KS99Q

#Universal buttons
${OKAY_POPUP}                   accessibility_id=Okay
${universalappbackButton}       accessibility_id=universal_back_button
${cancel}                       accessibility_id=Cancel
${appBackbutton}                accessibility_id=Back

#Valid sign-in credentials
${correctSigninemail}   qa.automationoAUAr@zwoop.biz
${password}             123456

# #Data
# ${VALID_EMAIL_SIGNIN}                  qa01@zwoop.biz
# ${VALID_PASSWORD_SIGNIN}               123
# ${INVALID_EMAIL_FORMAT}                qa01zwoop.biz
# ${INVALID_INCORRECT_PASSWORD}          xxxx
# ${NEW_PASSWORD}                        newpassword
# ${ADD_ADDRESS_LOGIN}                   shamiya.patel+4@zwoop.biz
# ${ADD_ADDRESS_PASSWORD}                1111
# # ${FORGOT_PASSWORD_EMAIL}               zwoopautomation@gamil.com
# ${GMAIL_SIGNIN}                        zwoopautomation@gamil.com
# ${GMAIL_PASSWORD}                      zwoopautomation
# ${MYACC_NEWPASSWORD}                   test1234facebook
# ${str1}     shamiya.patel
# ${str3}     @zwoop.biz
# ${str2}     +

#Results
# ${SCREENSHOT_DIRECTORY}     /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png${Date}
${TYPE OF FILE}              .png
# ${TEMPDIR}                   /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/screenshot${Date}
${IOS_DEVICELOGS}           /usr/local/lib/node_modules/deviceconsole/deviceconsole
${XCODE_CONFIG_FILE}        /mobile_automation/wdaxcode.xcconfig

*** Keywords ***
Launch Application
    [Arguments]         ${APPIUM_SERVER}   ${PLATFORM_VERSION}   ${DEVICE_NAME}
    Open Application    ${APPIUM_SERVER}    platformName=${PLATFORM_NAME}   platformVersion=${PLATFORM_VERSION}
    ...                 deviceName=${DEVICE_NAME}   app=${APP}  udid=${UDID}    bundleId=${BUNDLE_ID}   autoGrantPermissions=True
    ...                 automationName=${AUTOMATION_NAME}   fullReset=True  clearSystemFiles=True   xcodeOrgId=${ORG_ID}
    ...                 xcodeSigningId=iPhone Developer     usePrebuiltWDA=True     showXcodeLog=True   iosInstallPause=1000
    ...                 realDeviceLogger=${IOS_DEVICELOGS}      xcodeConfigFile=${XCODE_CONFIG_FILE}    shouldUseSingletonTestManager=False
    # ...                 commandTimeout=120000'{"findElement": 40000, "findElements": 40000, "setValue": 20000, "default": 120000}'
    # ...                 webDriverAgentUrl=WDAServer.SERVER_URL      webDriverAgentUrl=http://localhost:8100 useNewWDA=False   keychainPath=/mobile_automation/android_zwoop/iOS_automation/iOS_builds/MyPrivateKey.p12
# com.facebook.WebDriverAgentRunner     keychainPassword=zwoopZwoop1234     useNewWDA      newCommandTimeout=120
# 120000', '{"findElement": 40000, "findElements": 40000, "setValue": 20000, "default": 120000}'
    Log To Console  \nApp launched

Sign-in Test
    Input Value          ${loginEmailfield}       ${correctSigninemail}
    Input Value          ${loginPasswordfield}    ${password}
    Click Element        ${loginbutton}
    Wait Until Element Is Visible   ${successSign-upcheck}      20
    ${Landingpagename}  Get Text    ${successSign-upcheck}
    Log To Console  \nSign-in successful, landed on '${Landingpagename}'  page.

# Generate random number
#     ${NUM}   Generate Random String  2   [NUMBERS]
#     Log To Console  \n Number generated is ${NUM}
#     Set Global Variable     ${NO}   ${NUM}

# Generate random phone number
#     ${NUM}   Generate Random String  8   [NUMBERS]
#     Log To Console  \n Number generated is ${NUM}
#     Set Global Variable     ${PHONE_NO}   ${NUM}

# Generate email
# #Here a new email is generated everytime for fresh registration test
#     [Arguments]     ${number}
#     ${EMAIL}    Catenate    ${str1}${str2}${number}${str3}
#     Log To Console  \n Email is ${EMAIL}
#     Set Global Variable     ${NEW_EMAIL_REGISTER}    ${EMAIL}

# Generate random name/surname for sign-up
#     ${NAME}     Generate Random String      7   [LETTERS]
#     Log To Console  \n Sign-up name/surname is ${NAME}
#     Set Global Variable     ${SIGN-UP_NAME}     ${NAME}

# Get DateTime
#     ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
#     [Return]     ${date1}

# Set new screenshot directory
#     # [Arguments]     ${filename}
#     ${Date}=  Get DateTime
#     ${TIMESTAMP}    Convert Date  ${Date}     epoch
#     Set Global Variable     ${Date}     ${DATE}
#     #Create Directory            /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/screenshot${timestamp}

#     Set Screenshot Directory    /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots
    # Capture Page Screenshot     ios_screenshot${timestamp}${TYPE OF FILE}
    # Set Global Variable  ${Path}   /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/screenshot${Date}
    # Log To Console  ${Date}

# Screenshot
#     # [Arguments]  ${filename}
#     Create Directory    ${Path}
#     Set Screenshot Directory    ${Path}
#     # Wait Until Page Contains Element
#     # ${datetime}=  Get DateTime
#     Capture Page Screenshot  ios-screenshot
#     # File Should Exist   ${Path}
#     Log To Console  ${\n}Screenshot






