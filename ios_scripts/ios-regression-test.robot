*** Settings ***
Documentation   This is a regression test
Library         AppiumLibrary
# Library         Screenshot       /mobile_automation/zwoop_automation/iOS_automation/ios_screenshots/ios_screenshot-1.png
Suite Setup     Launch Application
Suite Teardown  Remove Application  ${BUNDLE_ID}
# Suite Teardown  Close Application
Resource        ios_resource.robot
Library         BuiltIn
Library         String


*** Variables ***



*** Test Cases ***
# [TEST-145] - Mobile app: Regression test(ios)

    # Log     "Step: Setup screenshot directory"
    # Get DateTime
    # Create new screenshot directory
    # Screenshot accessibility_id=Allow

Setp 1: Launch app
    Launch Application
    Sleep   1

Login with empty email and password
    # Wait Until Element Is Visible       xpath=//XCUIElementTypeButton[contains(@name,'Allow')]      10
    # Click Element   xpath=//XCUIElementTypeButton[contains(@name,'Allow')]
    # Log To Console  \nClicking on notification pop-up
    Login Test  ${EMAIL_FIELD}  ${EMPTY}    ${PASSWORD_FIELD}   ${EMPTY}
    Sleep   2
    # Click Element   ${OKAY_POPUP}
    Reset Application

Step 2: Login with password only
    # Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${EMPTY}    ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    Sleep   2
    # Click Element   ${OKAY_POPUP}
    Reset Application

    Log     "Step 3: Login with email only"
    # Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL_SIGNIN}  ${PASSWORD_FIELD}   ${EMPTY}
    Sleep   2
    # Click Element   ${OKAY_POPUP}
    Reset Application

    Log     "Step 4: Login with wrong email format"
    # Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${INVALID_EMAIL_FORMAT}  ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    Sleep   2
    # Click Element   ${OKAY_POPUP}
    Reset Application

#Skip special cases until API and dev side is ready
    # Log     "Step : Login with wrong password/format"
    # Click Element   accessibility_id=Allow
    # Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}   ${INVALID_INCORRECT_PASSWORD}
    # Click Element   ${OKAY_POPUP}
    # Reset Application

    # Log     "Step : Login with email and password mismatch"
    # Click Element   accessibility_id=Allow
    # Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL}  ${PASSWORD_FIELD}   ${INVALID_INCORRECT_PASSWORD}
    # Sleep   2
    # Click Element   ${OKAY_POPUP}
    # Reset Application

    Log     "Step 7: Login with correct email and password"
    Click Element   accessibility_id=Allow
    Login Test  ${EMAIL_FIELD}  ${VALID_EMAIL_SIGNIN}  ${PASSWORD_FIELD}   ${VALID_PASSWORD_SIGNIN}
    Sleep   20

    Log     "Step 8: Go to accounts, choose log-out and confirm"
    Click Element   accessibility_id=item account normal
    Click Element   accessibility_id=LOGOUT
    Click Element   ${OKAY_POPUP}
    Sleep   2

#Add case for input email and select forgot password should go to forgot password page and autofill email
    Log     "Step 9: Select forget password button without entering email"
    Sleep   1
    Click Element   accessibility_id=login_forgetPW_button
    # Click Element   accessibility_id=Back
    #Below step should not appear when user didnt enter password and select back to login page
    # Click Element   ${OKAY_POPUP}
    Click Element   accessibility_id=forgetPW_send_button
    Click Element   ${OKAY_POPUP}
    Sleep   5
    Input Text      accessibility_id=forgetPW_email_field  ${GMAIL_SIGNIN}
    Click Element   accessibility_id=forgetPW_send_button
    Sleep   30
    Click Element       ${OKAY_POPUP}
    Click Element   accessibility_id=Back

#Include missing fields and other sign up cases once api and dev side is ready
    Log     "Step: 10 Enter non-registered email and sign up"
    Generate random number
    Generate email  ${NO}
    Login Test  ${EMAIL_FIELD}  ${NEW_EMAIL_REGISTER}  ${PASSWORD_FIELD}   ${NEW_PASSWORD}
    Sleep   15
    Generate random name/surname for sign-up
    Input Text      accessibility_id=signup_first_field    ${SIGN-UP_NAME}
    Generate random name/surname for sign-up
    Input Text      accessibility_id=signup_last_field     ${SIGN-UP_NAME}
    Input Text      accessibility_id=signup_password_field     ${NEW_PASSWORD}
    Click Element   accessibility_id=signup_create_button
    Sleep   30

    # Close Application
    # Remove Application  ${BUNDLE_ID}
